/**
 */
package aatl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link aatl.Variable#getInitExpression <em>Init Expression</em>}</li>
 * </ul>
 *
 * @see aatl.AatlPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends PatternElement {
	/**
	 * Returns the value of the '<em><b>Init Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Expression</em>' attribute.
	 * @see #setInitExpression(String)
	 * @see aatl.AatlPackage#getVariable_InitExpression()
	 * @model
	 * @generated
	 */
	String getInitExpression();

	/**
	 * Sets the value of the '{@link aatl.Variable#getInitExpression <em>Init Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Expression</em>' attribute.
	 * @see #getInitExpression()
	 * @generated
	 */
	void setInitExpression(String value);

} // Variable
