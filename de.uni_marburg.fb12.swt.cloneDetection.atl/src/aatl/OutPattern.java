/**
 */
package aatl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link aatl.OutPattern#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see aatl.AatlPackage#getOutPattern()
 * @model
 * @generated
 */
public interface OutPattern extends EObject {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link aatl.OutPatternElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see aatl.AatlPackage#getOutPattern_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<OutPatternElement> getElements();
	

	/**
	 * Accept a ModulePartVisitor.
	 * @param the visitor
	 * @generated NOT
	 */
    void accept(ModulePartVisitor visitor);

} // OutPattern
