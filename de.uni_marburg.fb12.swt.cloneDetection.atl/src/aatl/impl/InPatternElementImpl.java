/**
 */
package aatl.impl;

import aatl.AatlPackage;
import aatl.InPatternElement;
import aatl.ModulePartVisitor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Pattern Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InPatternElementImpl extends PatternElementImpl implements InPatternElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InPatternElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AatlPackage.Literals.IN_PATTERN_ELEMENT;
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void accept(ModulePartVisitor visitor) {
		visitor.visit(this);
	}
} //InPatternElementImpl
