/**
 */
package aatl.impl;

import aatl.AatlPackage;
import aatl.PatternElement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Pattern
 * Element</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link aatl.impl.PatternElementImpl#getVarName <em>Var Name</em>}</li>
 *   <li>{@link aatl.impl.PatternElementImpl#getTypeName <em>Type Name</em>}</li>
 *   <li>{@link aatl.impl.PatternElementImpl#getTypeModelName <em>Type Model Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PatternElementImpl extends MinimalEObjectImpl.Container implements PatternElement {
	/**
	 * The default value of the '{@link #getVarName() <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getVarName()
	 * @generated
	 * @ordered
	 */
	protected static final String VAR_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVarName() <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getVarName()
	 * @generated
	 * @ordered
	 */
	protected String varName = VAR_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeName() <em>Type Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTypeName()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeName() <em>Type Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTypeName()
	 * @generated
	 * @ordered
	 */
	protected String typeName = TYPE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeModelName() <em>Type Model Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTypeModelName()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_MODEL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeModelName() <em>Type Model Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTypeModelName()
	 * @generated
	 * @ordered
	 */
	protected String typeModelName = TYPE_MODEL_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AatlPackage.Literals.PATTERN_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getVarName() {
		return varName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarName(String newVarName) {
		String oldVarName = varName;
		varName = newVarName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AatlPackage.PATTERN_ELEMENT__VAR_NAME, oldVarName, varName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeName(String newTypeName) {
		String oldTypeName = typeName;
		typeName = newTypeName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AatlPackage.PATTERN_ELEMENT__TYPE_NAME, oldTypeName, typeName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeModelName() {
		return typeModelName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeModelName(String newTypeModelName) {
		String oldTypeModelName = typeModelName;
		typeModelName = newTypeModelName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AatlPackage.PATTERN_ELEMENT__TYPE_MODEL_NAME, oldTypeModelName, typeModelName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AatlPackage.PATTERN_ELEMENT__VAR_NAME:
				return getVarName();
			case AatlPackage.PATTERN_ELEMENT__TYPE_NAME:
				return getTypeName();
			case AatlPackage.PATTERN_ELEMENT__TYPE_MODEL_NAME:
				return getTypeModelName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AatlPackage.PATTERN_ELEMENT__VAR_NAME:
				setVarName((String)newValue);
				return;
			case AatlPackage.PATTERN_ELEMENT__TYPE_NAME:
				setTypeName((String)newValue);
				return;
			case AatlPackage.PATTERN_ELEMENT__TYPE_MODEL_NAME:
				setTypeModelName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AatlPackage.PATTERN_ELEMENT__VAR_NAME:
				setVarName(VAR_NAME_EDEFAULT);
				return;
			case AatlPackage.PATTERN_ELEMENT__TYPE_NAME:
				setTypeName(TYPE_NAME_EDEFAULT);
				return;
			case AatlPackage.PATTERN_ELEMENT__TYPE_MODEL_NAME:
				setTypeModelName(TYPE_MODEL_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AatlPackage.PATTERN_ELEMENT__VAR_NAME:
				return VAR_NAME_EDEFAULT == null ? varName != null : !VAR_NAME_EDEFAULT.equals(varName);
			case AatlPackage.PATTERN_ELEMENT__TYPE_NAME:
				return TYPE_NAME_EDEFAULT == null ? typeName != null : !TYPE_NAME_EDEFAULT.equals(typeName);
			case AatlPackage.PATTERN_ELEMENT__TYPE_MODEL_NAME:
				return TYPE_MODEL_NAME_EDEFAULT == null ? typeModelName != null : !TYPE_MODEL_NAME_EDEFAULT.equals(typeModelName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (varName: ");
		result.append(varName);
		result.append(", typeName: ");
		result.append(typeName);
		result.append(", typeModelName: ");
		result.append(typeModelName);
		result.append(')');
		return result.toString();
	}

} // PatternElementImpl
