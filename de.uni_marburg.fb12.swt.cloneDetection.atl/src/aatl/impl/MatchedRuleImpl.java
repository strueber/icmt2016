/**
 */
package aatl.impl;

import aatl.AatlPackage;
import aatl.InPattern;
import aatl.MatchedRule;
import aatl.ModulePartVisitor;
import aatl.OutPattern;
import aatl.Variable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Matched Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link aatl.impl.MatchedRuleImpl#getInPattern <em>In Pattern</em>}</li>
 *   <li>{@link aatl.impl.MatchedRuleImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link aatl.impl.MatchedRuleImpl#getOutPattern <em>Out Pattern</em>}</li>
 *   <li>{@link aatl.impl.MatchedRuleImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchedRuleImpl extends ModuleElementImpl implements MatchedRule {
	/**
	 * The cached value of the '{@link #getInPattern() <em>In Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInPattern()
	 * @generated
	 * @ordered
	 */
	protected InPattern inPattern;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable> variables;

	/**
	 * The cached value of the '{@link #getOutPattern() <em>Out Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutPattern()
	 * @generated
	 * @ordered
	 */
	protected OutPattern outPattern;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchedRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AatlPackage.Literals.MATCHED_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InPattern getInPattern() {
		return inPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInPattern(InPattern newInPattern, NotificationChain msgs) {
		InPattern oldInPattern = inPattern;
		inPattern = newInPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AatlPackage.MATCHED_RULE__IN_PATTERN, oldInPattern, newInPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInPattern(InPattern newInPattern) {
		if (newInPattern != inPattern) {
			NotificationChain msgs = null;
			if (inPattern != null)
				msgs = ((InternalEObject)inPattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AatlPackage.MATCHED_RULE__IN_PATTERN, null, msgs);
			if (newInPattern != null)
				msgs = ((InternalEObject)newInPattern).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AatlPackage.MATCHED_RULE__IN_PATTERN, null, msgs);
			msgs = basicSetInPattern(newInPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AatlPackage.MATCHED_RULE__IN_PATTERN, newInPattern, newInPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variable> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<Variable>(Variable.class, this, AatlPackage.MATCHED_RULE__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutPattern getOutPattern() {
		return outPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutPattern(OutPattern newOutPattern, NotificationChain msgs) {
		OutPattern oldOutPattern = outPattern;
		outPattern = newOutPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AatlPackage.MATCHED_RULE__OUT_PATTERN, oldOutPattern, newOutPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutPattern(OutPattern newOutPattern) {
		if (newOutPattern != outPattern) {
			NotificationChain msgs = null;
			if (outPattern != null)
				msgs = ((InternalEObject)outPattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AatlPackage.MATCHED_RULE__OUT_PATTERN, null, msgs);
			if (newOutPattern != null)
				msgs = ((InternalEObject)newOutPattern).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AatlPackage.MATCHED_RULE__OUT_PATTERN, null, msgs);
			msgs = basicSetOutPattern(newOutPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AatlPackage.MATCHED_RULE__OUT_PATTERN, newOutPattern, newOutPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AatlPackage.MATCHED_RULE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AatlPackage.MATCHED_RULE__IN_PATTERN:
				return basicSetInPattern(null, msgs);
			case AatlPackage.MATCHED_RULE__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case AatlPackage.MATCHED_RULE__OUT_PATTERN:
				return basicSetOutPattern(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AatlPackage.MATCHED_RULE__IN_PATTERN:
				return getInPattern();
			case AatlPackage.MATCHED_RULE__VARIABLES:
				return getVariables();
			case AatlPackage.MATCHED_RULE__OUT_PATTERN:
				return getOutPattern();
			case AatlPackage.MATCHED_RULE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AatlPackage.MATCHED_RULE__IN_PATTERN:
				setInPattern((InPattern)newValue);
				return;
			case AatlPackage.MATCHED_RULE__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends Variable>)newValue);
				return;
			case AatlPackage.MATCHED_RULE__OUT_PATTERN:
				setOutPattern((OutPattern)newValue);
				return;
			case AatlPackage.MATCHED_RULE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AatlPackage.MATCHED_RULE__IN_PATTERN:
				setInPattern((InPattern)null);
				return;
			case AatlPackage.MATCHED_RULE__VARIABLES:
				getVariables().clear();
				return;
			case AatlPackage.MATCHED_RULE__OUT_PATTERN:
				setOutPattern((OutPattern)null);
				return;
			case AatlPackage.MATCHED_RULE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AatlPackage.MATCHED_RULE__IN_PATTERN:
				return inPattern != null;
			case AatlPackage.MATCHED_RULE__VARIABLES:
				return variables != null && !variables.isEmpty();
			case AatlPackage.MATCHED_RULE__OUT_PATTERN:
				return outPattern != null;
			case AatlPackage.MATCHED_RULE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public void accept(ModulePartVisitor visitor) {
		visitor.visit(this);
		if (inPattern != null) {
			inPattern.accept(visitor);
		}
		if(variables != null) {
			for(Variable var : variables) 
	            var.accept(visitor);			
		}
		if (outPattern != null) {
			outPattern.accept(visitor);
		}
	}

} //MatchedRuleImpl
