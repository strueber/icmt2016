package de.uni_marburg.fb12.swt.cloneDetection.atl;

public class CloneMetricResults {
	private int numberOfModules = 0;
	private int numberOfRules = 0;
	private int numberOfFilters = 0;
	private int numberOfVariables = 0;
	private int numberOfInElements = 0;
	private int numberOfOutElements = 0;
	private int numberOfBindings = 0;
	
	public CloneMetricResults(int numberOfModules, int numberOfRules, int numberOfFilters, int numberOfVariables,
			int numberOfInElements, int numberOfOutElements, int numberOfBindings) {
		super();
		this.numberOfModules = numberOfModules;
		this.numberOfRules = numberOfRules;
		this.numberOfFilters = numberOfFilters;
		this.numberOfVariables = numberOfVariables;
		this.numberOfInElements = numberOfInElements;
		this.numberOfOutElements = numberOfOutElements;
		this.numberOfBindings = numberOfBindings;
	}

	public int getNumberOfModules() {
		return numberOfModules;
	}

	public int getNumberOfRules() {
		return numberOfRules;
	}

	public int getNumberOfFilters() {
		return numberOfFilters;
	}
	
	public int getNumberOfVariables() {
		return numberOfVariables;
	}

	public int getNumberOfInElements() {
		return numberOfInElements;
	}

	public int getNumberOfOutElements() {
		return numberOfOutElements;
	}

	public int getNumberOfBindings() {
		return numberOfBindings;
	}
	
	public int getSize() {
		return numberOfInElements+numberOfFilters+numberOfOutElements+numberOfVariables +numberOfBindings;
	}
	
	@Override
	public String toString() {
		String result = "\t"+ numberOfModules + "\t" + numberOfRules + "\t" + numberOfInElements + "\t" + numberOfFilters + "\t" + numberOfVariables + "\t"
				+ + numberOfOutElements + "\t" + numberOfBindings;
		return result;
	}
}
