package de.uni_marburg.fb12.swt.cloneDetection.atl;

import java.util.List;

import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroup;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CloneMatrix;

public class CloneComputationUtil {

	public static CloneMatrix getLargest(List<CloneMatrix> list) {
		if (list.isEmpty())
			return new CloneMatrix();

		CloneMatrix largest = list.get(0);
		int maxSize = largest.getSize();

		for (CloneMatrix cg : list) {
			if (cg.getSize() > maxSize) {
				largest = cg;
				maxSize = cg.getSize();
			}
		}

		return largest;
	}

	/**
	 * 
	 * @param cloneGroups
	 * @param dummy
	 *            Dummy parameter to circumvent Java's type erasure.
	 * @return
	 */
	public static CloneGroup getLargest(List<CloneGroup> cloneGroups, boolean dummy) {
		if (cloneGroups.isEmpty())
			return null;

		CloneGroup largest = cloneGroups.get(0);
		int maxSize = largest.getSize();

		for (CloneGroup cg : cloneGroups) {
			if (cg.getSize() > maxSize) {
				largest = cg;
				maxSize = cg.getSize();
			}
		}

		System.out.println(largest);
		return largest;
	}

	public static CloneMatrix getBroadest(List<CloneMatrix> cloneGroups) {
		if (cloneGroups.isEmpty())
			return new CloneMatrix();
		CloneMatrix broadest = cloneGroups.get(0);
		int maxBroadness = broadest.toMetrics().getNumberOfRules();
		int maxSize = broadest.getSize();

		for (CloneMatrix cg : cloneGroups) {
			if (cg.toMetrics().getNumberOfRules() > maxBroadness) {
				broadest = cg;
				maxBroadness = broadest.toMetrics().getNumberOfRules();
				maxSize = broadest.getSize();
			} else if (cg.toMetrics().getNumberOfRules() == maxBroadness) {
				if (cg.getSize() > maxSize) { // size is secondary criterion
					broadest = cg;
					maxBroadness = broadest.toMetrics().getNumberOfRules();
					maxSize = broadest.getSize();
				}
			}
		}

		return broadest;
	}

	/**
	 * 
	 * @param cloneGroups
	 * @param dummy
	 *            Dummy parameter to circumvent Java's type erasure.
	 * @return
	 */
	public static CloneGroup getBroadest(List<CloneGroup> cloneGroups, boolean dummy) {
		if (cloneGroups.isEmpty())
			return null;
		CloneGroup broadest = cloneGroups.get(0);
		int maxBroadness = broadest.toMetrics().getNumberOfRules();
		int maxSize = broadest.getSize();

		for (CloneGroup cg : cloneGroups) {
			if (cg.toMetrics().getNumberOfRules() > maxBroadness) {
				broadest = cg;
				maxBroadness = broadest.toMetrics().getNumberOfRules();
				maxSize = broadest.getSize();
			} else if (cg.toMetrics().getNumberOfRules() == maxBroadness) {
				if (cg.getSize() > maxSize) { // size is secondary criterion
					broadest = cg;
					maxBroadness = broadest.toMetrics().getNumberOfRules();
					maxSize = broadest.getSize();
				}
			}
		}

		return broadest;
	}

}
