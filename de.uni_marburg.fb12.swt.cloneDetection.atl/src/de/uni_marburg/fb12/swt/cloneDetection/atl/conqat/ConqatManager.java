package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.conqat.engine.core.logging.ELogLevel;
import org.conqat.engine.core.logging.IConQATLogger;
import org.conqat.engine.model_clones.detection.ModelCloneReporterMock;
import org.conqat.engine.model_clones.detection.ModelCloneReporterMock.ModelClone;
import org.conqat.engine.model_clones.detection.clustering.CloneClusterer;
import org.conqat.engine.model_clones.detection.pairs.PairDetector;
import org.conqat.engine.model_clones.detection.util.AugmentedModelGraph;
import org.conqat.engine.model_clones.model.IDirectedEdge;
import org.conqat.engine.model_clones.model.IModelGraph;
import org.conqat.engine.model_clones.model.INode;
import org.eclipse.emf.ecore.EObject;

import aatl.MatchedRule;
import aatl.Module;
import de.uni_marburg.fb12.swt.cloneDetection.atl.Link;

/**
 * Manages the clone detection process based on ConQat: Creates and maintains a
 * mapping between the MatchedRule representation and the IModelGraph
 * representation required by ConQat, triggers the creation of the results and
 * creates MatchedRule-based representations of the results.
 * 
 * @author strueber
 *
 */
public class ConqatManager {

	private static final int SETTINGS_MIN_CLONE_SIZE = 3;
	private static final int SETTINGS_MIN_CLONE_WEIGHT = 1;
	private static final int SETTINGS_MIN_FREQ = 1;

	private List<MatchedRule> ruleGraphs;
	private IModelGraph modelGraph;
	private AAtlToConqatMap aatl2conqatMap = new AAtlToConqatMap();
	private ModelCloneReporterMock resultReporter;

	public ConqatManager(List<MatchedRule> ruleGraphs) {
		this.ruleGraphs = ruleGraphs;
	}

	public void doCloneDetection() {
		modelGraph = createModelGraph();
		try {
			resultReporter = runDetection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private IModelGraph createModelGraph() {
		return new AAtlToConqatGraphConverter(ruleGraphs, aatl2conqatMap).createConqatGraph();
	}

	protected ModelCloneReporterMock runDetection() throws Exception {
		int minCloneSize = SETTINGS_MIN_CLONE_SIZE;
		int minCloneWeight = SETTINGS_MIN_CLONE_WEIGHT;
		AugmentedModelGraph mag = new AugmentedModelGraph(modelGraph);
		ModelCloneReporterMock result = new ModelCloneReporterMock();
		IConQATLogger logger = createDummyLogger();
		CloneClusterer clusterer = new CloneClusterer(mag, result, logger, false);
		new PairDetector(mag, minCloneSize, minCloneWeight, false, clusterer, logger).execute();
		clusterer.performInclusionAnalysis();
		clusterer.performClustering();
		return result;
	}

	public ModelCloneReporterMock getResultReporter() {
		return resultReporter;
	}

	public List<MatchedRule> getInvolvedMatchedRules(ModelClone clone) {
		List<MatchedRule> result = new ArrayList<MatchedRule>();
		for (List<INode> nodeList : clone.nodes) {
			for (INode node : nodeList) {
				EObject container = aatl2conqatMap.get(node);
				while (!(container instanceof MatchedRule) && container != null) {
					container = container.eContainer();
					if (container instanceof MatchedRule && !result.contains(container)) {
						result.add((MatchedRule) container);
					}
				}
			}
		}
		return result;
	}

	public List<Module> getInvolvedModules(ModelClone clone) {
		List<Module> result = new ArrayList<Module>();
		for (List<INode> nodeList : clone.nodes) {
			for (INode node : nodeList) {
				EObject container = aatl2conqatMap.get(node);
				while (!(container instanceof Module) && container != null) {
					container = container.eContainer();
					if (container instanceof Module && !result.contains(container)) {
						result.add((Module) container);
					}
				}
			}
		}
		return result;
	}

	public EObject getObject(INode node) {
		return aatl2conqatMap.get(node);
	}

	/**
	 * Creates a dummy logger that won't perform any actions. This is to keep
	 * the console clean from ConQAT-related entries.
	 * 
	 * @return
	 */
	private IConQATLogger createDummyLogger() {
		return new IConQATLogger() {
			public void debug(Object arg0) {
			}

			public void debug(Object arg0, Throwable arg1) {
			}

			public void error(Object arg0) {
			}

			public void error(Object arg0, Throwable arg1) {
			}

			public void info(Object arg0) {
			}

			public void info(Object arg0, Throwable arg1) {
			}

			public void warn(Object arg0) {
			}

			public void warn(Object arg0, Throwable arg1) {
			}

			public ELogLevel getMinLogLevel() {
				return null;
			}

			public void log(ELogLevel arg0, Object arg1) {

			}

			public void log(ELogLevel arg0, Object arg1, Throwable arg2) {
			}

		};
	}

	public Map<EObject, Set<EObject>> createNodeMappings(ModelClone clone) {
		Map<EObject, Set<EObject>> result = new HashMap<EObject, Set<EObject>>();

		List<INode> firstCloneInstance = clone.nodes.iterator().next();
		int numberOfNodesInClone = firstCloneInstance.size();
		List<Set<EObject>> indexedList = new ArrayList<Set<EObject>>(numberOfNodesInClone);
		for (int i = 0; i < numberOfNodesInClone; i++)
			indexedList.add(new HashSet<EObject>());

		for (List<INode> nodes : clone.nodes) {
			for (int i = 0; i < numberOfNodesInClone; i++) {
				Set<EObject> group = indexedList.get(i);
				EObject o = aatl2conqatMap.get(nodes.get(i));
				group.add(o);
				result.put(o, group);
			}
		}

		return result;
	}

	 public Map<Link, Set<Link>> createLinkMappings(ModelClone clone) {
	 Map<Link, Set<Link>> result = new HashMap<Link, Set<Link>>();
	 
		List<IDirectedEdge> firstCloneInstance = clone.edges.iterator().next();
		int numberOfEdgesInClone = firstCloneInstance.size();
		List<Set<Link>> indexedList = new ArrayList<Set<Link>>(numberOfEdgesInClone);
		for (int i = 0; i < numberOfEdgesInClone; i++)
			indexedList.add(new HashSet<Link>());

		for (List<IDirectedEdge> edges : clone.edges) {
			for (int i = 0; i < numberOfEdgesInClone; i++) {
				Set<Link> group = indexedList.get(i);
				Link l = aatl2conqatMap.get(edges.get(i));
				group.add(l);
				result.put(l, group); 
			}
		}

	 return result;
	 }
	
	// public Map<EObject, Set<EObject>> createNodeMappings(ModelClone clone) {
	// Map<EObject, Set<EObject>> result = new HashMap<EObject, Set<EObject>>();
	//
	// for (List<INode> nodes : clone.nodes) {
	// Set<EObject> group = new HashSet<EObject>();
	// for (INode n : nodes) {
	// EObject o = aatl2conqatMap.get(n);
	// group.add(o);
	// result.put(o, group);
	// }
	// }
	// return result;
	// }
	//
	// public Map<Link, Set<Link>> createLinkMappings(ModelClone clone) {
	// Map<Link, Set<Link>> result = new HashMap<Link, Set<Link>>();
	// for (List<IDirectedEdge> edges : clone.edges) {
	// Set<Link> group = new HashSet<Link>();
	// for (IDirectedEdge e : edges) {
	// Link l = aatl2conqatMap.get(e);
	// group.add(l);
	// result.put(l, group);
	// }
	// }
	// return result;
	// }

}
