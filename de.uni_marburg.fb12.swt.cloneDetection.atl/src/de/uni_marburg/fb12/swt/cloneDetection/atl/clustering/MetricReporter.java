package de.uni_marburg.fb12.swt.cloneDetection.atl.clustering;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import aatl.Binding;
import aatl.InPatternElement;
import aatl.MatchedRule;
import aatl.Module;
import aatl.ModuleElement;
import aatl.OutPatternElement;

public class MetricReporter {
	private List<Module> modules;
	private String name = new String();

	private int numberOfModules = 0;
	private int numberOfRules = 0;
	private int numberOfFilters = 0;
	private int numberOfVariables = 0;
	private int numberOfInElements = 0;
	private int numberOfOutElements = 0;
	private int numberOfBindings = 0;

	public MetricReporter(List<Module> modules) {
		this.modules = modules;
	}

	public MetricReporter(Set<Module> modules) {
		this.modules = modules.stream().collect(Collectors.toList());
		;
	}

	public String getMetricReport(String separator) {
		setName();
		computeModuleMetrics();
		return toString(separator);
	}

	private void setName() {
		Iterator<Module> it = modules.iterator();
		while (it.hasNext()) {
			Module mod = it.next();
			name += mod.getName();
			if (it.hasNext())
				name += " ";
		}
	}

	private void computeModuleMetrics() {
		numberOfModules = modules.size();
		for (Module module : modules) {
			for (ModuleElement el : module.getElements()) {
				if (el instanceof MatchedRule) {
					numberOfRules++;
					computeRuleMetrics((MatchedRule) el);
				}
			}
		}
	}

	private void computeRuleMetrics(MatchedRule el) {
		if (el.getInPattern() != null) {
			numberOfInElements += el.getInPattern().getElements().size();
			numberOfFilters += (el.getInPattern().getFilter() != null) ? 1 : 0;
		}
		numberOfVariables += el.getVariables().size();
		if (el.getOutPattern() != null) {
			numberOfOutElements += el.getOutPattern().getElements().size();
			for (OutPatternElement outEl : el.getOutPattern().getElements())
				numberOfBindings += outEl.getBindings().size();
		}
	}

	public String toString(String separator) {
		return name + separator + numberOfModules + separator + numberOfRules + separator + numberOfInElements
				+ separator + numberOfFilters + separator + numberOfVariables + separator + numberOfOutElements
				+ separator + numberOfBindings;
	}
}
