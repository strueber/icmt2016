package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import java.util.List;

public class CloneGroupDetectionResult {
	List<CloneGroup> cloneGroups;

	public CloneGroupDetectionResult(List<CloneGroup> cloneGroups) {
		this.cloneGroups = cloneGroups;
	}

	public List<CloneGroup> getCloneGroups() {
		return cloneGroups;
	}
}
