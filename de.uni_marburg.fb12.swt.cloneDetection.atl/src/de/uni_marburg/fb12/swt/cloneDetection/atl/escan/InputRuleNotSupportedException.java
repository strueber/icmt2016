package de.uni_marburg.fb12.swt.cloneDetection.atl.escan;


public class InputRuleNotSupportedException extends Exception {
	String message; 
	
	public InputRuleNotSupportedException(String string) {
		message = string;
	}
	
	@Override
	public String getMessage() {
		return message;
	}

}
