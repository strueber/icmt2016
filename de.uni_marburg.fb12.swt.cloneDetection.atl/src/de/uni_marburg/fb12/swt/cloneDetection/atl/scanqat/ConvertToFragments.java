package de.uni_marburg.fb12.swt.cloneDetection.atl.scanqat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.uni_marburg.fb12.swt.cloneDetection.atl.Link;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroup;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroupDetectionResult;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.Fragment;

import org.eclipse.emf.ecore.EObject;
import org.jgrapht.DirectedGraph;

import aatl.MatchedRule;

public class ConvertToFragments {
	//
	public static Set<Set<Fragment>> convertCloneGroupsToFragments(CloneGroupDetectionResult inputResult,
			Map<MatchedRule, DirectedGraph<EObject, CapsuleEdge>> ruleGraphMap) {
		Set<Set<Fragment>> result = new HashSet<Set<Fragment>>();
		for (CloneGroup entry : inputResult.getCloneGroups()) {
			HashMap<MatchedRule, Fragment> rule2fragment = new HashMap<MatchedRule, Fragment>();
			for (MatchedRule r : entry.getInvolvedRules()) {
				Fragment f = new Fragment(new HashSet<CapsuleEdge>(), r, ruleGraphMap.get(r));
				rule2fragment.put(r, f);
			}

			for (Link l : entry.getLinkMappings().keySet()) {
				Fragment f = rule2fragment.get(l.getRule());
				DirectedGraph<EObject, CapsuleEdge> escanGraph = f.getGraph();
				EObject source = l.getSource();
				EObject target = l.getTarget();
				CapsuleEdge capsuleEdge = null;
				for (CapsuleEdge c : escanGraph.getAllEdges(source, target)) {
					if (c.getOriginalEdge().equals(l)) {
						capsuleEdge = c;
					}
					break;
				}
				if (capsuleEdge != null)
					f.getCapsuleEdges().add(capsuleEdge);
				else
					System.err.println("Found edge with no corresponding CapsuleEdge!");
			}

			Set<Fragment> fragments = new HashSet<Fragment>();
			for (MatchedRule r : rule2fragment.keySet()) {
				fragments.add(rule2fragment.get(r));
			}
			result.add(fragments);

		}

		return result;
	}

	// public Set<Fragment> getAsFragments(
	// Map<MatchedRule, DirectedGraph<EObject, CapsuleEdge>> ruleGraphMap) {
	// if (capsuleEdges1.size() == 0) {
	// return null;
	// }
	// Set<Fragment> res = new HashSet<Fragment>();
	//
	// MatchedRule rule1 = capsuleEdges1.get(0).getMatchedRule();
	// MatchedRule rule2 = capsuleEdges2.get(0).getMatchedRule();
	// Set<CapsuleEdge> capsuleEdgeSet1 = new HashSet<CapsuleEdge>();
	// Set<CapsuleEdge> capsuleEdgeSet2 = new HashSet<CapsuleEdge>();
	// capsuleEdgeSet1.addAll(capsuleEdges1);
	// capsuleEdgeSet2.addAll(capsuleEdges2);
	// DirectedGraph<EObject, CapsuleEdge> graph1neu = ruleGraphMap.get(rule1);
	// DirectedGraph<EObject, CapsuleEdge> graph2neu = ruleGraphMap.get(rule2);
	// Fragment f1 = new Fragment(capsuleEdgeSet1, rule1, graph1neu);
	// Fragment f2 = new Fragment(capsuleEdgeSet2, rule2, graph2neu);
	// res.add(f1);
	// res.add(f2);
	// return res;
	// }

}
