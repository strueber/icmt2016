package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import org.conqat.engine.model_clones.model.IModelGraph;
import org.conqat.engine.model_clones.model.INode;

/**
 * Represents a Henshin Node in the {@link IModelGraph} graph representation required by
 * ConQAT.
 * 
 * @author strueber
 *
 */
public class ConqatNode implements INode {
	String label;

	public ConqatNode(String label) {
		this.label = label;
	}

	@Override
	public String getEquivalenceClassLabel() {
		return label;
	}

	@Override
	public int getWeight() {
		return 1;
	}

}
