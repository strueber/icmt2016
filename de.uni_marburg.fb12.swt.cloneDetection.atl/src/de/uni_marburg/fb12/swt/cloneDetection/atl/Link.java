package de.uni_marburg.fb12.swt.cloneDetection.atl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import aatl.MatchedRule;

public class Link {
	private EObject source;
	private EObject target;
	private EReference reference;
	private MatchedRule rule;

	public MatchedRule getRule() {
		return rule;
	}

	public void setRule(MatchedRule rule) {
		this.rule = rule;
	}

//	public Link(EObject source, EObject target, EReference reference) {
//		super();
//		this.source = source;
//		this.target = target;
//		this.reference = reference;
//	}

	public Link(EObject source, EObject target, EReference reference, MatchedRule rule) {
		super();
		this.source = source;
		this.target = target;
		this.reference = reference;
		this.rule = rule;
	}
	
	public EObject getSource() {
		return source;
	}

	public EObject getTarget() {
		return target;
	}

	public EReference getReference() {
		return reference;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((reference == null) ? 0 : reference.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Link other = (Link) obj;
		if (reference == null) {
			if (other.reference != null)
				return false;
		} else if (!reference.equals(other.reference))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}
}
