package de.uni_marburg.fb12.swt.cloneDetection.atl.escan;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.jgrapht.DirectedGraph;

import aatl.MatchedRule;

public class EScanDetection3D extends EScanDetection3DAbstract {
	public EScanDetection3D(List<MatchedRule> rules) {
		super(rules);
	}

	public EScanDetection3D(
			Map<MatchedRule, DirectedGraph<EObject, CapsuleEdge>> ruleGraphMap,
			List<MatchedRule> ruleList) {
		super(ruleGraphMap, ruleList);
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		if (DEBUG) System.out
				.println(startDetectCloneGroups("EScanDetection3DIn1Step (CloneDetection"
						+ " directly with Attributes)"));
		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithAttributes(rules);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		if (edgesLayer1.size() == 0) {
			resultAsCloneMatrix = null;
		} else {
			Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1);
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
					layer1, edgesLayer1);

			Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
			if (DEBUG) System.out.println(startConversion());
			resultAsCloneMatrix = CloneMatrixCreator
					.convertEScanResult(cloneGroups);
			if (DEBUG) System.out.println(endDetectCloneGroups("EScanDetection3DIn1Step",
					startZeit));
		}

	}

	@Override
	public void detectCloneGroups(Map<Fragment, List<Set<Fragment>>> topLayer) {

		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithAttributes(rules);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
				topLayer, edgesLayer1);

		Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
		if (DEBUG) System.out.println(startConversion());
		resultAsCloneMatrix = CloneMatrixCreator
				.convertEScanResult(cloneGroups);

	}

	public Set<Set<Fragment>> getCloneGroups() {
		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithAttributes(rules);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		if (edgesLayer1.size() == 0) {
			return null;
		} else {
			Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1);
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
					layer1, edgesLayer1);

			Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
			return cloneGroups;
		}
	}

	public Set<Set<Fragment>> getCloneGroups(
			List<Set<CapsuleEdge>> graphsFragmentsSetList) {
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsFragmentsSetList);

		if (edgesLayer1.size() == 0) {
			return null;
		} else {
			Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1);
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
					layer1, edgesLayer1);

			Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
			return cloneGroups;
		}
	}

	private List<Set<CapsuleEdge>> getGraphsEdgeSetListWithAttributes(
			List<MatchedRule> ruleList) {
		List<Set<CapsuleEdge>> graphsEdgeSetList = new LinkedList<Set<CapsuleEdge>>();
		for (MatchedRule rule : ruleList) {
			graphsEdgeSetList.add(ruleGraphMap.get(rule).edgeSet());
		}
		return graphsEdgeSetList;
	}

}
