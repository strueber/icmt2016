package de.uni_marburg.fb12.swt.cloneDetection.atl.scanqat;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import aatl.MatchedRule;
import aatl.Module;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroupDetectionResult;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.ConqatBasedCloneGroupDetector;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.EScanDetection3D;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.EScanDetectionInc;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.Fragment;
/**
 * 
 * Contains the CombineAlgorithm consisting of CloneDetection and
 * ModelCd-eScanInc
 *
 */
public class ScanQatBasedCloneDetector extends HybridCloneDetector {

	public ScanQatBasedCloneDetector(List<MatchedRule> rules) {
		super(rules);
	}
	
	public ScanQatBasedCloneDetector(Set<Module> modules) {
		super(modules);
	}


	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		if (DEBUG) System.out
				.println(startDetectCloneGroups("CombineDetectionCDwithEScanInc"));

		ConqatBasedCloneGroupDetector cq = new ConqatBasedCloneGroupDetector(
				(List<MatchedRule>) rules);
		cq.detectCloneGroups();
		
		if (DEBUG) System.out.println(startConversionTempResult("CloneDetection"));
		
		Set<Set<Fragment>> cloneGroups = ConvertToFragments.convertCloneGroupsToFragments(cq.getResultOrderedBySize(), ruleGraphMap);
		
		resultAsCloneMatrix.clear();
		if (DEBUG) System.out.println(startConversion());
		Set<Set<MatchedRule>> combinations = getCombinations(cloneGroups);
		
		for (Set<MatchedRule> combination : combinations) {

			Set<Fragment> simpleTopLayer = getMaxSizedFragmentSetThatContainsOneFragmentOfEachRule(
					cloneGroups, combination);
			
			if (simpleTopLayer != null) {
				List<Set<Fragment>> lattice = CreateNewLatticeUtility.createLattice(simpleTopLayer);

				if (DEBUG) System.out.println(moveOnToSecondAlgortihm("ModelCd-eScanInc"));
				boolean ignoreIsGeneratingParent = true;
				EScanDetectionInc eScanDetection = new EScanDetectionInc(
						ruleGraphMap, rules, 
						ignoreIsGeneratingParent);
				int fragmentSize = lattice.get(lattice.size() - 1).iterator()
						.next().size();
				eScanDetection.detectCloneGroups(fragmentSize, lattice);

				
				Set<CloneMatrix> eScanIncResult = eScanDetection.getResultAsCloneMatrix();
				for (CloneMatrix m : eScanIncResult) {
						resultAsCloneMatrix.add(m);
				}

			}
			// else {
			// Set<Fragment> topLayer = createTopLayer(resConvert);
			// lattice = CreateNewLatticeUtility.createLattice(topLayer);
			// }
		}

		if (DEBUG) System.out.println(endDetectCloneGroups(
				"CombineDetectionCDwithEScanInc", startZeit));
	}

	private Set<Set<MatchedRule>> getCombinations(Set<Set<Fragment>> cloneGroups) {
		Set<Set<MatchedRule>> result = new HashSet<Set<MatchedRule>>();
		for (Set<Fragment> fragments : cloneGroups) {
			Set<MatchedRule> rules = new HashSet<MatchedRule>();
			for (Fragment f: fragments) {
				rules.add(f.getRule());
			}
			
			if (!result.contains(rules))
				result.add(rules);
		}
		return result;
	}

	private Set<Fragment> createTopLayer(Map<Integer, Set<Fragment>> resConvert) {

		Set<Fragment> maxFragments = CreateNewLatticeUtility
				.getMaxFragments(resConvert);
		List<Set<CapsuleEdge>> graphsFragmentsSetList = CreateNewLatticeUtility
				.getFragmentsEdgeSetListWithAttributes(maxFragments, rules);

		EScanDetection3D eScan3d = new EScanDetection3D(ruleGraphMap, rules);
		Set<Set<Fragment>> cloneGroups = eScan3d
				.getCloneGroups(graphsFragmentsSetList);
		if (cloneGroups == null) {
			return null;
		}
		// find maxSized CloneGroup
		Set<Fragment> res = cloneGroups.iterator().next();
		for (Set<Fragment> cloneGroup : cloneGroups) {
			int size = cloneGroup.iterator().next().size();
			if (size > res.iterator().next().size()) {
				res = cloneGroup;
			}
		}
		return res;
	}

	private Set<Fragment> getMaxSizedFragmentSetThatContainsOneFragmentOfEachRule(
			Set<Set<Fragment>> cloneGroups, Set<MatchedRule> set) {
		Set<Set<Fragment>> candidates = new HashSet<Set<Fragment>>();
		for (Set<Fragment> cloneGroup : cloneGroups) {
			Set<MatchedRule> rules = new HashSet<MatchedRule>();
			for (Fragment f : cloneGroup) {
				rules.add(f.getRule());
			}
			if (rules.equals(set))
				candidates.add(cloneGroup);
			
// old impl.
//			if (cloneGroup.size() == set.size()) {
//				// one Fragment per Rule?
//				boolean doubleRule = false;
//				Set<MatchedRule> rules = new HashSet<MatchedRule>();
//				for (Fragment fragment : cloneGroup) {
//					if (!rules.contains(fragment.getRule())) {
//						rules.add(fragment.getRule());
//					} else {
//						doubleRule = true;
//					}
//				}
//				if (!doubleRule) {
//					candidates.add(cloneGroup);
//				}
//			}
		}
		if (candidates.size() == 0) {
			return null;
		}
		// find maximal one
		int max = 0;
		Set<Fragment> res = candidates.iterator().next();
		for (Set<Fragment> candidate : candidates) {
			if (max < candidate.iterator().next().size()) {
				max = candidate.iterator().next().size();
				res = candidate;
			}
		}
		return res;
	}

}
