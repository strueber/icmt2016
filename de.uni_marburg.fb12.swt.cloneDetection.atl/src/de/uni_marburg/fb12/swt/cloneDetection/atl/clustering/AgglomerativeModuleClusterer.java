package de.uni_marburg.fb12.swt.cloneDetection.atl.clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import com.apporiented.algorithm.clustering.SingleLinkageStrategy;
import com.apporiented.algorithm.clustering.Cluster;
import com.apporiented.algorithm.clustering.ClusteringAlgorithm;
import com.apporiented.algorithm.clustering.DefaultClusteringAlgorithm;

import aatl.MatchedRule;
import aatl.Module;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroup;

public class AgglomerativeModuleClusterer {

	List<CloneGroup> cloneGroups;
	Map<Module, Map<Module, CloneGroup>> largestClones;
	double largestCloneTotalSize;
	Map<String, Module> nameToModule;
	// Map<String, MatchedRule> nameToRule;

	List<Module> modules;

	double distanceThreshold;
	boolean includeRhs;

	public List<List<Module>> clusterRules(List<CloneGroup> theCloneGroups, double distanceThreshold) {
		this.distanceThreshold = distanceThreshold;
		this.cloneGroups = theCloneGroups;
		initialize();
		List<List<Module>> result = new ArrayList<List<Module>>();
		if (modules.isEmpty())
			return result;

		String[] names = getModuleNames(modules).toArray(new String[modules.size()]);
		double[][] distances = getDistances();
//		 printDistances(names, distances);
		ClusteringAlgorithm alg = new DefaultClusteringAlgorithm();
		Cluster cluster = alg.performClustering(distances, names, new SingleLinkageStrategy());

		addToResult(cluster, result);
		return result;
	}

	private List<String> getModuleNames(List<Module> module) {
		List<String> result = new ArrayList<String>();
		for (Module m : modules)
			result.add(m.getName());
		return result;
	}

	private List<List<Module>> addToResult(Cluster cluster, List<List<Module>> result) {
		if (cluster.getChildren().isEmpty()) {
			List<Module> list = Collections.singletonList(nameToModule.get(cluster.getName()));
			result.add(list);
		} else if (cluster.getDistance().getDistance() < distanceThreshold) {
			List<Module> resultCluster = new ArrayList<Module>();
			result.add(resultCluster);
			addChildrenToOneResultCluster(cluster.getChildren(), resultCluster);
		} else {
			for (Cluster child : cluster.getChildren()) {
				addToResult(child, result);
			}
		}
		return result;
	}

	private void addChildrenToOneResultCluster(List<Cluster> children, List<Module> resultCluster) {
		for (Cluster child : children) {
			if (child.getChildren().isEmpty()) {
				resultCluster.add(nameToModule.get(child.getName()));
			} else {
				addChildrenToOneResultCluster(child.getChildren(), resultCluster);
			}
		}
	}

	private double[][] getDistances() {
		double[][] result = new double[modules.size()][modules.size()];
		for (int i = 0; i < modules.size(); i++) {
			for (int j = i + 1; j < modules.size(); j++) {
				result[i][j] = getDistance(modules.get(i), modules.get(j));
			}
		}
		for (int i = 0; i < modules.size(); i++) {
			for (int j = 0; j < i; j++) {
				result[i][j] = result[j][i];
			}
		}
		for (int i = 0; i < modules.size(); i++) {
			result[i][i] = 0;
		}
		return result;
	}

	private double getDistance(Module m1, Module m2) {
		return 1 / (calculcateSimilarity(m1, m2) + 1);
	}


	public double calculcateSimilarity(Module m1, Module m2) {
		double clonesize = 0;
		if (largestClones.containsKey(m1) && largestClones.get(m1).containsKey(m2)) {
			clonesize = largestClones.get(m1).get(m2).getSize();
		}
//		System.err.println(clonesize);
		return clonesize;
	}

	private void initialize() {
		modules = new ArrayList<Module>();
		largestClones = new HashMap<Module, Map<Module, CloneGroup>>();
		nameToModule = new HashMap<String, Module>();
		for (CloneGroup cg : cloneGroups) {
			addDuplicateFree(modules, cg.getInvolvedModules());
			initializeInnerMaps(largestClones, cg);
			for (MatchedRule r1 : cg.getInvolvedRules()) {
				for (MatchedRule r2 : cg.getInvolvedRules()) {
					Module m1 = r1.getModule();
					Module m2 = r2.getModule();
					if (m1 != m2) {
						CloneGroup existingEntry = largestClones.get(m1).get(m2);
						if (existingEntry == null || cg.getSize() > existingEntry.getSize()) {
							largestClones.get(m1).put(m2, cg);
							largestClones.get(m2).put(m1, cg);
						}

						if (largestCloneTotalSize < cg.getSize()) {
							largestCloneTotalSize = cg.getSize();
						}
					}
				}
			}
		}
	}

	private void addDuplicateFree(List<Module> modules, List<Module> additional) {
		for (Module a : additional) {
			if (!modules.contains(a)) {
				modules.add(a);
				nameToModule.put(a.getName(),a);
			}
		}
	}

	private void initializeInnerMaps(Map<Module, Map<Module, CloneGroup>> outerMap, CloneGroup cg) {
		for (Module r1 : cg.getInvolvedModules()) {
			Map<Module, CloneGroup> innerMap = largestClones.get(r1);
			if (innerMap == null) {
				innerMap = new HashMap<Module, CloneGroup>();
				outerMap.put(r1, innerMap);
			}
		}
	}


	private void printDistances(String[] names, double[][] distances) {
		for (int i = 0; i < names.length; i++) {
			System.out.print(i + " = " + names[i]);
			if (i + 1 < names.length)
				System.out.print(", ");
		}
		System.out.println();
		for (int i = 0; i < distances.length; i++) {
			for (int j = 0; j < distances[i].length; j++) {
				System.out.print(Math.round(distances[i][j] * 100.0) / 100.0 + " ");
			}
			System.out.println();
		}
	}

	public List<List<Module>>  removeSingletonClusters(List<List<Module>> clusters) {
		return clusters.stream().filter(cl -> cl.size() > 1).collect(Collectors.toList());
	}

}
