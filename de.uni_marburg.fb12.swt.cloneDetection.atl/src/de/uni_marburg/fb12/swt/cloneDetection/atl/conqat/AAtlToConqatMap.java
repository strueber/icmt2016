package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import java.util.HashMap;
import java.util.Map;

import org.conqat.engine.model_clones.model.IDirectedEdge;
import org.conqat.engine.model_clones.model.INode;
import org.eclipse.emf.ecore.EObject;

import de.uni_marburg.fb12.swt.cloneDetection.atl.Link;

public class AAtlToConqatMap {
	private Map<EObject, INode> aAtlNodeToINodeMap = new HashMap<EObject, INode>();
	private Map<INode, EObject> iNodeToAAtlNodeMap = new HashMap<INode, EObject>();
	
	private Map<Link, IDirectedEdge> linkToIEdgeMap = new HashMap<Link, IDirectedEdge>();
	private Map<IDirectedEdge, Link> iEdgeToLinkMap = new HashMap<IDirectedEdge, Link>();
	
	public void put(EObject node, INode iNode) {
		aAtlNodeToINodeMap.put(node, iNode);
		iNodeToAAtlNodeMap.put(iNode, node);
	}
	
	public void put(INode iNode, EObject node) {
		aAtlNodeToINodeMap.put(node, iNode);
		iNodeToAAtlNodeMap.put(iNode, node);
	}
	
	public EObject get(INode node) {
		return iNodeToAAtlNodeMap.get(node);
	}
	
	public INode get(EObject node) {
		return aAtlNodeToINodeMap.get(node);
	}
	
	public boolean contains(EObject node) {
		return aAtlNodeToINodeMap.containsKey(node);
	}

	public boolean contains(INode node) {
		return iNodeToAAtlNodeMap.containsKey(node);
	}
	

	public void put(Link link, IDirectedEdge iDirectedEdge) {
		linkToIEdgeMap.put(link, iDirectedEdge);
		iEdgeToLinkMap.put(iDirectedEdge, link);
	}
	
	public void put(IDirectedEdge iDirectedEdge, Link link) {
		linkToIEdgeMap.put(link, iDirectedEdge);
		iEdgeToLinkMap.put(iDirectedEdge, link);
	}
	
	public Link get(IDirectedEdge edge) {
		return iEdgeToLinkMap.get(edge);
	}
	
	public IDirectedEdge get(Link link) {
		return linkToIEdgeMap.get(link);
	}
	
	public boolean contains(Link link) {
		return linkToIEdgeMap.containsKey(link);
	}

	public boolean contains(IDirectedEdge edge) {
		return iEdgeToLinkMap.containsKey(edge);
	}
	
}
