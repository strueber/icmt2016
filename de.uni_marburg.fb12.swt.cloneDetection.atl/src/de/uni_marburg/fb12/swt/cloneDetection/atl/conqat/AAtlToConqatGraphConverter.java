package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import java.util.List;

import org.conqat.engine.model_clones.model.IDirectedEdge;
import org.conqat.engine.model_clones.model.IModelGraph;
import org.conqat.engine.model_clones.model.INode;
import org.conqat.engine.model_clones.model.ModelGraphMock;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import aatl.Binding;
import aatl.Filter;
import aatl.InPattern;
import aatl.InPatternElement;
import aatl.MatchedRule;
import aatl.Module;
import aatl.ModulePartVisitor;
import aatl.OutPattern;
import aatl.OutPatternElement;
import aatl.Variable;
import de.uni_marburg.fb12.swt.cloneDetection.atl.Link;

/**
 * Creates Conquat compatible graph representations of Henshin rule graphs.
 * 
 * @author str�ber
 *
 */
/**
 * @author strueber
 *
 */
public class AAtlToConqatGraphConverter implements ModulePartVisitor {
	AAtlToConqatMap map;
	List<MatchedRule> matchedRules;
	private ModelGraphMock graph;
	private MatchedRule currentRule;
	
	public AAtlToConqatGraphConverter(List<MatchedRule> matchedRules,
			AAtlToConqatMap map) {
		this.matchedRules = matchedRules;
		this.map = map;
	}

	/**
	 * Creates the Conqat graph representation of a set of Henshin Rules, represented by
	 * the input MatchedRule.
	 * 
	 * @param matchedRule
	 * @return
	 */
	public IModelGraph createConqatGraph() {
		IModelGraph resultGraph = new ModelGraphMock();
		for (MatchedRule matchedRule : matchedRules) {
			IModelGraph tempGraph = createConqatGraph(matchedRule);
			resultGraph.getNodes().addAll(tempGraph.getNodes());
			resultGraph.getEdges().addAll(tempGraph.getEdges());
		}
		return resultGraph;
	}
	
	/**
	 * Creates the Conqat graph representation of a Henshin Rule, represented by
	 * the input MatchedRule.
	 * 
	 * @param matchedRule
	 * @return
	 */
	public IModelGraph createConqatGraph(MatchedRule matchedRule) {
		graph = new ModelGraphMock();
		currentRule = matchedRule;
		matchedRule.accept(this);
		return graph;
	}

	@Override
	public void visit(Module module) {
		// Noop: This visitor starts at the rule level.
	}

	@Override
	public void visit(MatchedRule matchedRule) {
		addNodeToGraph("MatchedRule", matchedRule);
	}

	@Override
	public void visit(InPattern inPattern) {
		addNodeToGraph("InPattern", inPattern);
		addContainmentEdgeToGraph(inPattern);
	}

	@Override
	public void visit(OutPattern outPattern) {
		addNodeToGraph("OutPattern", outPattern);
		addContainmentEdgeToGraph(outPattern);
	}

	@Override
	public void visit(Variable variable) {
		String label = "var " + variable.getTypeModelName() + "!" + variable.getTypeName() + " <- " + variable.getInitExpression();
		addNodeToGraph(label, variable);
		addContainmentEdgeToGraph(variable);
	}

	@Override
	public void visit(Filter filter) {
		String label = "filter " +filter.getValue();
		addNodeToGraph(label, filter);
		addContainmentEdgeToGraph(filter);
	}

	@Override
	public void visit(InPatternElement inPatternElement) {
		String label = "in "+inPatternElement.getVarName()+":"+inPatternElement.getTypeModelName() + "!" + inPatternElement.getTypeName();
		addNodeToGraph(label, inPatternElement);
		addContainmentEdgeToGraph(inPatternElement);		
	}

	@Override
	public void visit(OutPatternElement outPatternElement) {
		String label = "out "+ outPatternElement.getVarName()+":"+outPatternElement.getTypeModelName() + "!" + outPatternElement.getTypeName();
		addNodeToGraph(label, outPatternElement);
		addContainmentEdgeToGraph(outPatternElement);	
	}

	@Override
	public void visit(Binding binding) {
		String label = "binding "+binding.getPropertyName()+ " <- " + binding.getValue();
		addNodeToGraph(label,   binding);
		addContainmentEdgeToGraph(binding);			
	}
	


	/**
	 * Adds the given node to the graph.
	 * 
	 * @param theMap
	 */
	private boolean addNodeToGraph(String label, EObject node) {
		INode newNode = new ConqatNode(label);
		graph.getNodes().add(newNode);
		map.put(node, newNode);
		return true;
	}

	private void addContainmentEdgeToGraph(EObject eObject) {
		EObject source = eObject.eContainer();
		EObject target = eObject;
		EReference reference = eObject.eContainmentFeature();
		addEdgeToGraph(reference, source, target);
	}

	/**
	 * Adds the given edge with given source and target nodes in the graph.
	 * 
	 * @param edge
	 * @param source
	 * @param target
	 * @param graph
	 */
	private boolean addEdgeToGraph(EReference reference, EObject source,
			EObject target) {
		INode sourceNode = map.get(source);
		INode targetNode = map.get(target);
		if (sourceNode != null && targetNode != null) {
			IDirectedEdge newEdge = new ConqatEdge(reference.getName(), sourceNode, targetNode);
			graph.getEdges().add(newEdge);
			map.put(new Link(source, target, reference, currentRule), newEdge);
			return true;
		} else {
			return false;
		}
	}
}
