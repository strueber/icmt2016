package de.uni_marburg.fb12.swt.cloneDetection.atl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.m2m.atl.common.ATL.MatchedRule;
import org.eclipse.m2m.atl.common.ATL.Module;

public class ExtractionTrace {

	public Map<Object,EObject> in2out = new HashMap<Object,EObject>();
	public Map<EObject,Object> out2in = new HashMap<EObject,Object>();
	private Module inModule;
	private aatl.Module outModule;
	
	public ExtractionTrace(Module inModule, aatl.Module outModule) {
		this.inModule = inModule;
		this.outModule = outModule;
	}
	
	public Module getInModule() {
		return inModule;
	}

	public aatl.Module getOutModule() {
		return outModule;
	}
	
	public void trace(Object in, EObject out) {
		in2out.put(in, out);
		out2in.put(out, in);
	}

}
