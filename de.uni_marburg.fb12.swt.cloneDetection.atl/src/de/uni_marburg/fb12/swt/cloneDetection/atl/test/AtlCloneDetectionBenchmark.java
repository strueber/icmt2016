package de.uni_marburg.fb12.swt.cloneDetection.atl.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.junit.Test;

import aatl.MatchedRule;
import aatl.Module;
import aatl.ModuleElement;
import de.uni_marburg.fb12.swt.cloneDetection.atl.AbstractCloneGroupDetector;
import de.uni_marburg.fb12.swt.cloneDetection.atl.AtlModuleExtractor;
import de.uni_marburg.fb12.swt.cloneDetection.atl.CloneComputationUtil;
import de.uni_marburg.fb12.swt.cloneDetection.atl.ExtractionTrace;
import de.uni_marburg.fb12.swt.cloneDetection.atl.clustering.AgglomerativeModuleClusterer;
import de.uni_marburg.fb12.swt.cloneDetection.atl.clustering.MetricReporter;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroup;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.ConqatBasedCloneGroupDetector;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.EScanDetectionOriginal;
import de.uni_marburg.fb12.swt.cloneDetection.atl.scanqat.ScanQatBasedCloneDetector;

public class AtlCloneDetectionBenchmark {
	static final String FILE_PATH = "atlzoo/";
	static final String FILE_PATH_RULES = "modules/";

	private static final int TIMEOUT_TIME = 60;
	private static final TimeUnit TIMEOUT_UNIT = TimeUnit.MINUTES;

	private Mode MODE = Mode.CONQAT;

	@Test
	public void detectClonesOnClusters() {
		List<Path> paths = IOUtil.getLocations(FILE_PATH + FILE_PATH_RULES);
		List<Module> modules = new AtlModuleExtractor().extractAbstractedModules(paths, true);

		StringBuilder result = new StringBuilder();
		List<List<Module>> moduleGroups = new ArrayList<List<Module>>();
//		moduleGroups.add(modules);
		moduleGroups.addAll(IOUtil.readClustersFromFile(modules));
//		for (List<Module> cl : moduleGroups) {
//			String report = new MetricReporter(cl).getMetricReport("\t");
//			println(report, result);
//		}
		
		for (List<Module> clusterModules : moduleGroups) {
			long start = System.currentTimeMillis();
			String detectionResult = applyCloneDetection(clusterModules);
			long runtime = System.currentTimeMillis() - start;
			print(runtime + "", result);
			println(detectionResult, result);
		}

		try {
			Files.write(Paths.get("output/cdresult " + MODE + ".txt"), result.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	 @Test
	public void detectClonesOnAllRules() {
		List<Path> paths = IOUtil.getLocations(FILE_PATH + FILE_PATH_RULES);
		int stopAfter = paths.size();
		List<Module> modules = new AtlModuleExtractor().extractAbstractedModules(paths.subList(0, stopAfter), true);
		applyCloneDetection(modules);
	}

//	 @Test
	public void detectClonesOnExampleModule() {
		List<Path> paths = new ArrayList<Path>();
		paths.add(Paths.get("input\\small.atl"));
		List<Module> modules = new AtlModuleExtractor().extractAbstractedModules(paths, true);
		applyCloneDetection(modules);
	}

	private String applyCloneDetection(List<Module> modules) {

		Set<Module> theModules = new HashSet<Module>(modules);
		StringBuilder result = new StringBuilder();
		switch (MODE) {
		case CONQAT:
			List<CloneGroup> cloneGroups = applyConqat(theModules);
			result.append(CloneComputationUtil.getLargest(cloneGroups, true).toMetrics().toString() + "\n");
			result.append(CloneComputationUtil.getBroadest(cloneGroups, true).toMetrics().toString());
			return result.toString();
		case SCANQAT:
			List<CloneMatrix> cloneMatrices = applyScanqat(theModules);
			result.append(CloneComputationUtil.getLargest(cloneMatrices).toMetrics().toString() + "\n");
			result.append(CloneComputationUtil.getBroadest(cloneMatrices).toMetrics().toString());
			return result.toString();
		case ESCAN:
			cloneMatrices = applyEScan(theModules);
			result.append(CloneComputationUtil.getLargest(cloneMatrices).toMetrics().toString() + "\n");
			result.append(CloneComputationUtil.getBroadest(cloneMatrices).toMetrics().toString());
			return result.toString();
		}
		return result.toString();
	}

	public List<CloneGroup> applyConqat(Set<Module> modules) {

		AbstractCloneGroupDetector cd = new ConqatBasedCloneGroupDetector(modules);
		cd.detectCloneGroups();
		List<CloneGroup> cloneGroups = cd.getResultOrderedBySize().getCloneGroups();
		cloneGroups = cloneGroups.stream().filter(c -> c.getSize() > 0).collect(Collectors.toList());
		return cloneGroups;
	}

	public List<CloneMatrix> applyScanqat(Set<Module> modules) {
		boolean timeout = false;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		ScanQatBasedCloneDetector cd = new ScanQatBasedCloneDetector(modules);

		try {
			executor.submit(new CloneDetectorTask(cd)).get(TIMEOUT_TIME, TIMEOUT_UNIT);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			System.err.println("Timeout!");
			timeout = true;
		}

		if (!timeout) {
			return cd.getResultAsCloneMatrixOrderedByNumberOfCommonElements().getCloneGroups().stream()
					.filter(c -> c.getSize() > 0).collect(Collectors.toList());
		} else {
			return new ArrayList<CloneMatrix>();
		}
	}

	public List<CloneMatrix> applyEScan(Set<Module> modules) {
		boolean timeout = false;
		ExecutorService executor = Executors.newSingleThreadExecutor();
		EScanDetectionOriginal cd = new EScanDetectionOriginal(modules);

		try {
			executor.submit(new CloneDetectorTask(cd)).get(TIMEOUT_TIME, TIMEOUT_UNIT);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			System.err.println("Timeout!");
			timeout = true;
		}

		if (!timeout) {
			return cd.getResultAsCloneMatrixOrderedByNumberOfCommonElements().getCloneGroups().stream()
					.filter(c -> c.getSize() > 0).collect(Collectors.toList());
		} else {
			return new ArrayList<CloneMatrix>();
		}
	}

	enum Mode {
		CONQAT, ESCAN, SCANQAT
	}

	static void print(String string, StringBuilder result) {
		result.append(string);
		System.out.print(string);
	}

	static void println(String string, StringBuilder result) {
		result.append(string);
		result.append("\n ");
		System.out.println(string);
	}

}
