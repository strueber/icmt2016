package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import org.conqat.engine.model_clones.model.IDirectedEdge;
import org.conqat.engine.model_clones.model.IModelGraph;
import org.conqat.engine.model_clones.model.INode;

/**
 * Represents a Henshin Edge in the {@link IModelGraph} graph representation required by
 * ConQAT.
 * 
 * @author strueber
 *
 */
public class ConqatEdge implements IDirectedEdge {
	private INode sourceNode;

	public ConqatEdge(String label, INode sourceNode, INode targetNode) {
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
		this.label = label;
	}

	private INode targetNode;
	private String label;

	@Override
	public String getEquivalenceClassLabel() {
		return label;
	}

	@Override
	public INode getSourceNode() {
		return sourceNode;
	}

	@Override
	public INode getTargetNode() {
		return targetNode;
	}

}
