package de.uni_marburg.fb12.swt.cloneDetection.atl.conqat;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import aatl.Binding;
import aatl.Filter;
import aatl.InPatternElement;
import aatl.MatchedRule;
import aatl.Module;
import aatl.OutPatternElement;
import aatl.Variable;
import de.uni_marburg.fb12.swt.cloneDetection.atl.CloneMetricResults;
import de.uni_marburg.fb12.swt.cloneDetection.atl.Link;

public class CloneGroup {
	private Map<EObject, Set<EObject>> nodeMappings;
	private Map<Link, Set<Link>> linkMappings;
	private List<Module> involvedModules;
	private List<MatchedRule> involvedRules;

	public CloneGroup(List<Module> involvedModules, List<MatchedRule> involvedRules, Map<EObject, Set<EObject>> nodeMappings,
			Map<Link, Set<Link>> linkMappings) {
		this.involvedModules = involvedModules;
		this.involvedRules = involvedRules;
		this.nodeMappings = nodeMappings;
		this.linkMappings = linkMappings;
	}

	public int getSize() {
		return toMetrics().getSize();
	}


	public String toString() {
		StringBuilder sb = new StringBuilder();
//		sb.append(super.toString());
		sb.append("CG [Size ");
		sb.append(getSize());
		sb.append(", rules: ");
		sb.append(involvedRules.size());
		sb.append(", modules: ");
		sb.append(involvedModules.size());
		sb.append(". Rules: ");
		Iterator<MatchedRule> it = involvedRules.iterator();
		while (it.hasNext()) {
			MatchedRule r = it.next();
			sb.append(((Module) r.eContainer()).getName());
			sb.append("::");
			sb.append(r.getName());
			if (it.hasNext())
				sb.append(", ");
		}
		sb.append("]");
		return sb.toString();
	}
	
	public String getInvolvedNodesString() {
		StringBuilder result = new StringBuilder();
		Iterator<EObject> it = nodeMappings.keySet().iterator();
		while (it.hasNext()) {
			EObject n = it.next();
			result.append(n);
			if (it.hasNext())
				result.append(", ");
		}
		return result.toString();
	}
	
	public Map<EObject, Set<EObject>> getNodeMappings() {
		return nodeMappings;
	}
	public Map<Link, Set<Link>> getLinkMappings() {
		return linkMappings;
	}
	public List<MatchedRule> getInvolvedRules() {
		return involvedRules;
	}
	public List<Module>  getInvolvedModules() {
		return involvedModules;
	}


	public CloneMetricResults toMetrics() {
		if (metricResult == null) {
			int numberOfModules = getInvolvedModules().size();
			int numberOfRules = getInvolvedRules().size();
			int numberOfInElements = 0;
			int numberOfFilters = 0;
			int numberOfVariables = 0;
			int numberOfOutElements = 0;
			int numberOfBindings = 0;

			Set<EObject> nodes = nodeMappings.keySet();
			for (EObject o : nodes) {
				if (o instanceof InPatternElement)
					numberOfInElements++;
				else if (o instanceof OutPatternElement)
					numberOfOutElements++;
				else if (o instanceof Variable)
					numberOfVariables++;
				else if (o instanceof Filter)
					numberOfFilters++;
				else if (o instanceof Binding)
					numberOfBindings++;
			}
			numberOfInElements = numberOfInElements / numberOfRules;
			numberOfOutElements = numberOfOutElements / numberOfRules;
			numberOfFilters = numberOfFilters / numberOfRules;
			numberOfVariables = numberOfVariables / numberOfRules;
			numberOfBindings = numberOfBindings / numberOfRules;

			metricResult = new CloneMetricResults(numberOfModules, numberOfRules, numberOfFilters, numberOfVariables, numberOfInElements,
					numberOfOutElements, numberOfBindings);
		}
		return metricResult;
	}

	private CloneMetricResults metricResult = null;
}
