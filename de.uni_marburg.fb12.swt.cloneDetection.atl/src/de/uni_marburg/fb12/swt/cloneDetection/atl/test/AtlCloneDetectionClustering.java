package de.uni_marburg.fb12.swt.cloneDetection.atl.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;

import aatl.MatchedRule;
import aatl.Module;
import de.uni_marburg.fb12.swt.cloneDetection.atl.AbstractCloneGroupDetector;
import de.uni_marburg.fb12.swt.cloneDetection.atl.AtlModuleExtractor;
import de.uni_marburg.fb12.swt.cloneDetection.atl.CloneComputationUtil;
import de.uni_marburg.fb12.swt.cloneDetection.atl.ExtractionTrace;
import de.uni_marburg.fb12.swt.cloneDetection.atl.clustering.AgglomerativeModuleClusterer;
import de.uni_marburg.fb12.swt.cloneDetection.atl.clustering.MetricReporter;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.CloneGroup;
import de.uni_marburg.fb12.swt.cloneDetection.atl.conqat.ConqatBasedCloneGroupDetector;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.EScanDetectionOriginal;
import de.uni_marburg.fb12.swt.cloneDetection.atl.scanqat.ScanQatBasedCloneDetector;

public class AtlCloneDetectionClustering {

	@Test
	public void doClustering() {
		List<Path> paths = IOUtil.getLocations(AtlCloneDetectionBenchmark.FILE_PATH + AtlCloneDetectionBenchmark.FILE_PATH_RULES);
		int stopAfter = paths.size();
		List<Module> modules = new AtlModuleExtractor().extractAbstractedModules(paths.subList(0, stopAfter), true);		
		List<CloneGroup> cloneGroups = new AtlCloneDetectionBenchmark().applyConqat(new HashSet<Module>(modules));

		StringBuilder result = new StringBuilder();
		AgglomerativeModuleClusterer clusterer = new AgglomerativeModuleClusterer();
		List<List<Module>> clusters = clusterer.clusterRules(cloneGroups, 0.28);
		println("[Info] Determined " + +clusters.size() + " clusters", result);
		clusters = clusterer.removeSingletonClusters(clusters);
		println("[Info] Determined " + +clusters.size() + " non-singleton clusters", result);
		println(new MetricReporter(modules).getMetricReport("\t"), result);
		for (List<Module> cl : clusters) {
			String report = new MetricReporter(cl).getMetricReport("\t");
			println(report, result);
		}
	}
	

	 static void println(String string, StringBuilder result) {
		result.append(string);
		result.append("\n ");
		System.out.println(string);
	}
}
