/**
 */
package metrics;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Rule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Metrics</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metrics.RuleMetrics#getNumberOfNodes <em>Number Of Nodes</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfEdges <em>Number Of Edges</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfAttributes <em>Number Of Attributes</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfLhsNodes <em>Number Of Lhs Nodes</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfLhsEdges <em>Number Of Lhs Edges</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfLhsAttributes <em>Number Of Lhs Attributes</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getRule <em>Rule</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfAnnotatedNodes <em>Number Of Annotated Nodes</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfAnnotatedEdges <em>Number Of Annotated Edges</em>}</li>
 *   <li>{@link metrics.RuleMetrics#getNumberOfAnnotatedAttributes <em>Number Of Annotated Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see metrics.MetricsPackage#getRuleMetrics()
 * @model
 * @generated
 */
public interface RuleMetrics extends EObject {
	/**
	 * Returns the value of the '<em><b>Number Of Nodes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Nodes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Nodes</em>' attribute.
	 * @see #setNumberOfNodes(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfNodes()
	 * @model
	 * @generated
	 */
	int getNumberOfNodes();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfNodes <em>Number Of Nodes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Nodes</em>' attribute.
	 * @see #getNumberOfNodes()
	 * @generated
	 */
	void setNumberOfNodes(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Edges</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Edges</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Edges</em>' attribute.
	 * @see #setNumberOfEdges(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfEdges()
	 * @model
	 * @generated
	 */
	int getNumberOfEdges();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfEdges <em>Number Of Edges</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Edges</em>' attribute.
	 * @see #getNumberOfEdges()
	 * @generated
	 */
	void setNumberOfEdges(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Attributes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Attributes</em>' attribute.
	 * @see #setNumberOfAttributes(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfAttributes()
	 * @model
	 * @generated
	 */
	int getNumberOfAttributes();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfAttributes <em>Number Of Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Attributes</em>' attribute.
	 * @see #getNumberOfAttributes()
	 * @generated
	 */
	void setNumberOfAttributes(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Lhs Nodes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Lhs Nodes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Lhs Nodes</em>' attribute.
	 * @see #setNumberOfLhsNodes(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfLhsNodes()
	 * @model
	 * @generated
	 */
	int getNumberOfLhsNodes();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfLhsNodes <em>Number Of Lhs Nodes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Lhs Nodes</em>' attribute.
	 * @see #getNumberOfLhsNodes()
	 * @generated
	 */
	void setNumberOfLhsNodes(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Lhs Edges</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Lhs Edges</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Lhs Edges</em>' attribute.
	 * @see #setNumberOfLhsEdges(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfLhsEdges()
	 * @model
	 * @generated
	 */
	int getNumberOfLhsEdges();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfLhsEdges <em>Number Of Lhs Edges</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Lhs Edges</em>' attribute.
	 * @see #getNumberOfLhsEdges()
	 * @generated
	 */
	void setNumberOfLhsEdges(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Lhs Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Lhs Attributes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Lhs Attributes</em>' attribute.
	 * @see #setNumberOfLhsAttributes(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfLhsAttributes()
	 * @model
	 * @generated
	 */
	int getNumberOfLhsAttributes();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfLhsAttributes <em>Number Of Lhs Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Lhs Attributes</em>' attribute.
	 * @see #getNumberOfLhsAttributes()
	 * @generated
	 */
	void setNumberOfLhsAttributes(int value);

	/**
	 * Returns the value of the '<em><b>Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule</em>' reference.
	 * @see #setRule(Rule)
	 * @see metrics.MetricsPackage#getRuleMetrics_Rule()
	 * @model
	 * @generated
	 */
	Rule getRule();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getRule <em>Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rule</em>' reference.
	 * @see #getRule()
	 * @generated
	 */
	void setRule(Rule value);

	/**
	 * Returns the value of the '<em><b>Number Of Annotated Nodes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Annotated Nodes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Annotated Nodes</em>' attribute.
	 * @see #setNumberOfAnnotatedNodes(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfAnnotatedNodes()
	 * @model
	 * @generated
	 */
	int getNumberOfAnnotatedNodes();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfAnnotatedNodes <em>Number Of Annotated Nodes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Annotated Nodes</em>' attribute.
	 * @see #getNumberOfAnnotatedNodes()
	 * @generated
	 */
	void setNumberOfAnnotatedNodes(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Annotated Edges</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Annotated Edges</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Annotated Edges</em>' attribute.
	 * @see #setNumberOfAnnotatedEdges(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfAnnotatedEdges()
	 * @model
	 * @generated
	 */
	int getNumberOfAnnotatedEdges();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfAnnotatedEdges <em>Number Of Annotated Edges</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Annotated Edges</em>' attribute.
	 * @see #getNumberOfAnnotatedEdges()
	 * @generated
	 */
	void setNumberOfAnnotatedEdges(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Annotated Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Annotated Attributes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Annotated Attributes</em>' attribute.
	 * @see #setNumberOfAnnotatedAttributes(int)
	 * @see metrics.MetricsPackage#getRuleMetrics_NumberOfAnnotatedAttributes()
	 * @model
	 * @generated
	 */
	int getNumberOfAnnotatedAttributes();

	/**
	 * Sets the value of the '{@link metrics.RuleMetrics#getNumberOfAnnotatedAttributes <em>Number Of Annotated Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Annotated Attributes</em>' attribute.
	 * @see #getNumberOfAnnotatedAttributes()
	 * @generated
	 */
	void setNumberOfAnnotatedAttributes(int value);

} // RuleMetrics
