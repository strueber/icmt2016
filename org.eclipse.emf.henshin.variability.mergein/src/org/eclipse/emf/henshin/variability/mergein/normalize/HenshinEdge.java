package org.eclipse.emf.henshin.variability.mergein.normalize;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.henshin.model.Action.Type;

public class HenshinEdge extends HenshinGraphElement {
	private EReference type;
	private Type actionType;
	private String ruleName;
	
	public HenshinEdge(HenshinGraph graph, EReference type, Type actionType, String ruleName, boolean attributeEdge) {
		super(graph);
		this.type = type;
		this.actionType = actionType;
		this.ruleName = ruleName;
		
		if (type==null || actionType == null) {
			System.err.println("type or actionType was null");
		}
	}
	public EReference getType() {
		return type;
	}
	public void setType(EReference type) {
		this.type = type;
	}
	public Type getActionType() {
		return actionType;
	}
	public void setActionType(Type actionType) {
		this.actionType = actionType;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
}
