package org.eclipse.emf.henshin.variability.mergein.normalize;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Action.Type;

public class HenshinAttributeNode extends HenshinNode {
	String value;
	
	public HenshinAttributeNode(HenshinGraph graph, EObject type, Action action, String ruleName, String value) {
		super(graph,type, action, ruleName);
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
