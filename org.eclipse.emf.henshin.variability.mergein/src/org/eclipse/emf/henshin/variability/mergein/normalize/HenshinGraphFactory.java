package org.eclipse.emf.henshin.variability.mergein.normalize;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Action.Type;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

public class HenshinGraphFactory {

	private static HenshinGraphFactory instance = new HenshinGraphFactory();

	private HenshinGraphFactory() {
	}

	public static HenshinGraphFactory getInstance() {
		return instance;
	}

	public HenshinGraph createIntegratedGraph(Rule rule, boolean includeRhs)
			throws InputRuleNotSupportedException {
		checkPrecondition(rule);
		HenshinGraph result = new HenshinGraph();
		result.setRuleName(rule.getName());

		// Ignoring kernel rule name: We look for clones between rules
		addGraphElementsToResult(rule, "", result, includeRhs);

		for (Rule multiRule : rule.getMultiRules()) {
			String ruleName = determineRuleName(rule, multiRule);
			addGraphElementsToResult(multiRule, ruleName, result, includeRhs);
		}

		return result;
	}

	private String determineRuleName(Rule rule, Rule multiRule) {
		if (multiRule.getName() !=  null && multiRule.getName().length() > 0)
			return multiRule.getName();
		if (rule.getMultiRules().size() == 1)
			return "mul";
		else {
			int i = 0;
			String result = "multi0";
			while (rule.getMultiRule(result) != null) {
				i++;
				result = "multi"+i;
			}
			return result;				
		}
	}

	private void addGraphElementsToResult(Rule rule, String ruleName,
			HenshinGraph result, boolean includeRhS) {
		Set<Type> types = getRelevantTypes(includeRhS);
		for (Type type : types) {
			for (Node n : rule.getActionNodes(new Action(type))) {
				HenshinNode node = new HenshinNode(result, n.getType(), n.getAction(),
						ruleName);
				result.addVertex(n, node);
			}
		}

		for (Type type : types) {
			for (Edge edge : rule.getActionEdges(new Action(type))) {
				Node source = edge.getSource().getActionNode();
				Node target = edge.getTarget().getActionNode();
				HenshinEdge integratedEdge = new HenshinEdge(result,
						edge.getType(), type, ruleName, false);
				result.addEdge(source, target, edge, integratedEdge);
			}
		}

		for (Type type : types) {
			for (Attribute attribute : getActionAttributes(rule, new Action(
					type))) {
				result.addAttribute(attribute, type, ruleName);
			}
		}
	}

	private Set<Type> getRelevantTypes(boolean includeRhs) {
		Set<Type> set = new HashSet<Type>();
		set.add(Type.DELETE);
		set.add(Type.FORBID);
		set.add(Type.PRESERVE);
		set.add(Type.REQUIRE);
		if (includeRhs) {
			set.add(Type.CREATE);
		}
		return set;
	}

	private void checkPrecondition(Rule rule)
			throws InputRuleNotSupportedException {
		// if (rule.getMultiRules().size() > 1)
		// throw new
		// InputRuleNotSupportedException("Currently support only 1 nested rule.");
//		if (rule.getLhs().getNACs().size() > 1)
//			throw new InputRuleNotSupportedException(
//					"Currently only supporting 1 NAC per rule.");
//		if (rule.getLhs().getPACs().size() > 1)
//			throw new InputRuleNotSupportedException(
//					"Currently only supporting 1 PAC per rule.");
//		if (rule.getLhs().getNACs().size() == 1)
//			if (!rule.getLhs().getNACs().get(0).getConclusion()
//					.getNestedConditions().isEmpty())
//				throw new InputRuleNotSupportedException(
//						"Currently not supporting nesting of application conditions.");
//		if (rule.getLhs().getPACs().size() == 1)
//			if (!rule.getLhs().getPACs().get(0).getConclusion()
//					.getNestedConditions().isEmpty())
//				throw new InputRuleNotSupportedException(
//						"Currently not supporting nesting of application conditions.");

	}

	private List<Attribute> getActionAttributes(Rule rule, Action action) {
		ArrayList<Attribute> result = new ArrayList<Attribute>();
		TreeIterator<EObject> it = rule.eAllContents();
		while (it.hasNext()) {
			EObject current = it.next();
			if (current instanceof Node) {
				result.addAll(((Node) current).getActionAttributes(action));
			}
		}
		return result;
	}

	public RuleToHenshinGraphMap createIntegratedGraphs(List<Rule> rules, boolean includeRhs) {
		RuleToHenshinGraphMap result = new RuleToHenshinGraphMap();
		for (Rule rule : rules) {
			try {
				result.put(rule, createIntegratedGraph(rule, includeRhs));
			} catch (InputRuleNotSupportedException e) {
				System.out.println("Skipping rule " + rule.getName() + ": "
						+ e.getMessage());
			}
		}
		return result;
	}
}
