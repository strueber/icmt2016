/**
 */
package mergeSuggestion;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mergeSuggestion.MergeSuggestionFactory
 * @model kind="package"
 * @generated
 */
public interface MergeSuggestionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mergeSuggestion";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mergeSuggestion";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mergeSuggestion";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MergeSuggestionPackage eINSTANCE = mergeSuggestion.impl.MergeSuggestionPackageImpl.init();

	/**
	 * The meta object id for the '{@link mergeSuggestion.impl.MergeSuggestionImpl <em>Merge Suggestion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mergeSuggestion.impl.MergeSuggestionImpl
	 * @see mergeSuggestion.impl.MergeSuggestionPackageImpl#getMergeSuggestion()
	 * @generated
	 */
	int MERGE_SUGGESTION = 0;

	/**
	 * The feature id for the '<em><b>Merge Clusters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_SUGGESTION__MERGE_CLUSTERS = 0;

	/**
	 * The number of structural features of the '<em>Merge Suggestion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_SUGGESTION_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Find Merge Rule</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_SUGGESTION___FIND_MERGE_RULE__RULE = 0;

	/**
	 * The number of operations of the '<em>Merge Suggestion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_SUGGESTION_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link mergeSuggestion.impl.MergeRuleImpl <em>Merge Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mergeSuggestion.impl.MergeRuleImpl
	 * @see mergeSuggestion.impl.MergeSuggestionPackageImpl#getMergeRule()
	 * @generated
	 */
	int MERGE_RULE = 1;

	/**
	 * The feature id for the '<em><b>Master Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE__MASTER_RULE = 0;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE__RULES = 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE__ELEMENTS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE__NAME = 3;

	/**
	 * The number of structural features of the '<em>Merge Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Find Merge Rule Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE___FIND_MERGE_RULE_ELEMENT__GRAPHELEMENT = 0;

	/**
	 * The operation id for the '<em>Add Merge Rule Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE___ADD_MERGE_RULE_ELEMENT__MERGERULEELEMENT = 1;

	/**
	 * The number of operations of the '<em>Merge Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link mergeSuggestion.impl.MergeRuleElementImpl <em>Merge Rule Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mergeSuggestion.impl.MergeRuleElementImpl
	 * @see mergeSuggestion.impl.MergeSuggestionPackageImpl#getMergeRuleElement()
	 * @generated
	 */
	int MERGE_RULE_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Reference Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT__REFERENCE_ELEMENTS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT__NAME = 1;

	/**
	 * The number of structural features of the '<em>Merge Rule Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Is Nac Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT___IS_NAC_ELEMENT = 0;

	/**
	 * The operation id for the '<em>Is Pac Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT___IS_PAC_ELEMENT = 1;

	/**
	 * The operation id for the '<em>Is Lhs Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT___IS_LHS_ELEMENT = 2;

	/**
	 * The operation id for the '<em>Is Rhs Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT___IS_RHS_ELEMENT = 3;

	/**
	 * The operation id for the '<em>Is Multi Rule Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT___IS_MULTI_RULE_ELEMENT = 4;

	/**
	 * The number of operations of the '<em>Merge Rule Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_RULE_ELEMENT_OPERATION_COUNT = 5;


	/**
	 * Returns the meta object for class '{@link mergeSuggestion.MergeSuggestion <em>Merge Suggestion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Suggestion</em>'.
	 * @see mergeSuggestion.MergeSuggestion
	 * @generated
	 */
	EClass getMergeSuggestion();

	/**
	 * Returns the meta object for the containment reference list '{@link mergeSuggestion.MergeSuggestion#getMergeClusters <em>Merge Clusters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Merge Clusters</em>'.
	 * @see mergeSuggestion.MergeSuggestion#getMergeClusters()
	 * @see #getMergeSuggestion()
	 * @generated
	 */
	EReference getMergeSuggestion_MergeClusters();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeSuggestion#findMergeRule(org.eclipse.emf.henshin.model.Rule) <em>Find Merge Rule</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Merge Rule</em>' operation.
	 * @see mergeSuggestion.MergeSuggestion#findMergeRule(org.eclipse.emf.henshin.model.Rule)
	 * @generated
	 */
	EOperation getMergeSuggestion__FindMergeRule__Rule();

	/**
	 * Returns the meta object for class '{@link mergeSuggestion.MergeRule <em>Merge Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Rule</em>'.
	 * @see mergeSuggestion.MergeRule
	 * @generated
	 */
	EClass getMergeRule();

	/**
	 * Returns the meta object for the reference '{@link mergeSuggestion.MergeRule#getMasterRule <em>Master Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Master Rule</em>'.
	 * @see mergeSuggestion.MergeRule#getMasterRule()
	 * @see #getMergeRule()
	 * @generated
	 */
	EReference getMergeRule_MasterRule();

	/**
	 * Returns the meta object for the reference list '{@link mergeSuggestion.MergeRule#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rules</em>'.
	 * @see mergeSuggestion.MergeRule#getRules()
	 * @see #getMergeRule()
	 * @generated
	 */
	EReference getMergeRule_Rules();

	/**
	 * Returns the meta object for the containment reference list '{@link mergeSuggestion.MergeRule#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see mergeSuggestion.MergeRule#getElements()
	 * @see #getMergeRule()
	 * @generated
	 */
	EReference getMergeRule_Elements();

	/**
	 * Returns the meta object for the attribute '{@link mergeSuggestion.MergeRule#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mergeSuggestion.MergeRule#getName()
	 * @see #getMergeRule()
	 * @generated
	 */
	EAttribute getMergeRule_Name();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRule#findMergeRuleElement(org.eclipse.emf.henshin.model.GraphElement) <em>Find Merge Rule Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Merge Rule Element</em>' operation.
	 * @see mergeSuggestion.MergeRule#findMergeRuleElement(org.eclipse.emf.henshin.model.GraphElement)
	 * @generated
	 */
	EOperation getMergeRule__FindMergeRuleElement__GraphElement();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRule#addMergeRuleElement(mergeSuggestion.MergeRuleElement) <em>Add Merge Rule Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Merge Rule Element</em>' operation.
	 * @see mergeSuggestion.MergeRule#addMergeRuleElement(mergeSuggestion.MergeRuleElement)
	 * @generated
	 */
	EOperation getMergeRule__AddMergeRuleElement__MergeRuleElement();

	/**
	 * Returns the meta object for class '{@link mergeSuggestion.MergeRuleElement <em>Merge Rule Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge Rule Element</em>'.
	 * @see mergeSuggestion.MergeRuleElement
	 * @generated
	 */
	EClass getMergeRuleElement();

	/**
	 * Returns the meta object for the reference list '{@link mergeSuggestion.MergeRuleElement#getReferenceElements <em>Reference Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reference Elements</em>'.
	 * @see mergeSuggestion.MergeRuleElement#getReferenceElements()
	 * @see #getMergeRuleElement()
	 * @generated
	 */
	EReference getMergeRuleElement_ReferenceElements();

	/**
	 * Returns the meta object for the attribute '{@link mergeSuggestion.MergeRuleElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mergeSuggestion.MergeRuleElement#getName()
	 * @see #getMergeRuleElement()
	 * @generated
	 */
	EAttribute getMergeRuleElement_Name();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRuleElement#isNacElement() <em>Is Nac Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Nac Element</em>' operation.
	 * @see mergeSuggestion.MergeRuleElement#isNacElement()
	 * @generated
	 */
	EOperation getMergeRuleElement__IsNacElement();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRuleElement#isPacElement() <em>Is Pac Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Pac Element</em>' operation.
	 * @see mergeSuggestion.MergeRuleElement#isPacElement()
	 * @generated
	 */
	EOperation getMergeRuleElement__IsPacElement();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRuleElement#isLhsElement() <em>Is Lhs Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Lhs Element</em>' operation.
	 * @see mergeSuggestion.MergeRuleElement#isLhsElement()
	 * @generated
	 */
	EOperation getMergeRuleElement__IsLhsElement();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRuleElement#isRhsElement() <em>Is Rhs Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Rhs Element</em>' operation.
	 * @see mergeSuggestion.MergeRuleElement#isRhsElement()
	 * @generated
	 */
	EOperation getMergeRuleElement__IsRhsElement();

	/**
	 * Returns the meta object for the '{@link mergeSuggestion.MergeRuleElement#isMultiRuleElement() <em>Is Multi Rule Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Multi Rule Element</em>' operation.
	 * @see mergeSuggestion.MergeRuleElement#isMultiRuleElement()
	 * @generated
	 */
	EOperation getMergeRuleElement__IsMultiRuleElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MergeSuggestionFactory getMergeSuggestionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mergeSuggestion.impl.MergeSuggestionImpl <em>Merge Suggestion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mergeSuggestion.impl.MergeSuggestionImpl
		 * @see mergeSuggestion.impl.MergeSuggestionPackageImpl#getMergeSuggestion()
		 * @generated
		 */
		EClass MERGE_SUGGESTION = eINSTANCE.getMergeSuggestion();

		/**
		 * The meta object literal for the '<em><b>Merge Clusters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_SUGGESTION__MERGE_CLUSTERS = eINSTANCE.getMergeSuggestion_MergeClusters();

		/**
		 * The meta object literal for the '<em><b>Find Merge Rule</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_SUGGESTION___FIND_MERGE_RULE__RULE = eINSTANCE.getMergeSuggestion__FindMergeRule__Rule();

		/**
		 * The meta object literal for the '{@link mergeSuggestion.impl.MergeRuleImpl <em>Merge Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mergeSuggestion.impl.MergeRuleImpl
		 * @see mergeSuggestion.impl.MergeSuggestionPackageImpl#getMergeRule()
		 * @generated
		 */
		EClass MERGE_RULE = eINSTANCE.getMergeRule();

		/**
		 * The meta object literal for the '<em><b>Master Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_RULE__MASTER_RULE = eINSTANCE.getMergeRule_MasterRule();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_RULE__RULES = eINSTANCE.getMergeRule_Rules();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_RULE__ELEMENTS = eINSTANCE.getMergeRule_Elements();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MERGE_RULE__NAME = eINSTANCE.getMergeRule_Name();

		/**
		 * The meta object literal for the '<em><b>Find Merge Rule Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE___FIND_MERGE_RULE_ELEMENT__GRAPHELEMENT = eINSTANCE.getMergeRule__FindMergeRuleElement__GraphElement();

		/**
		 * The meta object literal for the '<em><b>Add Merge Rule Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE___ADD_MERGE_RULE_ELEMENT__MERGERULEELEMENT = eINSTANCE.getMergeRule__AddMergeRuleElement__MergeRuleElement();

		/**
		 * The meta object literal for the '{@link mergeSuggestion.impl.MergeRuleElementImpl <em>Merge Rule Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mergeSuggestion.impl.MergeRuleElementImpl
		 * @see mergeSuggestion.impl.MergeSuggestionPackageImpl#getMergeRuleElement()
		 * @generated
		 */
		EClass MERGE_RULE_ELEMENT = eINSTANCE.getMergeRuleElement();

		/**
		 * The meta object literal for the '<em><b>Reference Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MERGE_RULE_ELEMENT__REFERENCE_ELEMENTS = eINSTANCE.getMergeRuleElement_ReferenceElements();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MERGE_RULE_ELEMENT__NAME = eINSTANCE.getMergeRuleElement_Name();

		/**
		 * The meta object literal for the '<em><b>Is Nac Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE_ELEMENT___IS_NAC_ELEMENT = eINSTANCE.getMergeRuleElement__IsNacElement();

		/**
		 * The meta object literal for the '<em><b>Is Pac Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE_ELEMENT___IS_PAC_ELEMENT = eINSTANCE.getMergeRuleElement__IsPacElement();

		/**
		 * The meta object literal for the '<em><b>Is Lhs Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE_ELEMENT___IS_LHS_ELEMENT = eINSTANCE.getMergeRuleElement__IsLhsElement();

		/**
		 * The meta object literal for the '<em><b>Is Rhs Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE_ELEMENT___IS_RHS_ELEMENT = eINSTANCE.getMergeRuleElement__IsRhsElement();

		/**
		 * The meta object literal for the '<em><b>Is Multi Rule Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MERGE_RULE_ELEMENT___IS_MULTI_RULE_ELEMENT = eINSTANCE.getMergeRuleElement__IsMultiRuleElement();

	}

} //MergeSuggestionPackage
