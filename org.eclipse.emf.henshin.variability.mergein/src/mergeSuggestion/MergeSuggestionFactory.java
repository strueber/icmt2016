/**
 */
package mergeSuggestion;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see mergeSuggestion.MergeSuggestionPackage
 * @generated
 */
public interface MergeSuggestionFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MergeSuggestionFactory eINSTANCE = mergeSuggestion.impl.MergeSuggestionFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Merge Suggestion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge Suggestion</em>'.
	 * @generated
	 */
	MergeSuggestion createMergeSuggestion();

	/**
	 * Returns a new object of class '<em>Merge Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge Rule</em>'.
	 * @generated
	 */
	MergeRule createMergeRule();

	/**
	 * Returns a new object of class '<em>Merge Rule Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge Rule Element</em>'.
	 * @generated
	 */
	MergeRuleElement createMergeRuleElement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MergeSuggestionPackage getMergeSuggestionPackage();

} //MergeSuggestionFactory
