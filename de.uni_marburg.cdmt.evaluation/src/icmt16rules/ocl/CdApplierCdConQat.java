package icmt16rules.ocl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroupDetectionResult;
import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;
import org.junit.Test;

import GraphConstraint.GraphConstraintPackage;
import icmt16rules.util.CloneComputationUtil;
import icmt16rules.util.WriteUtil;

public class CdApplierCdConQat {
	String example[] = { "ocl", "OCL2NGC" };
	private static final String FILE_EXTENSION_HENSHIN = ".henshin";


	@Test
	public void doCloneDetection() throws IOException {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));

		StringBuilder resultTrace = new StringBuilder();
		String resultTracePath = path + "/" + locationPath + "/trace/conqat-results-" + fileName + ".txt";
		StringBuilder timeTrace = new StringBuilder();
		String timeTracePath = path + "/" + locationPath + "/trace/conqat-time-" + fileName + ".txt";
		
		FileReader fr = new FileReader(locationPath+"/allRules");
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();

		while (line != null) {
			String[] split = line.split(": ");
			String name = split[0];
			String[] relevant = split[1].split(" ");
			doCloneDetection(false, name, relevant, resultTrace, timeTrace);
			line = br.readLine();
		}

		doCloneDetection(true, "all", new String[0], resultTrace, timeTrace);
		
		WriteUtil.write(resultTrace, resultTracePath);
		WriteUtil.write(timeTrace, timeTracePath);
	}

	private void doCloneDetection(boolean all, String name, String[] relevant, StringBuilder resultTrace, StringBuilder timeTrace) {
		String locationPath = example[0];
		String fileName = example[1];
		HenshinResourceSet rs = new HenshinResourceSet(locationPath);	
		rs.registerDynamicEPackages("Pivot.ecore");

		initialize(locationPath, rs);
		Module module = (Module) rs.getEObject(fileName
				+ FILE_EXTENSION_HENSHIN);

		List<Rule> rules = getRelevantRules(module, all, relevant);

		long startTime = System.currentTimeMillis();

		ConqatBasedCloneGroupDetector cq = new ConqatBasedCloneGroupDetector(
				rules, true);
		cq.detectCloneGroups();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		CloneGroupDetectionResult result = cq
				.getResultOrderedByNumberOfCommonElements();

		CloneGroup l = CloneComputationUtil.getLargest(result.getCloneGroups(), true);
		CloneGroup b = CloneComputationUtil.getBroadest(result.getCloneGroups(), true);

		WriteUtil.printCloneGroup(l, name + "\tLargest", resultTrace);
		WriteUtil.printCloneGroup(b, "\tBroadest", resultTrace);
		WriteUtil.println(name+" \t "+elapsedTime, timeTrace);
	}

	
	private List<Rule> getRelevantRules(Module module, boolean all,
			String[] relevant) {
		ArrayList<Rule> result = new ArrayList<Rule>();
		ArrayList<String> names = new ArrayList<String>(Arrays.asList(relevant));
	
		if (all) {
			for (Unit u : module.getUnits()) {
				if (u instanceof Rule) {
					Rule r = (Rule) u;
					// if (all || names.contains(r.getName()))
					result.add(r);
				}
			}
		} else {
			for (String n : names) {
				Unit u = module.getUnit(n);
				if (u == null) {
					System.err.println("Rule not found: " + n);
				} else
					result.add((Rule) u);
			}
		}
	
		return result;
	}

	private void initialize(String locationPath, HenshinResourceSet rs) {
		GraphConstraintPackage.eINSTANCE.eClass();
	}
}