package icmt16rules.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import icmt16rules.fmedit.CdApplierCdEScan;

public class WriteUtil {

	public static void write(StringBuilder trace, String tracePath) {
		try {
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(tracePath));
			writer.write(trace.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public static void println(String string, StringBuilder trace) {
		System.out.println(string);
		trace.append(string);
		trace.append('\n');
	}

	public static void printCloneGroup(CloneGroup r, String label, StringBuilder trace) {
		int ruleCount = r.getRules().size();
		String res = label + "\t" + 0 + "\t" + 0 + "\t" + 0 + "\t" + 0;
		if (!r.getRules().isEmpty()) {
			 res = label + "\t" + ruleCount + "\t"
					+ r.getNodeMappings().size() / ruleCount + "\t"
					+ r.getEdgeMappings().size() / ruleCount + "\t"
					+ r.getAttributeMappings().size() / ruleCount;
		}
		println(res, trace);
	}

	public static void printCloneGroup(CloneMatrix r, String label, StringBuilder trace) {
		int ruleCount = r.getRuleSet().size();
		String res = label + "\t" + 0 + "\t" + 0 + "\t" + 0 + "\t" + 0;
		if (!r.getRuleSet().isEmpty()) {
			res = label + "\t" + ruleCount + "\t" + r.getNumberOfCommonNodes() + "\t" + r.getNumberOfCommonEdges()
					+ "\t" + r.getNumberOfCommonAttributes();
		}
		println(res, trace);
	}

}
