package icmt16rules.util;

import java.util.List;

import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;


public class CloneComputationUtil {

	public static CloneMatrix getLargest(List<CloneMatrix> list) {
		if (list.isEmpty())
			return new CloneMatrix();
	
		CloneMatrix largest = list.get(0);
		int maxSize = largest.getSize();
	
		for (CloneMatrix cg : list) {
			if (cg.getSize() > maxSize) {
				largest = cg;
				maxSize = cg.getSize();
			}
		}
	
		return largest;
	}

	/**
	 * 
	 * @param cloneGroups
	 * @param dummy Dummy parameter to circumvent Java's type erasure.
	 * @return
	 */
	public static CloneGroup getLargest(List<CloneGroup> cloneGroups, boolean dummy) {
		if (cloneGroups.isEmpty())
			return new CloneGroup();
	
		CloneGroup largest = cloneGroups.get(0);
		int maxSize = largest.getSize();
	
		for (CloneGroup cg : cloneGroups) {
			if (cg.getSize() > maxSize) {
				largest = cg;
				maxSize = cg.getSize();
			}
		}
	
		return largest;
	}

	public static CloneMatrix getBroadest(List<CloneMatrix> cloneGroups) {
		if (cloneGroups.isEmpty())
			return new CloneMatrix();
		CloneMatrix broadest = cloneGroups.get(0);
		int maxBroadness = broadest.getRuleSet().size();
		int maxSize = broadest.getSize();
	
		for (CloneMatrix cg : cloneGroups) {
			System.err.println(cg.getRuleSet().size());
			if (cg.getRuleSet().size() > maxBroadness) {
				broadest = cg;
				maxBroadness = broadest.getRuleSet().size();
				maxSize = broadest.getSize();
			} else if (cg.getRuleSet().size() > maxSize) { // size is secondary
															// criterion
				if (cg.getSize() > maxSize) {
					broadest = cg;
					maxBroadness = broadest.getRuleSet().size();
					maxSize = broadest.getSize();
				}
			}
		}
	
		return broadest;
	}

	/**
	 * 
	 * @param cloneGroups
	 * @param dummy Dummy parameter to circumvent Java's type erasure.
	 * @return
	 */
	public static CloneGroup getBroadest(List<CloneGroup> cloneGroups, boolean dummy) {
		if (cloneGroups.isEmpty())
			return new CloneGroup();
		CloneGroup broadest = cloneGroups.get(0);
		int maxBroadness = broadest.getRules().size();
		int maxSize = broadest.getSize();
	
		for (CloneGroup cg : cloneGroups) {
			if (cg.getRules().size() > maxBroadness) {
				broadest = cg;
				maxBroadness = broadest.getRules().size();
				maxSize = broadest.getSize();
			} else if (cg.getRules().size() > maxSize) { // size is secondary
															// criterion
				if (cg.getSize() > maxSize) {
					broadest = cg;
					maxBroadness = broadest.getRules().size();
					maxSize = broadest.getSize();
				}
			}
		}
	
		return broadest;
	}

}
