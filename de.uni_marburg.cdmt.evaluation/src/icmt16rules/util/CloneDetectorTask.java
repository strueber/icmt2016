package icmt16rules.util;

import java.util.concurrent.Callable;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;


public class CloneDetectorTask implements Callable {
	CloneDetection cloneDetector;

	public CloneDetectorTask(CloneDetection cloneDetector) {
		this.cloneDetector = cloneDetector;
	}

	@Override
	public Object call() throws Exception {
		cloneDetector.detectCloneGroups();
		return cloneDetector;
	}

}