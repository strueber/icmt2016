package icmt16rules.umledit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;

public class ModuleInfo {
	public ModuleInfo(Module module) {
		super();
		this.module = module;
		this.pathsToRules = new HashMap<String, List<Rule>>();
		this.directoriesToRules = new HashMap<String, List<Rule>>();
	}

	public Module getModule() {
		return module;
	}
	
	public Map<String, List<Rule>> getPathsToRules() {
		return pathsToRules;
	}

	public Map<String, List<Rule>> getDirectoriesToRules() {
		return directoriesToRules;
	}

	private Module module;
	private Map<String, List<Rule>> pathsToRules;
	private Map<String, List<Rule>> directoriesToRules;
	
	public void addPathAndRule(String path, Rule rule) {
		List<Rule> s = pathsToRules.get(path);
		if (s == null) {
			s = new ArrayList<Rule>();
			pathsToRules.put(path, s);
		}
		s.add(rule);
		
		String directory = path.substring(0, path.lastIndexOf('/'));
		addDirectoryAndRule(directory, rule);
	}
	
	private void addDirectoryAndRule(String dir, Rule rule) {
		List<Rule> s = directoriesToRules.get(dir);
		if (s == null) {
			s = new ArrayList<Rule>();
			directoriesToRules.put(dir, s);
		}
		s.add(rule);

	}
}
