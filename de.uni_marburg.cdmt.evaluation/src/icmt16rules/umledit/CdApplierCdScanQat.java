package icmt16rules.umledit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.Test;

import GraphConstraint.GraphConstraintPackage;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneGroupDetectionResultAsCloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetectionWeb.CombineDetectionCDWebwithEScanInc;
import icmt16rules.util.CloneComputationUtil;
import icmt16rules.util.CloneDetectorTask;
import icmt16rules.util.WriteUtil;

public class CdApplierCdScanQat {
	String example[] = { "umledit", "umleditoperations" };
	private static final int TIMEOUT_TIME = 60;
	private static final TimeUnit TIMEOUT_UNIT = TimeUnit.MINUTES;
	
	@Test
	public void doCloneDetection() throws IOException {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));
		StringBuilder resultTrace = new StringBuilder();
		String resultTracePath = path + "/" + locationPath + "/trace/scanqat-results-" + fileName + ".txt";
		StringBuilder timeTrace = new StringBuilder();
		String timeTracePath = path + "/" + locationPath + "/trace/scanqat-time-" + fileName + ".txt";


		ModuleInfo moduleInfo = ModuleLoader.loadModule();
		for (String dir : moduleInfo.getDirectoriesToRules().keySet()) {
			doCloneDetection(false, dir, moduleInfo.getDirectoriesToRules().get(dir), resultTrace, timeTrace);
		}

		doCloneDetection(true, "all", moduleInfo.getModule().getRules(), resultTrace, timeTrace);

		WriteUtil.write(resultTrace, resultTracePath);
		WriteUtil.write(timeTrace, timeTracePath);
	}

	private void doCloneDetection(boolean all, String name, List<Rule> relevant, StringBuilder resultTrace,
			StringBuilder timeTrace) {

		long startTime = System.currentTimeMillis();

		CombineDetectionCDWebwithEScanInc detector = new CombineDetectionCDWebwithEScanInc(relevant);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		
		boolean timeout = false;
	
		try {
			executor.submit(new CloneDetectorTask(detector)).get(TIMEOUT_TIME, TIMEOUT_UNIT);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			System.err.println("Timeout!");
			timeout = true;
		}

		if (!timeout) {
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			CloneGroupDetectionResultAsCloneMatrix result = detector
					.getResultAsCloneMatrixOrderedByNumberOfCommonElements();

			WriteUtil.println(name + "\t" + elapsedTime + "\t "
					+ result.getCloneGroups().size() + " entries in "
					+ relevant.size() + " rules.", timeTrace);
			
			CloneMatrix l = CloneComputationUtil.getLargest(result.getCloneGroups());
			CloneMatrix b = CloneComputationUtil.getBroadest(result.getCloneGroups());
			WriteUtil.printCloneGroup(l, name + "\tLargest", resultTrace);
			WriteUtil.printCloneGroup(b, "\tBroadest", resultTrace);			
		} else {
			WriteUtil.println(name + "\t" + "--- (timeout after "+TIMEOUT_TIME+" "+TIMEOUT_UNIT+")",timeTrace);
			WriteUtil.println(name + "\t" + "--- ", resultTrace);
			WriteUtil.println("\t" + "--- ", resultTrace);
		}

	}

}
