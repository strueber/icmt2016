package icmt16rules.umledit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.uml2.uml.UMLPackage;

public class ModuleLoader {

	private static final String FILE_PATH = "umledit/";
	private static final String FILE_PATH_RULES = "rules/";
	private static final String FILE_NAME_RULES_CLASSIC = ".henshin";
	
	public static ModuleInfo loadModule() {
		// Create a resource set with a base directory:
		HenshinResourceSet rs = new HenshinResourceSet(FILE_PATH);
		rs.getPackageRegistry().put(UMLPackage.eINSTANCE.getNsURI(), UMLPackage.eINSTANCE);

		ModuleInfo moduleInfo = null;
		
		for (String location : getLocations(FILE_PATH + FILE_PATH_RULES)) {
			location = FILE_PATH_RULES + location;
			if (moduleInfo == null) {
				Module module = rs.getModule(location, false);
				moduleInfo = new ModuleInfo(module);
				Rule rule = (Rule) module.getUnits().get(0);
				moduleInfo.addPathAndRule(location, rule);
			} else {
				Module mod = rs.getModule(location, false);
				Rule rule = (Rule) mod.getUnits().get(0);
				moduleInfo.getModule().getUnits().add(rule);
				moduleInfo.addPathAndRule(location, rule);
			}
		}

		return moduleInfo;
	}

	public static Set<String> getLocations(String path) {
		Set<String> result = new HashSet<String>();
		try {
			Files.walk(Paths.get(path)).filter(Files::isRegularFile)
					.forEach(f -> result.add(f.getParent().getParent().getFileName() + "/" + f.getParent().getFileName()
							+ "/" + f.getFileName()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result.stream().filter(s -> s.endsWith(FILE_NAME_RULES_CLASSIC)).collect(Collectors.toSet());
	}
}
