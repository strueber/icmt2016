package icmt16rules;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.ocl.examples.pivot.PivotPackage;
import org.junit.Test;

import GraphConstraint.GraphConstraintPackage;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneGroupDetectionResultAsCloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetectionWeb.CombineDetectionCDWebwithEScanInc;

public class CdApplierCdCombined {
//	 String example[] = { "ocl", "OCL2NGC" };
	String example[] = { "fmedit", "featuremodeleditoperations" };
	private static final String FILE_PATH_EXPERIMENTAL_DATA = "exp/";
	private static final String FILE_EXTENSION_HENSHIN = ".henshin";

	//
	// private String name = "tr_E_15a";
	// private String[] relevant = { "tr_E_15a_body", "tr_E_15a_source",
	// "tr_E_15a_argument" };

	@Test
	public void testDetect() throws IOException {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));
		StringBuilder trace = new StringBuilder();
		String tracePath = path + "/" + locationPath + "/trace/combined-" + fileName
				+ ".txt";
		
		
		FileReader fr = new FileReader(locationPath+"/allRules");
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();

		while (line != null) {
			String[] split = line.split(": ");
			String name = split[0];
			String[] relevant = split[1].split(" ");
			trace.append(testDetect(false, name, relevant));
			line = br.readLine();
		}

		trace.append(testDetect(true, "all", new String[0]));

		try {
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(tracePath));
			writer.write(trace.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	private String testDetect(boolean all, String name, String[] relevant) {
		String locationPath = example[0];
		String fileName = example[1];
		HenshinResourceSet rs = new HenshinResourceSet(locationPath);
		initialize(locationPath, rs);
		Module module = (Module) rs.getEObject(fileName
				+ FILE_EXTENSION_HENSHIN);

		StringBuilder trace = new StringBuilder();
		List<Rule> rules = getRelevantRules(module, all, relevant);
//		StringBuilder ruleInfo = new StringBuilder();
//		ruleInfo.append("" + name + "\t" + rules.size() + "");
//		for (Rule r : rules)
//			ruleInfo.append(r.getName() + " ");
//		println(ruleInfo.toString(), trace);

//		RuleSetMetricsCalculator calc = new RuleSetMetricsCalculator();
//		String pre = name + "\t" + calc.calculcate(rules).createPresentationString();
//		println(pre, trace);

		long startTime = System.currentTimeMillis();
		
		CombineDetectionCDWebwithEScanInc cloneDetective = new CombineDetectionCDWebwithEScanInc(rules);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {
			executor.submit(new CloneDetectorTask(cloneDetective)).get(20, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			System.err.println("Timeout!");
			executor.shutdown();
			return name+"\t Timeout";
		}
		executor.shutdown();
//		cloneDetective.detectCloneGroups();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		CloneGroupDetectionResultAsCloneMatrix result = cloneDetective.getResultAsCloneMatrixOrderedByNumberOfCommonElements();
//		CloneGroupDetectionResult result = cq
//				.getResultOrderedByNumberOfCommonElements();
//		println("Took " + elapsedTime + " ms. Found "
//				+ result.getCloneGroups().size() + " entries in "
//				+ rules.size() + " rules.", trace);

		CloneMatrix l = getLargest(result.getCloneGroups());
		CloneMatrix b = getBroadest(result.getCloneGroups());

		printCloneGroup(l, name + "\tLargest", trace);
		printCloneGroup(b, "\tBroadest", trace);
		return trace.toString();

	}

	private void printCloneGroup(CloneMatrix r, String label, StringBuilder trace) {
		int ruleCount = r.getRuleSet().size();
		String res = label + "\t" + 0 + "\t" + 0 + "\t" + 0 + "\t" + 0;
		if (!r.getRuleSet().isEmpty()) {
			 res = label + "\t" + ruleCount + "\t"
					+ r.getNumberOfCommonNodes() + "\t"
					+ r.getNumberOfCommonEdges() + "\t"
					+ r.getNumberOfCommonAttributes();
		}
		println(res, trace);
	}

	private CloneMatrix getBroadest(List<CloneMatrix> cloneGroups) {
		if (cloneGroups.isEmpty())
			return new CloneMatrix();
		CloneMatrix broadest = cloneGroups.get(0);
		int maxBroadness = broadest.getRuleSet().size();
		int maxSize = broadest.getSize();

		for (CloneMatrix cg : cloneGroups) {
			System.err.println(cg.getRuleSet().size());
			if (cg.getRuleSet().size() > maxBroadness) {
				broadest = cg;
				maxBroadness = broadest.getRuleSet().size();
				maxSize = broadest.getSize();
			} else if (cg.getRuleSet().size() > maxSize) { // size is secondary
															// criterion
				if (cg.getSize() > maxSize) {
					broadest = cg;
					maxBroadness = broadest.getRuleSet().size();
					maxSize = broadest.getSize();
				}
			}
		}

		return broadest;
	}

	private CloneMatrix getLargest(List<CloneMatrix> list) {
		if (list.isEmpty())
			return new CloneMatrix();

		 CloneMatrix largest = list.get(0);
		int maxSize = largest.getSize();

		for (CloneMatrix cg : list) {
			if (cg.getSize() > maxSize) {
				largest = cg;
				maxSize = cg.getSize();
			}
		}

		return largest;
	}

	private Set<Rule> getUniqueRules(List<Rule> rules) {
		Set<Rule> result = new HashSet<Rule>();
		for (Rule r : rules) {
			result.add(r);
		}
		return result;
	}

	private List<Rule> getRelevantRules(Module module, boolean all,
			String[] relevant) {
		ArrayList<Rule> result = new ArrayList<Rule>();
		ArrayList<String> names = new ArrayList<String>(Arrays.asList(relevant));

		if (all) {
			for (Unit u : module.getUnits()) {
				if (u instanceof Rule) {
					Rule r = (Rule) u;
					// if (all || names.contains(r.getName()))
					result.add(r);
				}
			}
		} else {
			for (String n : names) {
				Unit u = module.getUnit(n);
				if (u == null) {
					System.err.println("Rule not found: " + n);
				} else
					result.add((Rule) u);
			}
		}

		return result;
	}

	private void println(String string, StringBuilder trace) {
		System.out.println(string);
		trace.append(string);
		trace.append('\n');
	}

	private List<Rule> getRulesForIndependentExecution(Module module) {
		List<Rule> result = new ArrayList<Rule>();
		Unit indUnit = module.getUnit("closureIndependent");
		for (Unit u : indUnit.getSubUnits(false)) {
			if (u instanceof Rule) {
				result.add((Rule) u);
			}
		}
		return result;
	}

	private void initialize(String locationPath, HenshinResourceSet rs) {
		PivotPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();
		FeatureModelPackage.eINSTANCE.eClass();
	}
	
}

class CloneDetectorTask implements Callable {
	CloneDetection cloneDetector; 
	public CloneDetectorTask(CloneDetection cloneDetector) {
		this.cloneDetector = cloneDetector;
	}

	@Override
	public Object call() throws Exception {
		cloneDetector.detectCloneGroups();
		return cloneDetector;
	}
	
}