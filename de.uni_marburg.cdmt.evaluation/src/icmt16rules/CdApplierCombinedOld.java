package icmt16rules;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroupDetectionResult;
import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;
import org.eclipse.emf.henshin.variability.mergein.evaluation.RuleSetMetricsCalculator;
import org.eclipse.ocl.examples.pivot.PivotPackage;
import org.junit.Test;

import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneGroupDetectionResultAsCloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.CloneDetective;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CloneDetectiveWithEScanGroupAndFilterAdded;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CombineDetectionCDwithEScanInc;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetectionWeb.CombineDetectionCDWebwithEScanInc;
import GraphConstraint.GraphConstraintPackage;

public class CdApplierCombinedOld {
//	String example[] = { "ocl", "OCL2NGC" };
	 String example[] = { "fmedit", "featuremodeleditoperations" };
	private static final String FILE_PATH_EXPERIMENTAL_DATA = "exp/";
	private static final String FILE_EXTENSION_HENSHIN = ".henshin";

	private boolean all = false;
	private String name = "Generalization";
	private String[] relevant = { "Generalization_2-1","Generalization_2-2","Generalization_2-2b","Generalization_2-4","Generalization_2-5","Generalization_2-6","Generalization_2-7" };

//	private String name = "tr_E_15a";
//	private String[] relevant = { "tr_E_15a_body", "tr_E_15a_source",
//			"tr_E_15a_argument" };
	@Test
	public void testMerge() {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));
		StringBuilder trace = new StringBuilder();
		String tracePath = path + "/" + locationPath + "/trace/combined-"+name+".txt";
	

		HenshinResourceSet rs = new HenshinResourceSet(locationPath);
		initialize(locationPath, rs);
		Module module = (Module) rs.getEObject(fileName
				+ FILE_EXTENSION_HENSHIN);

		List<Rule> rules = getRelevantRules(module);
		trace.append("Rule set "+ name+": ");
		for (Rule r : rules)
			trace.append(r.getName() + " ");
		trace.append("\n");
		
		// if (NORMALIZE) {
		// RuleNormalizer.normalizeRules(rules);
		// rs.saveEObject(module, fileName + FILE_EXTENSION_HENSHIN);
		// rs.saveEObject(module, fileName + FILE_EXTENSION_HENSHIN);
		// println("Saved normalized", trace);
		// }
		RuleSetMetricsCalculator calc = new RuleSetMetricsCalculator();
		String pre = calc.calculcate(rules).createPresentationString();

		long startTime = System.currentTimeMillis();

		CombineDetectionCDWebwithEScanInc cloneDetective = new CombineDetectionCDWebwithEScanInc(rules);
		cloneDetective.detectCloneGroups();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		CloneGroupDetectionResultAsCloneMatrix result = cloneDetective
				.getResultAsCloneMatrixOrderedByNumberOfCommonElements();

		println("Took " + elapsedTime + " ms. Found "
				+ result.getCloneGroups().size() + " entries in "
				+ rules.size() + " rules.", trace);

		for (CloneMatrix m : result.getCloneGroups()) {
			int ruleCount = m.getRuleList().size();
			String res = ruleCount + " " + m.getNumberOfCommonNodes() + " "
					+ m.getNumberOfCommonEdges() + " "
					+ m.getNumberOfCommonAttributes();
			println(res, trace);
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					
							tracePath));
			writer.write(trace.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	

	}

	private List<Rule> getRelevantRules(Module module) {
		ArrayList<Rule> result = new ArrayList<Rule>();
		ArrayList<String> relevantRules = new ArrayList<String>(
				Arrays.asList(relevant));

		for (Unit u : module.getUnits()) {
			if (u instanceof Rule) {
				Rule r = (Rule) u;
				if (all || relevantRules.contains(r.getName()))
					result.add(r);
			}
		}

		return result;
	}

	private void println(String string, StringBuilder trace) {
		System.out.println(string);
		trace.append(string);
		trace.append('\n');
	}

	private List<Rule> getRulesForIndependentExecution(Module module) {
		List<Rule> result = new ArrayList<Rule>();
		Unit indUnit = module.getUnit("closureIndependent");
		for (Unit u : indUnit.getSubUnits(false)) {
			if (u instanceof Rule) {
				result.add((Rule) u);
			}
		}
		return result;
	}

	private void initialize(String locationPath, HenshinResourceSet rs) {
		PivotPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();
		FeatureModelPackage.eINSTANCE.eClass();
//		rs.getPackageRegistry().put(FeatureModelPackage.eINSTANCE.getNsURI(),
//				FeatureModelPackage.eINSTANCE);
//		EPackage pack1 = rs.registerDynamicEPackages("Symmetric.ecore").get(0);
//		rs.getPackageRegistry().put(pack1.getNsURI(), pack1);

	}
}
