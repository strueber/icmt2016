package org.eclipse.emf.henshin.variability.ui.clonedetector;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

public abstract class AbstractCloneGroupDetector {
	protected Collection<Rule> rules;
	protected Set<CloneGroupMapping> result;
	
	@SuppressWarnings("unused")
	private AbstractCloneGroupDetector() {}
	
	public AbstractCloneGroupDetector(Collection<Rule> rules) {
		this.rules = rules;
	}
	
	
	public Set<CloneGroupMapping> getResult() {
		return result;
	}
	
	
	
	public CloneGroupDetectionResult getResultOrderedByNumberOfCommonElements() {
		List<CloneGroupMapping> orderedResult = new ArrayList<CloneGroupMapping>();
		orderedResult.addAll(result);
		Comparator<CloneGroupMapping> comp = new Comparator<CloneGroupMapping>() {
			@Override
			public int compare(CloneGroupMapping arg0, CloneGroupMapping arg1) {
				return arg1.getNumberOfCommonEdges() - arg0.getNumberOfCommonEdges();
			}
		};
		Collections.sort(orderedResult, comp);
		return new CloneGroupDetectionResult(orderedResult);
	}
	
	public abstract void detectCloneGroups();
	
}
