package de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.MappingList;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.eclipse.emf.henshin.model.Attribute;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;

/**
 * 
 * extended: Attributes are transformed in additional Nodes and (Capsule-)Edges
 *
 */

public class ExtendedIntegratedGraphConverterBackUp {

	private static final Action PRESERVE = new Action(Action.Type.PRESERVE);
	private static final Action DELETE = new Action(Action.Type.DELETE);
	private static final Action CREATE = new Action(Action.Type.CREATE);
	private static final Action FORBID = new Action(Action.Type.FORBID);
	private static final Action REQUIRE = new Action(Action.Type.REQUIRE);

	/**
	 * the extended Version
	 * 
	 * @param rules
	 * @return
	 */
	public Map<Rule, DirectedGraph<Node, CapsuleEdge>> getRuleToGraphWithAttributesMap(
			Collection<Rule> rules) {
		Map<Rule, DirectedGraph<Node, CapsuleEdge>> ergebnis 
				= new HashMap<Rule, DirectedGraph<Node, CapsuleEdge>>();

		for (Rule rule : rules) {
			DirectedGraph<Node, CapsuleEdge> graph = getIntegratedGraph(rule);
			DirectedGraph<Node, CapsuleEdge> graph2 = getExtendendIntegratedGraph(graph);
			ergebnis.put(rule, graph2);
		}
		return ergebnis;
	}

	/**
	 * the second Step: Attributes are transformed in additional Nodes and
	 * (Capsule-)Edges
	 * 
	 * @param graph
	 * @return
	 */

	private DirectedGraph<Node, CapsuleEdge> getExtendendIntegratedGraph(
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<Node> nodes = new HashSet<Node>();
		for (Node n : graph.vertexSet()) {
			if (n.getActionNode().getAttributes().size() != 0) {
				nodes.add(n);
			}
		}
		for (Node n : nodes) {
			for (Attribute a : n.getActionNode().getAttributes()) {
				Node node = HenshinFactory.eINSTANCE.createNode();
				node.setName(LabelCreator.getAttributeNodeName(a));
				graph.addVertex(node);

				Edge originalEdge = HenshinFactory.eINSTANCE.createEdge();
				// n, node, type); <-- is troublesome, since Henshin dislikes
				// Edges to/from Nodes without Graphs

				CapsuleEdge capsuleEdge = new CapsuleEdge(originalEdge, a);
				graph.addEdge(n, node, capsuleEdge);

			}
		}
		return graph;
	}

	/**
	 * 
	 * the first Step: Attributes are ignored
	 * 
	 * @param rule
	 * @return
	 */
	private DirectedGraph<Node, CapsuleEdge> getIntegratedGraph(Rule rule) {

		DirectedGraph<Node, CapsuleEdge> graphN = new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);

		Iterator<Node> nodeIteratorP = rule.getActionNodes(PRESERVE).iterator();
		while (nodeIteratorP.hasNext()) {
			Node tempNode = (nodeIteratorP.next());
			graphN.addVertex(tempNode);

		}

		Iterator<Node> nodeIteratorD = rule.getActionNodes(DELETE).iterator();
		while (nodeIteratorD.hasNext()) {
			graphN.addVertex(nodeIteratorD.next());
		}

		Iterator<Node> nodeIteratorC = rule.getActionNodes(CREATE).iterator();
		while (nodeIteratorC.hasNext()) {
			graphN.addVertex(nodeIteratorC.next());
		}

		Iterator<Node> nodeIteratorF = rule.getActionNodes(FORBID).iterator();
		while (nodeIteratorF.hasNext()) {
			graphN.addVertex(nodeIteratorF.next());
		}

		Iterator<Node> nodeIteratorR = rule.getActionNodes(REQUIRE).iterator();
		while (nodeIteratorR.hasNext()) {
			graphN.addVertex(nodeIteratorR.next());
		}

		graphN = setEdges(rule, PRESERVE, graphN);
		graphN = setEdges(rule, CREATE, graphN);
		graphN = setEdges(rule, DELETE, graphN);
		graphN = setEdges(rule, FORBID, graphN);
		graphN = setEdges(rule, REQUIRE, graphN);
		return graphN;
	}

	private DirectedGraph<Node, CapsuleEdge> setEdges(Rule rule, Action action,
			DirectedGraph<Node, CapsuleEdge> graphN) {
		MappingList mappingList = rule.getMappings();
		if (action.equals(FORBID)) {
			mappingList = rule.getAllMappings();
		}
		if (action.equals(REQUIRE)) {
			mappingList = rule.getAllMappings();
		}

		for (Edge tempEdge2 : rule.getActionEdges(action)) {
			CapsuleEdge capsuleEdge = new CapsuleEdge(tempEdge2);

			Node source = tempEdge2.getSource();
			Node target = tempEdge2.getTarget();

			for (int j = 0; j < mappingList.size(); j++) {
				if (mappingList.get(j).getImage().equals(tempEdge2.getSource())) {
					source = mappingList.get(j).getOrigin(); // <-- setSource is
																// troublesome -
																// create new
																// Edge
				}
				if (mappingList.get(j).getImage().equals(tempEdge2.getTarget())) {
					target = mappingList.get(j).getOrigin();
				}
			}

			try {
				graphN.addEdge(source, target, capsuleEdge);
			} catch (java.lang.IllegalArgumentException e) {
				System.out
						.println("ExtendedIntegratedGraphConverter - setEdges");
				System.out.println(e);
			}
		}

		return graphN;
	}
}
