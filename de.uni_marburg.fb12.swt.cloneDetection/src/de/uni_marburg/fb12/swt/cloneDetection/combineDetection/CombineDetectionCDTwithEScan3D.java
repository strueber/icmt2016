package de.uni_marburg.fb12.swt.cloneDetection.combineDetection;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.CloneDetectiveTupel;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.CloneTupel;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetection3DAbstract;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetection3D;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

/**
 * 
 * Contains the CombineAlgorithm consisting of CloneDetectionTupel and eScan3d
 *
 */
public class CombineDetectionCDTwithEScan3D extends CloneDetection {
	private boolean considerAttributeNodesInStep1;

	public CombineDetectionCDTwithEScan3D(Collection<Rule> rules) {
		super(rules);
		considerAttributeNodesInStep1 = true;
	}

	public CombineDetectionCDTwithEScan3D(Collection<Rule> rules,
			boolean considerAttributeNodesInStep1) {
		super(rules);
		this.considerAttributeNodesInStep1 = considerAttributeNodesInStep1;
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		System.out
				.println(startDetectCloneGroups("CombineDetectionCDTwithEScan3Din1Step"));
		CloneDetectiveTupel cloneDetectiveTupel = new CloneDetectiveTupel(
				ruleGraphMap, ruleList, considerAttributeNodesInStep1);
		Collection<CloneTupel> res = cloneDetectiveTupel
				.runCloneDetectiveTupel();

		System.out.println(startConversionTempResult("CloneDetectionTupel"));
		Map<Integer, Set<List<Fragment>>> resConvert = ConvertToFragments
				.convertCloneTupelsToFragmentLists(res, ruleGraphMap);
		Map<Fragment, List<Set<Fragment>>> topLayer = CreateNewLatticeUtility
				.createTopLayerLattice3D(resConvert);

		System.out.println(moveOnToSecondAlgortihm("eScan3d"));
		EScanDetection3DAbstract eScanDetection3D = new EScanDetection3D(
				ruleGraphMap, ruleList, considerAttributeNodes);
		eScanDetection3D.detectCloneGroups(topLayer);

		System.out.println(startConversion());
		resultAsCloneMatrix = eScanDetection3D.getResultAsCloneMatrix();

		System.out.println(endDetectCloneGroups(
				"CombineDetectionCDTwithEScan3Din1Step", startZeit));
	}

}
