package de.uni_marburg.fb12.swt.cloneDetection.combineDetection;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleOriginal;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

public abstract class CombineDetection extends CloneDetection {
	public CombineDetection(Collection<Rule> rules) {
		super(rules);
	}

	protected Set<Set<Fragment>> getGroupedAndFilteredCloneGroups(
			Map<Integer, Set<Fragment>> resConvert) {
		EScanDetectionArticleOriginal eScanDetection = new EScanDetectionArticleOriginal(
				ruleGraphMap, ruleList, considerAttributeNodes);

		int maxSizeFragments = CreateNewLatticeUtility
				.getMaxSizeFragments(resConvert);
		List<Set<Fragment>> lattice = CreateNewLatticeUtility.createLattice(
				resConvert, maxSizeFragments, maxSizeFragments);
		List<Set<Fragment>> cloneCandidates = new LinkedList<Set<Fragment>>();

		for (int i = 0; i < lattice.size(); i++) {
			Set<Fragment> set = new HashSet<Fragment>();
			set.addAll(lattice.get(i));
			cloneCandidates.add(set);
		}

		Set<Fragment> set = new HashSet<Fragment>();
		cloneCandidates.add(set);

		Set<Set<Fragment>> cloneGroups;
		cloneGroups = eScanDetection.eScanGroupAndFilterLattice(lattice);
		return cloneGroups;
	}

}
