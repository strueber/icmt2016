package de.uni_marburg.fb12.swt.cloneDetection.modelCd;

import org.jgrapht.DirectedGraph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import java.util.Collection;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;

public class EScanDetectionArticleIncOpt1 extends EScanDetectionArticleInc {

	/**
	 * optimized is true in child-class EScanDetectionArticleIncOpt2
	 */
	private final boolean optimized = false;

	protected final int START_LAYER = 1; // <-- article way of counting

	public EScanDetectionArticleIncOpt1(Collection<Rule> rules) {
		super(rules);
	}

	public EScanDetectionArticleIncOpt1(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap, ruleList, considerAttributeNodes);
	}

	public EScanDetectionArticleIncOpt1(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		super(ruleGraphMap.keySet());
	}

	@Override
	public void detectCloneGroups() {
		int itemizedUpTo = getMaxSizeOfClones() + 1; // +1 just to be on the
														// safe side
		detectCloneGroups(itemizedUpTo);
	}

	/**
	 * 
	 * @param itemizedUpTo
	 *            the Layer up to with lattice is build up, step by step,
	 *            respectively layer by layer itemizedUpTo < 0 --> normal eScan
	 *            Algortihmn
	 */
	public void detectCloneGroups(int itemizedUpTo) {
		List<Set<Fragment>> lattice = new LinkedList<Set<Fragment>>();
		detectCloneGroups(itemizedUpTo, lattice);

	}

	/**
	 * 
	 * @param itemizedUpTo
	 *            the Layer up to with lattice is build up, step by step,
	 *            respectively layer by layer itemizedUpTo < 0 --> normal eScan
	 *            Algortihmn
	 * @param lattice
	 *            the data structure eScan works with
	 */
	public void detectCloneGroups(int itemizedUpTo, List<Set<Fragment>> lattice) {
		long startZeit = System.currentTimeMillis();
		final int MAX_LAYER = -1;
		boolean latticeComplete;

		System.out
				.println(startDetectCloneGroups("EScanDetectionArticleInc2Steps - Step 1"));
		latticeComplete = false;
		Set<Fragment> all1Fragments = getL1FragmentWithoutAttributes(); // getL1Fragment();
		for (int i = START_LAYER; i < itemizedUpTo; i++) {
			eScanBuildLatticeAndPrintToConsole(lattice, all1Fragments, i,
					(i + 1));

			if (lattice.size() < (i)) {
				latticeComplete = true;
				break;
			}
		}

		System.out
				.println(startDetectCloneGroups("EScanDetectionArticleInc2Steps - Step 2"));
		boolean ignoreIsGeneratingParent = true;
		latticeComplete = false;
		Set<CapsuleEdge> attributeEdges = getAttributeEdges();

		for (int i = START_LAYER; i < itemizedUpTo; i++) {
			eScanBuildLatticeAndPrintToConsoleStep2(lattice, attributeEdges, i,
					(i + 1), ignoreIsGeneratingParent, optimized);

			if (lattice.size() < (i + 1)) {
				latticeComplete = true;
				break;
			}
		}

		Set<Set<Fragment>> cloneGroups;
		if ((latticeComplete) || (itemizedUpTo == lattice.size())) {
			cloneGroups = eScanGroupAndFilterLattice(lattice);
		} else {
			if (itemizedUpTo < 0) {
				itemizedUpTo = START_LAYER;
			}
			eScanBuildLatticeAndPrintToConsole(lattice, all1Fragments,
					itemizedUpTo, MAX_LAYER);
			cloneGroups = eScanGroupAndFilterLattice(lattice);
		}

		System.out.println(startConversion());

		resultAsCloneMatrix = CloneMatrixCreator
				.convertEScanResult(cloneGroups);

		System.out.println(endDetectCloneGroups(
				"EScanDetectionArticleInc2Steps", startZeit));
	}

	protected Set<CapsuleEdge> getAttributeEdges() {
		Set<CapsuleEdge> res = new HashSet<CapsuleEdge>();

		for (Rule r : ruleGraphMap.keySet()) {
			for (CapsuleEdge h : ruleGraphMap.get(r).edgeSet()) {
				if (h.isAttributeEdge()) {
					res.add(h);
				}
			}

		}
		return res;
	}

	protected void eScanBuildLatticeAndPrintToConsoleStep2(
			List<Set<Fragment>> lattice, Set<CapsuleEdge> attributeEdges,
			int startLayer, int maxLayer, boolean ignoreIsGeneratingParent,
			boolean optimized) {

		System.out.println();
		System.out.println("eScan von: " + startLayer + " bis " + maxLayer);
		lattice = eScanBuildLatticeStep2(lattice, attributeEdges, startLayer,
				maxLayer, ignoreIsGeneratingParent, optimized);

		Date date = new Date();
		System.out.println();
		System.out.println("lattice done - lattice.size: " + lattice.size()
				+ ": " + date.toString());
		printLattice(lattice);

	}

	protected List<Set<Fragment>> eScanBuildLatticeStep2(
			List<Set<Fragment>> lattice, Set<CapsuleEdge> attributeEdges,
			int startLayer, int maxLayer, boolean ignoreIsGeneratingParent,
			boolean optimized) {

		System.out.println("attributeEdges.size(): " + attributeEdges.size());
		System.out.println("startLayer: " + startLayer);

		if (lattice.size() < startLayer) {
			System.out
					.println("Fehler in buildLattice_eScan_Line_2_3:"
							+ " lattice.get(startLayer-1) == null");
		} else {
			/*
			 * while (((lattice.size() > startLayer - 1)) &&
			 * ((lattice.get(startLayer - 1).size() == 0 ))) { startLayer++; }
			 */
			// line 3
			for (Fragment f1 : lattice.get(startLayer - 1)) {
				discover(f1, clones(f1, lattice.get(startLayer - 1)), lattice,
						attributeEdges, startLayer, maxLayer,
						ignoreIsGeneratingParent, optimized);
			}
		}
		return lattice;
	}

	/**
	 * 
	 * modified Discover from the article builts up lattice (the "line" comments
	 * are refering to the article)
	 * 
	 * @param f
	 *            the first parameter from the article
	 * @param fClones
	 *            the second parameter from the article, fClones contains f and
	 *            all of f clones
	 * @param lattice
	 *            the data structure eScan works with, the first layer must
	 *            already be added (the Fragments of size 1) (which have a leat
	 *            one clone, since lattice only contains cloned fragments) (at
	 *            index 0, c.p. kFromArticle)
	 * @param edgesForExtens
	 *            the edges a fragment maybe be extended with, q.v.
	 *            Fragment.extensOp
	 * @param kFromArticle
	 *            the kth layer of lattice, in the article lattice starts with
	 *            1, but a List starts with index 0, so kFromArticle will be
	 *            decreased by 1 and Fragments of size 2 will be stored at index
	 *            1,
	 * @param maxLayer
	 *            the Layer up to with lattice is build up (article way of
	 *            counting), if maxLayer < 0 lattice will be build up completely
	 * @param ignoreIsGeneratingParent
	 * @param optimized
	 *            EScanDetectionArticleInkOpt1: false,
	 *            EScanDetectionArticleInkOpt1: true
	 * 
	 */
	protected void discover(Fragment f, Set<Fragment> fClones,
			List<Set<Fragment>> lattice, Set<CapsuleEdge> edgesForExtens,
			int kFromArticle, int maxLayer, boolean ignoreIsGeneratingParent,
			boolean optimized) {
		if ((maxLayer < 0) || (kFromArticle < maxLayer)) {

			if (fClones.size() <= 1) {
				System.out.println("Fehler in discover: kFromArticle: "
						+ kFromArticle + " - fClones.size: " + fClones.size()
						+ " Fragment: " + f.toString());
			}

			// in the article lattice starts with 1, but a List starts with
			// index 0, so
			// a decreased version, k, of kFromArticle will be used instead
			int k = kFromArticle - 1;

			Set<Fragment> candidateSetCkp1 = new HashSet<Fragment>();
			// line 9 + 10
			for (Fragment g : fClones) {
				Set<Fragment> extensOp = g.extensOp(edgesForExtens);
				candidateSetCkp1.addAll(extensOp);
			}

			if (optimized) {
				// NEW: recycling of Step 1
				if (lattice.size() > (k + 1)) {
					candidateSetCkp1.addAll(lattice.get(k + 1));
				}
			}

			Set<Fragment> findClones;
			// line 11
			for (Fragment ckp1 : candidateSetCkp1) {
				// line 12
				if (ignoreIsGeneratingParent || f.isGeneratingParent(ckp1)) {
					// line 13
					findClones = clones(ckp1, candidateSetCkp1);
					// line 14
					if ((findClones.size() > 1)) {
						if (lattice.size() <= k + 1) {
							Set<Fragment> newLayer = new HashSet<Fragment>();
							lattice.add(newLayer);
						}
						// line 15
						lattice.get(k + 1).addAll(findClones);
						// line 16
						int kFromArticleNext = kFromArticle + 1;
						discover(ckp1, findClones, lattice, edgesForExtens,
								kFromArticleNext, maxLayer,
								ignoreIsGeneratingParent, optimized);
					}
				}
			}
		}
	}

}
