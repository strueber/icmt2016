package de.uni_marburg.fb12.swt.cloneDetection.modelCd;

import org.jgrapht.DirectedGraph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.NodeUtility;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public class EScanDetection3DOpt extends EScanDetection3DAbstract {

	public EScanDetection3DOpt(Collection<Rule> rules) {
		super(rules);
	}

	public EScanDetection3DOpt(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap, ruleList, considerAttributeNodes);
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		System.out
				.println(startDetectCloneGroups("EScanDetection3DIn2Steps (Step1:"
						+ " without Attributes - Step2: add Attributes"));
		// Step 1
		List<Set<CapsuleEdge>> graphsEdgeSetListStep1 
				= getGraphsEdgeSetListWithOutAttributes(ruleList);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1Step1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetListStep1);

		Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1Step1);
		List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
				layer1, edgesLayer1Step1);

		// Step 2
		Map<Fragment, List<Set<Fragment>>> topLayer = lattice3D.get(lattice3D
				.size() - 1);
		detectCloneGroupsPart2(lattice3D, topLayer);

		System.out.println(endDetectCloneGroups("EScanDetection3DIn2Steps",
				startZeit));
	}

	@Override
	public void detectCloneGroups(Map<Fragment, List<Set<Fragment>>> topLayer) {
		// Step 1
		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithOutAttributes(ruleList);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
				topLayer, edgesLayer1);

		// Step 2
		Map<Fragment, List<Set<Fragment>>> topLayerNow = lattice3D
				.get(lattice3D.size() - 1);
		detectCloneGroupsPart2(lattice3D, topLayerNow);
	}

	public void detectCloneGroupsPart2(
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D,
			Map<Fragment, List<Set<Fragment>>> topLayer) {
		if (containsOnlySimpleFragments(topLayer)) {
			System.out
					.println("Step2 - is simpel - Attributes will be set during Conversion");
		} else {
			System.out
					.println("Attributes will be set during Conversion,"
							+ " but maybe some are missing.");
		}

		Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
		System.out.println(startConversion());
		resultAsCloneMatrix = CloneMatrixCreator
				.convertEScanWithoutAttibuteResultAndAddAttributes(cloneGroups,
						ruleGraphMap);

	}

	private boolean containsOnlySimpleFragments(
			Map<Fragment, List<Set<Fragment>>> map) {
		boolean isSimpel = true;
		for (Fragment fragment : map.keySet()) {
			if (!isSimpleFragmentCapsuleEdge(fragment,
					ruleGraphMap.get(ruleList.get(0)))) {
				isSimpel = false;
			} else {
				for (int i = 0; i < map.get(fragment).size(); i++) {
					for (Fragment f : map.get(fragment).get(i)) {
						if (!isSimpleFragmentCapsuleEdge(f,
								ruleGraphMap.get(ruleList.get(i + 1)))) {
							isSimpel = false;
						}
					}
				}
			}
		}

		return isSimpel;
	}

	private boolean isSimpleFragmentCapsuleEdge(Fragment fragment,
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<CapsuleEdge> duplikateCapsuleEdgesInGraph = NodeUtility
				.getDuplikateCapsuleEdges(graph);
		if (duplikateCapsuleEdgesInGraph == null
				|| duplikateCapsuleEdgesInGraph.size() == 0) {
			return true;
		}
		for (CapsuleEdge capsuleEdge : fragment.getCapsuleEdges()) {
			for (CapsuleEdge ce : duplikateCapsuleEdgesInGraph) {
				if (capsuleEdge == ce) {
					return false;
				}
			}
		}
		return true;
	}

	private List<Set<CapsuleEdge>> getGraphsEdgeSetListWithOutAttributes(
			List<Rule> ruleList) {
		List<Set<CapsuleEdge>> graphsEdgeSetList = new LinkedList<Set<CapsuleEdge>>();
		for (Rule rule : ruleList) {
			Set<CapsuleEdge> capsuleEdgeSet = getSetWithoutAttributeCapsuleEdges(ruleGraphMap
					.get(rule).edgeSet());
			graphsEdgeSetList.add(capsuleEdgeSet);
		}
		return graphsEdgeSetList;
	}

	private Set<CapsuleEdge> getSetWithoutAttributeCapsuleEdges(
			Set<CapsuleEdge> allCapsuleEdges) {
		Set<CapsuleEdge> res = new HashSet<CapsuleEdge>();
		for (CapsuleEdge capsuleEdge : allCapsuleEdges) {
			if (!capsuleEdge.isAttributeEdge()) {
				res.add(capsuleEdge);
			}
		}
		return res;
	}

}
