package de.uni_marburg.fb12.swt.cloneDetection.cloneDetection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Attribute;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

/**
 * 
 * Provides the basic labeling of nodes and capsuleEdges Note: the basic labels
 * are not the canonical labels
 * 
 *
 */
public class LabelCreator {


	/**
	 * Since the labels based on more that one part, this parts are separated by
	 * an Separator instead of just putting them together to avoid mistakes,
	 * like it would be in, for example: part 1: aba and part 2: c --> abac,
	 * would be the samt label as part 1: ab and part 2: ac --> abac
	 * 
	 */
	private static final String SEPARATOR = System
			.getProperty("line.separator");

	public static String getSeparator() {
		return SEPARATOR;
	}

	static Map<Attribute, String> attributeNodeNameCache = new HashMap<Attribute, String>();
	// ** Part 1 - names and toString()
	public static String getAttributeNodeName(Attribute a) {
		if (attributeNodeNameCache.containsKey(a))
			return attributeNodeNameCache.get(a);
		// return ("A: " + a.getValue());

		String result = ("A: " + a.getValue() + " Rule: "
				+ a.getGraph().getRule().getName() + " Node: "
				+ a.getNode().toString() + " - " + a.getNode().getAttributes()
				.toString());
		attributeNodeNameCache.put(a, result);
		return result;
		// return (a.getGraph().getRule().getName());
	}

	static Map<CapsuleEdge, String> capsuleEdgeNameCache = new HashMap<CapsuleEdge, String>();
	public static String getCapsuleEdgeToString(CapsuleEdge capsuleEdge) {
		if (capsuleEdgeNameCache.containsKey(capsuleEdge))
			return capsuleEdgeNameCache.get(capsuleEdge);
		String result = null;
		if (capsuleEdge.isAttributeEdge()) {
			// return getAttributeLabel(capsuleEdge.getAttribute());
			result = capsuleEdge.getRule().getName() + " - "
					+ getAttributeLabel(capsuleEdge.getAttribute());
		} else {

			StringBuilder stringBuilder = new StringBuilder();
			Action action = capsuleEdge.getAction();
			EReference type = capsuleEdge.getType();
			if (action != null) {
				if (action.toString().equals("preserve")) {
					stringBuilder.append("p ");
				}
				if (action.toString().equals("delete")) {
					stringBuilder.append("d ");
				}
				if (action.toString().equals("create")) {
					stringBuilder.append("c ");
				}
				if (action.toString().equals("forbid")) {
					stringBuilder.append("f ");
				}
				if (action.toString().equals("require")) {
					stringBuilder.append("r ");
				}
			}
			if (type != null) {
				stringBuilder.append(type.getName());
			}
			result = stringBuilder.toString();
		}
		capsuleEdgeNameCache.put(capsuleEdge, result);
		return result;
	}

	// ** Part 2 - Attributes

	static Map<Attribute, String> attributeNameCache = new HashMap<Attribute, String>();
	public static String getAttributeLabel(Attribute attribute) {
		if (attributeNameCache.containsKey(attribute))
			return attributeNameCache.get(attribute);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("a ");
		stringBuilder.append(attribute.getType().getName());
		stringBuilder.append(attribute.getValue());
		attributeNameCache.put(attribute,stringBuilder.toString());
		return stringBuilder.toString();
	}

	// ** Part 3 - ModelCD - eScan
	static Map<Node, String> eScanNodeCache = new HashMap<Node, String>();
	/**
	 * 
	 * @param node
	 * @param graph
	 *            used for method from NodeUtility, hence graph has to be the
	 *            computation- or the fragment-graph
	 * @return
	 */
	public static String getModelCdNodeLabel(Node node,
			DirectedGraph<Node, CapsuleEdge> graph) {
		if (eScanNodeCache.containsKey(node))
			return eScanNodeCache.get(node);

		String result = null;
		if (graph.containsVertex(node)) {
			if (!NodeUtility.isAttributeNode(node, graph)) {
				StringBuilder stringBuilder = new StringBuilder();

				if (node.getGraph() == null)
					System.out.println();
					
				if (node.getGraph().getRule() != null) {
					Action action = node.getActionNode().getAction();
					if (action != null) {
						if (action.toString().equals("preserve")) {
							stringBuilder.append("p ");
						}
						if (action.toString().equals("delete")) {
							stringBuilder.append("d ");
						}
						if (action.toString().equals("create")) {
							stringBuilder.append("c ");
						}
						if (action.toString().equals("forbid")) {
							stringBuilder.append("f ");
						}
						if (action.toString().equals("require")) {
							stringBuilder.append("r ");
						}
					}
				}

				stringBuilder.append(node.getType().getName());
				result = stringBuilder.toString();
			} else {
				Attribute a = NodeUtility.getAttribute(node, graph);
				result = (a.getType().toString() + " " + a.getValue());
				// return (a.getType().toString());
			}
			eScanNodeCache.put(node, result);
			return result;
		} else {
			System.out
					.println("LabelCreator - getModelCdNodeLabel: node is not contained in graph");
			return null;
		}
	}


	static Map<CapsuleEdge, String> eScanSimpleEdgeCache = new HashMap<CapsuleEdge, String>();
	public static String getSimpleCapsuleEdgeLabel(CapsuleEdge capsuleEdge) {
		if (eScanSimpleEdgeCache.containsKey(capsuleEdge))
			return eScanSimpleEdgeCache.get(capsuleEdge);
		
		String result = null;
		if (capsuleEdge.isAttributeEdge()) {
			result = getAttributeLabel(capsuleEdge.getAttribute());
		} else {
		StringBuilder stringBuilder = new StringBuilder();
		Action action = capsuleEdge.getAction();
		EReference type = capsuleEdge.getType();
		if (action != null) {
			if (action.toString().equals("preserve")) {
				stringBuilder.append("p ");
			}
			if (action.toString().equals("delete")) {
				stringBuilder.append("d ");
			}
			if (action.toString().equals("create")) {
				stringBuilder.append("c ");
			}
			if (action.toString().equals("forbid")) {
				stringBuilder.append("f ");
			}
			if (action.toString().equals("require")) {
				stringBuilder.append("r ");
			}
		}
		if (type != null) {
			stringBuilder.append(type.getName());
		}
		result = stringBuilder.toString();
		}
		eScanSimpleEdgeCache.put(capsuleEdge, result);
		return result;
	}

	public static String getModelCdEdgeLabel(CapsuleEdge h,
			DirectedGraph<Node, CapsuleEdge> graph, int indexSourceNode,
			int indexTargetNode) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(getSimpleCapsuleEdgeLabel(h));
		stringBuilder.append(" ");
		stringBuilder.append(indexSourceNode);
		stringBuilder.append(" ");
		stringBuilder
				.append(getModelCdNodeLabel(graph.getEdgeSource(h), graph));
		stringBuilder.append(" ");
		stringBuilder.append(indexTargetNode);
		stringBuilder.append(" ");
		stringBuilder
				.append(getModelCdNodeLabel(graph.getEdgeTarget(h), graph));
		stringBuilder.append(SEPARATOR);
		return stringBuilder.toString();
	}

	static Map<CapsuleEdge, String> eScanSimpleEdgeCache2 = new HashMap<CapsuleEdge, String>();
	public static String getSimpleModelCdEdgeLabel(CapsuleEdge h,
			DirectedGraph<Node, CapsuleEdge> graph) {
		if (eScanSimpleEdgeCache2.containsKey(h))
			return eScanSimpleEdgeCache2.get(h);
		
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(getSimpleCapsuleEdgeLabel(h));
		stringBuilder.append(" ");
		stringBuilder
				.append(getModelCdNodeLabel(graph.getEdgeSource(h), graph));
		stringBuilder.append(" ");
		stringBuilder
				.append(getModelCdNodeLabel(graph.getEdgeTarget(h), graph));
		stringBuilder.append(SEPARATOR);
		eScanSimpleEdgeCache2.put(h, stringBuilder.toString());
		return stringBuilder.toString();
	}

	// ** Part 4 - CloneDetective

	public static String getCloneDetectiveNodeLabel(Node node,
			DirectedGraph<Node, CapsuleEdge> graph) {
		return getModelCdNodeLabel(node, graph);
	}

	public static boolean haveEqualCloneDetectiveLabels(Node node1, Node node2,
			DirectedGraph<Node, CapsuleEdge> graph1,
			DirectedGraph<Node, CapsuleEdge> graph2) {
		return getCloneDetectiveNodeLabel(node1, graph1).equals(
				getCloneDetectiveNodeLabel(node2, graph2));
	}

	public static boolean haveEqualCloneDetectiveLabels(Node node1, Node node2,
			Node node3, Node node4, DirectedGraph<Node, CapsuleEdge> graph1,
			DirectedGraph<Node, CapsuleEdge> graph2,
			DirectedGraph<Node, CapsuleEdge> graph3,
			DirectedGraph<Node, CapsuleEdge> graph4) {

		if ((LabelCreator.haveEqualCloneDetectiveLabels(node1, node2, graph1,
				graph2))
				&& (LabelCreator.haveEqualCloneDetectiveLabels(node1, node3,
						graph1, graph3))
				&& (LabelCreator.haveEqualCloneDetectiveLabels(node1, node4,
						graph1, graph4))) {
			return true;
		}
		return false;
	}

	public static boolean haveEqualCloneDetectiveLabels(List<Node> nodes,
			List<DirectedGraph<Node, CapsuleEdge>> graphs) {
		String label1 = getCloneDetectiveNodeLabel(nodes.get(0), graphs.get(0));
		boolean labelsAreEqual = true;
		for (Node node : nodes) {
			int index = nodes.indexOf(node);
			if (!(label1.equals(getCloneDetectiveNodeLabel(node,
					graphs.get(index))))) {
				labelsAreEqual = false;
			}
		}
		return labelsAreEqual;
	}

	public static boolean haveEqualCloneDetectiveCapsuleEdgeLabels(
			CapsuleEdge capsuleEdge1, CapsuleEdge capsuleEdge2) {
		return getSimpleCapsuleEdgeLabel(capsuleEdge1).equals(
				getSimpleCapsuleEdgeLabel(capsuleEdge2));
	}

	public static boolean haveEqualCloneDetectiveCapsuleEdgeLabels(
			List<CapsuleEdge> capsuleEdges) {
		String label1 = getSimpleCapsuleEdgeLabel(capsuleEdges.get(0));
		boolean labelsAreEqual = true;
		for (CapsuleEdge capsuleEdge : capsuleEdges) {
			int index = capsuleEdges.indexOf(capsuleEdge);
			if (!(label1.equals(getSimpleCapsuleEdgeLabel(capsuleEdges
					.get(index))))) {
				labelsAreEqual = false;
			}
		}
		return labelsAreEqual;
	}

}
