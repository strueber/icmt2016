package de.uni_marburg.fb12.swt.cloneDetection.cloneDetection;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
//import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;


import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.ClonePair;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.CloneTupel;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.NodeTupel;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

/**
 * 
 * Provides conversion to CloneGroupMappings for all (temp-) results of the
 * CloneDetections.
 * 
 * In some special cases this includes addingAttributes.
 */
public class CloneMatrixCreator {

	public static Set<CloneMatrix> convertEScanWithoutAttibuteResultAndAddAttributes(
			Set<Set<Fragment>> setOfSetsOfSameCanonicalLabeledNonOverlappingFragments,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<CloneMatrix> res = new HashSet<CloneMatrix>();

		for (Set<Fragment> fragmentsCloneGroup 
						: setOfSetsOfSameCanonicalLabeledNonOverlappingFragments) {
			if (fragmentsCloneGroup.size() > 1) {
				res.add(convertAndAddAttributes(fragmentsCloneGroup,
						ruleGraphMap));
			}
		}
		return res;
	}

	private static CloneMatrix convertAndAddAttributes(
			Set<Fragment> cloneGroup,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<Rule> rules = new HashSet<Rule>();
		for (Fragment f : cloneGroup) {
			rules.add(f.getRule());
		}

		CloneMatrix cloneMatrixEdges = convert(cloneGroup);
		List<List<Edge>> edgeMatrix = cloneMatrixEdges.getEdgeMatrix();

		Set<List<Attribute>> attributeColumns = getAttributeColumns(
				getCapsuleEdgeMatrix(cloneGroup), ruleGraphMap);

		List<List<Attribute>> attributeMatrix = new LinkedList<List<Attribute>>();
		if ((attributeColumns != null)
				&& (attributeColumns.iterator().hasNext())) {
			int size = attributeColumns.iterator().next().size();
			for (int row = 0; row < size; row++) {
				attributeMatrix.add(new LinkedList<Attribute>());
			}

			for (List<Attribute> column : attributeColumns) {
				for (int rowPosition = 0; rowPosition < column.size(); rowPosition++) {
					attributeMatrix.get(rowPosition).add(
							column.get(rowPosition));
				}
			}

		}
		return new CloneMatrix(edgeMatrix, attributeMatrix);
	}

	private static List<List<CapsuleEdge>> getCapsuleEdgeMatrix(
			Set<Fragment> cloneGroup) {
		List<List<CapsuleEdge>> capsuleEdgeMatrix = new LinkedList<List<CapsuleEdge>>();

		for (Fragment fragment : cloneGroup) {
			List<CapsuleEdge> capsuleEdges = fragment.getCapsuleEdges();
			capsuleEdgeMatrix.add(capsuleEdges);
		}

		return capsuleEdgeMatrix;
	}

	private static Set<List<Attribute>> getAttributeColumns(
			List<List<CapsuleEdge>> capsuleEdgeMatrix,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {

		Set<List<Attribute>> attributeColumns = new HashSet<List<Attribute>>();

		for (int column = 0; column < capsuleEdgeMatrix.get(0).size(); column++) { // c
																					// column
			/*
			 * key : attributes of (source/target)Nodes from first Edge value:
			 * key and clones of key of (source/target)Node from all other Edges
			 */
			Map<Attribute, List<Attribute>> sourceNodeAttributes 
			= new HashMap<Attribute, List<Attribute>>();
			Map<Attribute, List<Attribute>> targetNodeAttributes 
			= new HashMap<Attribute, List<Attribute>>();
			boolean init = false;
			for (List<CapsuleEdge> row : capsuleEdgeMatrix) {
				CapsuleEdge capsuleEdge = row.get(column);
				DirectedGraph<Node, CapsuleEdge> graph = ruleGraphMap
						.get(capsuleEdge.getRule());
				Node source = graph.getEdgeSource(capsuleEdge);
				Node target = graph.getEdgeTarget(capsuleEdge);
				if (!init) {
					for (Attribute attribute : source.getActionNode()
							.getAttributes()) {
						List<Attribute> setS = new LinkedList<Attribute>();
						setS.add(attribute);
						sourceNodeAttributes.put(attribute, setS);
					}
					if (!capsuleEdge.isAttributeEdge()) {
						for (Attribute attribute : target.getActionNode()
								.getAttributes()) {
							List<Attribute> setT = new LinkedList<Attribute>();
							setT.add(attribute);
							targetNodeAttributes.put(attribute, setT);
						}
					}
					init = true;
				} else {
					Set<Attribute> sourceNodeAttributesToRemove = new HashSet<Attribute>();
					Set<Attribute> targetNodeAttributesToRemove = new HashSet<Attribute>();
					for (Attribute attribute : sourceNodeAttributes.keySet()) {
						boolean found = false;
						Attribute foundAttribute = null;
						for (Attribute a : source.getActionNode()
								.getAttributes()) {
							if (LabelCreator.getAttributeLabel(attribute)
									.equals(LabelCreator.getAttributeLabel(a))) {
								found = true;
								foundAttribute = a;
							}
						}
						if (found) {
							sourceNodeAttributes.get(attribute).add(
									foundAttribute);
						} else {
							sourceNodeAttributesToRemove.add(attribute);
						}
					}
					for (Attribute attribute : targetNodeAttributes.keySet()) {
						boolean found = false;
						Attribute foundAttribute = null;
						for (Attribute a : target.getActionNode()
								.getAttributes()) {
							if (LabelCreator.getAttributeLabel(attribute)
									.equals(LabelCreator.getAttributeLabel(a))) {
								found = true;
								foundAttribute = a;
							}
						}
						if (found) {
							targetNodeAttributes.get(attribute).add(
									foundAttribute);
						} else {
							targetNodeAttributesToRemove.add(attribute);
						}
					}

					for (Attribute attribute : sourceNodeAttributesToRemove) {
						sourceNodeAttributes.remove(attribute);
					}
					for (Attribute attribute : targetNodeAttributesToRemove) {
						targetNodeAttributes.remove(attribute);
					}
				}

				if (sourceNodeAttributes.isEmpty()
						&& targetNodeAttributes.isEmpty()) {
					break;
				}
			}

			attributeColumns.addAll(sourceNodeAttributes.values());
			attributeColumns.addAll(targetNodeAttributes.values());
		}
		return attributeColumns;

	}

	public static Set<CloneMatrix> convertEScanResult(
			Set<Set<Fragment>> setOfSetsOfSameCanonicalLabeledNonOverlappingFragments) {
		Set<CloneMatrix> res = new HashSet<CloneMatrix>();

		for (Set<Fragment> fragmentsCloneGroup 
				: setOfSetsOfSameCanonicalLabeledNonOverlappingFragments) {
			if (fragmentsCloneGroup.size() > 1) {
				res.add(CloneMatrixCreator.convert(fragmentsCloneGroup));
			}
		}
		return res;
	}

	private static CloneMatrix convert(Set<Fragment> cloneGroup) {

		Set<Rule> rules = new HashSet<Rule>();
		for (Fragment f : cloneGroup) {
			rules.add(f.getRule());
		}

		List<List<Edge>> edgeMatrix = new LinkedList<List<Edge>>();
		List<List<Attribute>> attributeMatrix = new LinkedList<List<Attribute>>();

		for (Fragment fragment : cloneGroup) {
			List<Edge> originalEdges = new LinkedList<Edge>();
			List<Attribute> attributes = new LinkedList<Attribute>();
			List<CapsuleEdge> capsuleEdges = fragment.getCapsuleEdges();
			for (CapsuleEdge capsuleEdge : capsuleEdges) {
				if (capsuleEdge.isAttributeEdge()) {
					attributes.add(capsuleEdge.getAttribute()
							.getActionAttribute());
				} else {
					originalEdges.add(capsuleEdge.getOriginalEdge()
							.getActionEdge());
				}
			}
			attributeMatrix.add(attributes);
			edgeMatrix.add(originalEdges);
		}

		CloneMatrix res = new CloneMatrix(edgeMatrix, attributeMatrix);
		return res;
	}

	public static Set<CloneGroupMapping> convertEScanResultOld(
			Collection<Set<Fragment>> setOfSetsOfSameCanonicalLabeledNonOverlappingFragments) {
		Set<CloneGroupMapping> res = new HashSet<CloneGroupMapping>();

		for (Set<Fragment> fragments : setOfSetsOfSameCanonicalLabeledNonOverlappingFragments) {
			if (fragments.size() > 1) {
				res.add(CloneMatrixCreator.convertOld(fragments));
			}
		}
		return res;
	}

	private static CloneGroupMapping convertOld(Set<Fragment> cloneGroupFragment) {
		Set<Rule> rules = new HashSet<Rule>();
		for (Fragment f : cloneGroupFragment) {
			rules.add(f.getRule());
		}

		// Die Klassenfelder von CloneGroup
		Map<Edge, Map<Rule, Edge>> edgeMappings = new HashMap<Edge, Map<Rule, Edge>>();
		Map<Attribute, Map<Rule, Attribute>> attributeMappings 
		= new HashMap<Attribute, Map<Rule, Attribute>>();

		for (Fragment f : cloneGroupFragment) {
			for (CapsuleEdge capsuleEdge : f.getCapsuleEdges()) {
				Map<Rule, Edge> tempMapEdge = new HashMap<Rule, Edge>();
				Map<Rule, Attribute> tempMapAttribute = new HashMap<Rule, Attribute>();

				if (capsuleEdge.isAttributeEdge()) {
					tempMapAttribute.put(f.getRule(),
							capsuleEdge.getAttribute());
					for (Fragment f2 : cloneGroupFragment) {
						if (!(f2 == f)) {
							for (CapsuleEdge capsuleEdge2 : f2
									.getCapsuleEdges()) {
								if (capsuleEdge2.isAttributeEdge()) {
									if (capsuleEdge.getAttribute() == capsuleEdge2
											.getAttribute()) {
										tempMapAttribute.put(f2.getRule(),
												capsuleEdge.getAttribute());
									}
								}
							}
						}
					}
					for (Attribute a : tempMapAttribute.values()) {
						attributeMappings.put(a, tempMapAttribute);
					}
				} else {
					tempMapEdge.put(f.getRule(), capsuleEdge.getOriginalEdge());
					for (Fragment f2 : cloneGroupFragment) {
						if (!(f2 == f)) {
							for (CapsuleEdge capsuleEdge2 : f2
									.getCapsuleEdges()) {
								if (!capsuleEdge2.isAttributeEdge()) {
									if (capsuleEdge.getOriginalEdge() == capsuleEdge2
											.getOriginalEdge()) {
										tempMapEdge.put(f2.getRule(),
												capsuleEdge.getOriginalEdge());
									}
								}
							}
						}
					}
					for (Edge e : tempMapEdge.values()) {
						edgeMappings.put(e, tempMapEdge);
					}
				}

			}
		}
		return new CloneGroupMapping(rules, edgeMappings, attributeMappings);
	}

	// ** CloneDetective

	public static Set<CloneMatrix> convertClonePairSet(
			Collection<ClonePair> clonePairs,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<CloneMatrix> res = new HashSet<CloneMatrix>();
		for (ClonePair clonePair : clonePairs) {
			CloneMatrix cloneMatrix = convertClonePair(clonePair, ruleGraphMap);
			if ((cloneMatrix.getEdgeMatrix().size() > 0)
					|| (cloneMatrix.getAttributeMatrix().size() > 0)) {
				res.add(cloneMatrix);
			}
		}
		return res;
	}

	private static CloneMatrix convertClonePair(ClonePair clonePair,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<Rule> rules = new HashSet<Rule>();
		Rule rule1 = NodeUtility.getRule(clonePair.getNodePairs().iterator()
				.next().getNode1(), ruleGraphMap);
		Rule rule2 = NodeUtility.getRule(clonePair.getNodePairs().iterator()
				.next().getNode2(), ruleGraphMap);
		rules.add(rule1);
		rules.add(rule2);

		List<List<Edge>> edgeMatrix = new LinkedList<List<Edge>>();
		List<List<Attribute>> attributeMatrix = new LinkedList<List<Attribute>>();

		List<Edge> originalEdges1 = new LinkedList<Edge>();
		List<Attribute> attributes1 = new LinkedList<Attribute>();
		for (CapsuleEdge capsuleEdge : clonePair.getCapsuleEdges1()) {
			if (capsuleEdge.isAttributeEdge()) {
				attributes1
						.add(capsuleEdge.getAttribute().getActionAttribute());
			} else {
				originalEdges1.add(capsuleEdge.getOriginalEdge()
						.getActionEdge());
			}
		}
		attributeMatrix.add(attributes1);
		edgeMatrix.add(originalEdges1);

		List<Edge> originalEdges2 = new LinkedList<Edge>();
		List<Attribute> attributes2 = new LinkedList<Attribute>();
		for (CapsuleEdge capsuleEdge : clonePair.getCapsuleEdges2()) {
			if (capsuleEdge.isAttributeEdge()) {
				attributes2
						.add(capsuleEdge.getAttribute().getActionAttribute());
			} else {
				originalEdges2.add(capsuleEdge.getOriginalEdge()
						.getActionEdge());
			}
		}
		attributeMatrix.add(attributes2);
		edgeMatrix.add(originalEdges2);

		return new CloneMatrix(edgeMatrix, attributeMatrix);
	}

	public static Set<CloneMatrix> convertCloneTupelSet(
			Collection<CloneTupel> cloneTupels,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<CloneMatrix> res = new HashSet<CloneMatrix>();
		for (CloneTupel cloneTupel : cloneTupels) {
			CloneMatrix cloneMatrix = convertCloneTupel(cloneTupel,
					ruleGraphMap);
			if ((cloneMatrix.getEdgeMatrix().size() > 0)
					|| (cloneMatrix.getAttributeMatrix().size() > 0)) {
				res.add(cloneMatrix);
			}
		}
		return res;
	}

	private static CloneMatrix convertCloneTupel(CloneTupel cloneTupel,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {

		List<Rule> ruleList = new LinkedList<Rule>();
		Set<Rule> rules = new HashSet<Rule>();
		NodeTupel nodeTupel = cloneTupel.getNodeTupels().iterator().next();
		for (Node node : nodeTupel.getNodeTupelAsNodeList()) {
			Rule rule = NodeUtility.getRule(node, ruleGraphMap);
			rules.add(rule);
			ruleList.add(rule);
		}

		List<List<Edge>> edgeMatrix = new LinkedList<List<Edge>>();
		List<List<Attribute>> attributeMatrix = new LinkedList<List<Attribute>>();

		for (List<CapsuleEdge> capsuleEdges : cloneTupel.getCapsuleEdges()) {
			List<Edge> originalEdges1 = new LinkedList<Edge>();
			List<Attribute> attributes1 = new LinkedList<Attribute>();
			for (CapsuleEdge capsuleEdge : capsuleEdges) {
				if (capsuleEdge.isAttributeEdge()) {
					attributes1.add(capsuleEdge.getAttribute()
							.getActionAttribute());
				} else {
					originalEdges1.add(capsuleEdge.getOriginalEdge()
							.getActionEdge());
				}
			}
			attributeMatrix.add(attributes1);
			edgeMatrix.add(originalEdges1);
		}

		return new CloneMatrix(edgeMatrix, attributeMatrix);

	}

}
