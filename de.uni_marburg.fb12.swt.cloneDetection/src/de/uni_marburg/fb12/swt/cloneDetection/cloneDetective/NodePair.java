package de.uni_marburg.fb12.swt.cloneDetection.cloneDetective;

import org.eclipse.emf.henshin.model.Node;

/**
 * 
 * A Pair of Nodes
 *
 */
public class NodePair {
	private Node node1;
	private Node node2;

	public NodePair(Node node1, Node node2) {
		this.node1 = node1;
		this.node2 = node2;
	}

	public boolean nodesAreIdentical() {
		return (node1 == node2);
	}

	public Node getNode1() {
		return node1;
	}

	public Node getNode2() {
		return node2;
	}

	@Override
	public String toString() {
		return node1.toString() + " - " + node2.toString();
	}

	@Override
	public int hashCode() {
		return node1.hashCode() + node2.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		NodePair np = (NodePair) o;
		if (!node1.equals(np.getNode1())) {
			return false;
		}
		if (!node2.equals(np.getNode2())) {
			return false;
		}
		return true;
	}
}
