package de.uni_marburg.fb12.swt.cloneDetection.cloneDetective;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

public class ClonePair {

	/**
	 * the NodePairs of this ClonePair
	 */
	private Set<NodePair> nodePairs;

	/**
	 * the CapsuleEdges between the Nodes at the first position of the NodePairs
	 * in nodePairs the CapsuleEdge at the k-th position of capsuleEdges1 is a
	 * clone of the CapsuleEdge at the k-th position of capsuleEdges2
	 */
	private List<CapsuleEdge> capsuleEdges1;

	/**
	 * the edges between the Nodes at the second position of the NodePairs in
	 * nodePairs the edge at the k-th position of capsuleEdges2 is a clone of
	 * the edge at the k-th position of capsuleEdges1
	 */
	private List<CapsuleEdge> capsuleEdges2;

	public boolean edgeSetIsEmpty() {
		if (capsuleEdges1.size() != 0) {
			return false;
		}

		if (capsuleEdges2.size() != 0) {
			return false;
		}

		return true;
	}

	public Set<Fragment> getAsFragments(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		if (capsuleEdges1.size() == 0) {
			return null;
		}
		Set<Fragment> res = new HashSet<Fragment>();

		Rule rule1 = capsuleEdges1.get(0).getRule();
		Rule rule2 = capsuleEdges2.get(0).getRule();
		Set<CapsuleEdge> capsuleEdgeSet1 = new HashSet<CapsuleEdge>();
		Set<CapsuleEdge> capsuleEdgeSet2 = new HashSet<CapsuleEdge>();
		capsuleEdgeSet1.addAll(capsuleEdges1);
		capsuleEdgeSet2.addAll(capsuleEdges2);
		DirectedGraph<Node, CapsuleEdge> graph1neu = ruleGraphMap.get(rule1);
		DirectedGraph<Node, CapsuleEdge> graph2neu = ruleGraphMap.get(rule2);
		Fragment f1 = new Fragment(capsuleEdgeSet1, rule1, graph1neu);
		Fragment f2 = new Fragment(capsuleEdgeSet2, rule2, graph2neu);
		res.add(f1);
		res.add(f2);
		return res;
	}

	@Override
	public String toString() {
		String res = "NodePairs: + \n";
		for (NodePair nodePair : nodePairs) {
			res = res + nodePair.toString() + "\t";
		}

		res = res + "\n" + "CapsuleEdges" + "\n";

		for (CapsuleEdge capsuleEdge : capsuleEdges1) {
			res = res + capsuleEdge.toString();
		}

		return res;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		ClonePair cp = (ClonePair) o;
		return nodePairs.equals(cp.nodePairs);
	}

	public Set<NodePair> getNodePairs() {
		return nodePairs;
	}

	public ClonePair(Set<NodePair> nodePairs,
			DirectedGraph<Node, CapsuleEdge> graph1,
			DirectedGraph<Node, CapsuleEdge> graph2) {
		this.nodePairs = nodePairs;
		setCapsuleEdges(graph1, graph2);
	}

	public List<List<CapsuleEdge>> getCapsuleEdges() {
		List<List<CapsuleEdge>> res = new LinkedList<List<CapsuleEdge>>();
		res.add(capsuleEdges1);
		res.add(capsuleEdges2);
		return res;
	}

	public List<CapsuleEdge> getCapsuleEdges1() {
		return capsuleEdges1;
	}

	public List<CapsuleEdge> getCapsuleEdges2() {
		return capsuleEdges2;
	}

	public Set<Node> getNodes1() {
		Set<Node> res = new HashSet<Node>();
		for (NodePair nodePair : nodePairs) {
			res.add(nodePair.getNode1());
		}
		return res;
	}

	public Set<Node> getNodes2() {
		Set<Node> res = new HashSet<Node>();
		for (NodePair nodePair : nodePairs) {
			res.add(nodePair.getNode2());
		}
		return res;
	}

	public int size() {
		return nodePairs.size();
	}

	public Rule getRule1() {
		List<CapsuleEdge> temp = capsuleEdges1;
		CapsuleEdge ce = temp.get(0);
		return ce.getRule();
	}

	public Rule getRule2() {
		return capsuleEdges2.get(0).getRule();
	}

	/**
	 * Only exact clones are allowed, so wrong, means not cloned, edges are not
	 * set.
	 * 
	 * @param graph1
	 * @param graph2
	 */
	private void setCapsuleEdges(DirectedGraph<Node, CapsuleEdge> graph1,
			DirectedGraph<Node, CapsuleEdge> graph2) {
		capsuleEdges1 = new LinkedList<CapsuleEdge>();
		capsuleEdges2 = new LinkedList<CapsuleEdge>();
		Map<Node, NodePair> nodeNodePairMap = new HashMap<Node, NodePair>();

		for (NodePair nodePair : nodePairs) {
			nodeNodePairMap.put(nodePair.getNode1(), nodePair);
			nodeNodePairMap.put(nodePair.getNode2(), nodePair);
		}

		for (NodePair nodePair : nodePairs) {

			Node nodeInGraph1 = nodePair.getNode1();
			Node nodeInGraph2 = nodePair.getNode2();

			Set<CapsuleEdge> inCapsuleEdgesNode1 = graph1
					.incomingEdgesOf(nodeInGraph1);
			for (CapsuleEdge capsuleEdge : inCapsuleEdgesNode1) {
				if (!capsuleEdges1.contains(capsuleEdge)) {
					Node node1out = graph1.getEdgeSource(capsuleEdge);
					if (nodeNodePairMap.containsKey(node1out)) {
						Node nodeOutCandidateGraph2 = nodeNodePairMap.get(
								node1out).getNode2();

						CapsuleEdge cloneCapsuleEdgeCandidate = graph2.getEdge(
								nodeOutCandidateGraph2, nodeInGraph2);

						if (cloneCapsuleEdgeCandidate != null) {
							if (LabelCreator
									.haveEqualCloneDetectiveCapsuleEdgeLabels(
											capsuleEdge,
											cloneCapsuleEdgeCandidate)) {
								capsuleEdges1.add(capsuleEdge);
								capsuleEdges2.add(cloneCapsuleEdgeCandidate);
							}
						}
					}
				}
			}
			// -------------------------------

			Node nodeOutGraph1 = nodePair.getNode1();
			Node nodeOutGraph2 = nodePair.getNode2();

			Set<CapsuleEdge> outCapsuleEdgesNode1 = graph1
					.outgoingEdgesOf(nodeOutGraph1);
			for (CapsuleEdge capsuleEdge : outCapsuleEdgesNode1) {
				if (!capsuleEdges1.contains(capsuleEdge)) {
					Node node1in = graph1.getEdgeTarget(capsuleEdge);
					if (nodeNodePairMap.containsKey(node1in)) {
						Node nodeInCandidateGraph2 = nodeNodePairMap.get(
								node1in).getNode2();

						CapsuleEdge cloneCapsuleEdgeCandidate = graph2.getEdge(
								nodeOutGraph2, nodeInCandidateGraph2);

						if (cloneCapsuleEdgeCandidate != null) {
							if (LabelCreator
									.haveEqualCloneDetectiveCapsuleEdgeLabels(
											capsuleEdge,
											cloneCapsuleEdgeCandidate)) {
								capsuleEdges1.add(capsuleEdge);
								capsuleEdges2.add(cloneCapsuleEdgeCandidate);
							}
						}
					}
				}
			}

		}

		Set<Node> nodes = new HashSet<Node>();
		for (CapsuleEdge capsuleEdge : capsuleEdges1) {
			nodes.add(graph1.getEdgeSource(capsuleEdge));
			nodes.add(graph1.getEdgeTarget(capsuleEdge));
		}

		for (CapsuleEdge capsuleEdge : capsuleEdges2) {
			nodes.add(graph2.getEdgeSource(capsuleEdge));
			nodes.add(graph2.getEdgeTarget(capsuleEdge));
		}

		Set<Node> nodes2 = new HashSet<Node>();
		for (NodePair nodePair : nodePairs) {
			nodes2.add(nodePair.getNode1());
			nodes2.add(nodePair.getNode2());
		}

	}

}
