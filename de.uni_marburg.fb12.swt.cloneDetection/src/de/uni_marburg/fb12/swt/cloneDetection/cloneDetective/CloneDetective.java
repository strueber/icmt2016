package de.uni_marburg.fb12.swt.cloneDetection.cloneDetective;

import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.NodeUtility;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 
 * Contains the CloneDetection-Algorithm CloneDetective
 *
 */
public class CloneDetective extends CloneDetection {

	private final int NUMBER_OF_TERMS;

	public CloneDetective(Collection<Rule> rules) {
		super(rules);
		NUMBER_OF_TERMS = getNumberOfTermsForCloneDetective();
	}

	public CloneDetective(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap, ruleList, considerAttributeNodes);
		NUMBER_OF_TERMS = getNumberOfTermsForCloneDetective();
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		System.out.println(startDetectCloneGroups("CloneDetective"));
		Collection<ClonePair> res = runCloneDetective();

		System.out.println(startConversion());

		resultAsCloneMatrix = CloneMatrixCreator.convertClonePairSet(res,
				ruleGraphMap);

		System.out.println(endDetectCloneGroups("CloneDetective", startZeit));
	}

	/**
	 * the actual CloneDetective algorithm from the paper, the line comments
	 * correspond to the lines of the pseudocode from the paper
	 * 
	 */
	public Collection<ClonePair> runCloneDetective() {
		Set<Node> vertexSet = new HashSet<Node>();
		for (DirectedGraph<Node, CapsuleEdge> graph : ruleGraphMap.values()) {
			vertexSet.addAll(graph.vertexSet());
		}
		List<NodePair> nodePairSet = new LinkedList<NodePair>();
		List<DirectedGraph<Node, CapsuleEdge>> graphNodes1 
				= new LinkedList<DirectedGraph<Node, CapsuleEdge>>();
		List<DirectedGraph<Node, CapsuleEdge>> graphNodes2 
				= new LinkedList<DirectedGraph<Node, CapsuleEdge>>();
		for (Node node1 : vertexSet) {
			for (Node node2 : vertexSet) {
				if (node1 != node2) {
					DirectedGraph<Node, CapsuleEdge> graph1 = NodeUtility
							.getGraph(node1, ruleGraphMap);
					DirectedGraph<Node, CapsuleEdge> graph2 = NodeUtility
							.getGraph(node2, ruleGraphMap);
					if (LabelCreator.haveEqualCloneDetectiveLabels(node1,
							node2, graph1, graph2)) {
						NodePair nodepair = new NodePair(node1, node2);
						nodePairSet.add(nodepair);
						graphNodes1.add(graph1);
						graphNodes2.add(graph2);
					}
				}
			}
		}

		List<NodePair> sortedNodePairSet = sortNodePairsPerWeight(nodePairSet,
				graphNodes1, graphNodes2);

		List<ClonePair> clonePairs = new LinkedList<ClonePair>();

		// line 1
		Set<NodePair> doneD = new HashSet<NodePair>();
		// line 2
		for (NodePair nodePairUV : sortedNodePairSet) {
			// line 3
			if (!doneD.contains(nodePairUV)) {
				DirectedGraph<Node, CapsuleEdge> graph1 = NodeUtility.getGraph(
						nodePairUV.getNode1(), ruleGraphMap);
				DirectedGraph<Node, CapsuleEdge> graph2 = NodeUtility.getGraph(
						nodePairUV.getNode2(), ruleGraphMap);
				// line 4
				Queue<NodePair> queueQ = new LinkedBlockingQueue<NodePair>();
				queueQ.add(nodePairUV);
				Set<NodePair> currentC = new HashSet<NodePair>();
				currentC.add(nodePairUV);
				Set<Node> seenS = new HashSet<Node>();
				seenS.add(nodePairUV.getNode1());
				seenS.add(nodePairUV.getNode2());
				// line 5
				while (!queueQ.isEmpty()) {
					// line 6
					NodePair nodePairWZ = queueQ.poll();
					// line 7
					List<NodePair> adjacentP = getWeightedNeighbourhood(
							nodePairWZ, graph1, graph2, ruleGraphMap);
					for (NodePair nodePairXY : adjacentP) {
						// line 9 <-- optional
						// line 10
						if (!nodePairXY.nodesAreIdentical()) {
							if ((!seenS.contains(nodePairXY.getNode1()))
									&& (!seenS.contains(nodePairXY.getNode2()))) {
								// line 11
								currentC.add(nodePairXY);
								seenS.add(nodePairXY.getNode1());
								seenS.add(nodePairXY.getNode2());
								// line 12
								queueQ.add(nodePairXY);
							}
						}
					}

				}
				Set<NodePair> clonePairSet = new HashSet<NodePair>();
				clonePairSet.addAll(currentC);
				ClonePair clonePair = new ClonePair(clonePairSet, graph1,
						graph2);
				if (!clonePair.edgeSetIsEmpty()) {
					// line 13
					clonePairs.add(clonePair);
					// line 14
					doneD.addAll(currentC);
				}
			}
		}

		return clonePairs;
	}

	/**
	 * Condition 1 from the paper: The Labels of the Nodes in a NodePair have to
	 * be equal.
	 * 
	 * @param adjacentNodesW
	 *            Nodes from graphW
	 * @param adjacentNodesZ
	 *            Nodes from graphZ
	 * @param graphW
	 * @param graphZ
	 * @return
	 */
	private Set<NodePair> getNodePairsFulfilCondition1(
			Set<Node> adjacentNodesW, Set<Node> adjacentNodesZ,
			DirectedGraph<Node, CapsuleEdge> graphW,
			DirectedGraph<Node, CapsuleEdge> graphZ) {
		Set<NodePair> nodePairsFulFilCondition1 = new HashSet<NodePair>();
		for (Node adjacentNodeW : adjacentNodesW) {
			for (Node adjacentNodeZ : adjacentNodesZ) {
				// condition (1)
				if (LabelCreator.haveEqualCloneDetectiveLabels(adjacentNodeW,
						adjacentNodeZ, graphW, graphZ)) {
					nodePairsFulFilCondition1.add(new NodePair(adjacentNodeW,
							adjacentNodeZ));
				}
			}
		}
		return nodePairsFulFilCondition1;
	}

	/**
	 * Condition 1 and 2 from the Paper Condition 1 from the paper: The Labels
	 * of the Nodes in a NodePair have to be equal.
	 * 
	 * Condition 2 from the paper: (since there are no multiple Edges allowed in
	 * Henshin it is either/or) Either: The Label of the Edges from nodeU to the
	 * first Node of the NodePair and the Label of the Edges from nodeV to the
	 * second Node of the NodePair must be equal Or: The Label of the Edges from
	 * the first Node of the NodePair to nodeU and the Label of the Edges from
	 * the second Node of the NodePair to nodeV must be equal
	 * 
	 * @param nodePairWZ
	 * @param graphW
	 * @param graphZ
	 * @return
	 */
	private Set<NodePair> getNeighbourhoodUnderCondition1And2(
			NodePair nodePairWZ, DirectedGraph<Node, CapsuleEdge> graphW,
			DirectedGraph<Node, CapsuleEdge> graphZ) {
		Node nodeW = nodePairWZ.getNode1();
		Node nodeZ = nodePairWZ.getNode2();
		Set<Node> adjacentNodesW = NodeUtility.getAdjacentNodes(nodeW, graphW);
		Set<Node> adjacentNodesZ = NodeUtility.getAdjacentNodes(nodeZ, graphZ);
		Set<NodePair> nodePairsFulfilCondition1;
		nodePairsFulfilCondition1 = getNodePairsFulfilCondition1(
				adjacentNodesW, adjacentNodesZ, graphW, graphZ);
		if (nodePairsFulfilCondition1.size() == 0) {
			return nodePairsFulfilCondition1;
		}
		Set<NodePair> nodePairsFulfilCondition1And2;
		nodePairsFulfilCondition1And2 = getNodePairsFulfilCondition2(
				nodePairsFulfilCondition1, nodeW, nodeZ, graphW, graphZ);
		return nodePairsFulfilCondition1And2;
	}

	/**
	 * 
	 * Condition 2 from the paper: (since there are no multiple Edges allowed in
	 * Henshin it is either/or) Either: The Label of the Edges from nodeU to the
	 * first Node of the NodePair and the Label of the Edges from nodeV to the
	 * second Node of the NodePair must be equal Or: The Label of the Edges from
	 * the first Node of the NodePair to nodeU and the Label of the Edges from
	 * the second Node of the NodePair to nodeV must be equal
	 * 
	 * @param nodePairsXYFulfilCondition1
	 * @param nodeU
	 * @param nodeV
	 * @param graphU
	 * @param graphV
	 * @return
	 */
	private Set<NodePair> getNodePairsFulfilCondition2(
			Set<NodePair> nodePairsXYFulfilCondition1, Node nodeU, Node nodeV,
			DirectedGraph<Node, CapsuleEdge> graphU,
			DirectedGraph<Node, CapsuleEdge> graphV) {
		Set<NodePair> nodePairsFulFilCondition2 = new HashSet<NodePair>();

		for (NodePair nodePairXY : nodePairsXYFulfilCondition1) {
			CapsuleEdge capsuleEdgeUX = graphU.getEdge(nodeU,
					nodePairXY.getNode1());
			CapsuleEdge capsuleEdgeVY = graphV.getEdge(nodeV,
					nodePairXY.getNode2());
			if ((capsuleEdgeUX != null) && (capsuleEdgeVY != null)) {
				if (LabelCreator.haveEqualCloneDetectiveCapsuleEdgeLabels(
						capsuleEdgeUX, capsuleEdgeVY)) {
					nodePairsFulFilCondition2.add(nodePairXY);
				}
			}

			CapsuleEdge capsuleEdgeXU = graphU.getEdge(nodePairXY.getNode1(),
					nodeU);
			CapsuleEdge capsuleEdgeYV = graphV.getEdge(nodePairXY.getNode2(),
					nodeV);
			if ((capsuleEdgeXU != null) && (capsuleEdgeYV != null)) {
				if (LabelCreator.haveEqualCloneDetectiveCapsuleEdgeLabels(
						capsuleEdgeXU, capsuleEdgeYV)) {
					nodePairsFulFilCondition2.add(nodePairXY);
				}
			}
		}

		return nodePairsFulFilCondition2;
	}

	/**
	 * the positions in the lists are according to each other, means the graph
	 * at the k-th position of the graphWList/graphZList belongs to the
	 * first/second Node of the NodePairs at the k-th position of the
	 * NodePair-List
	 * 
	 * @param nodePairWZ
	 * @param graphW
	 * @param graphZ
	 * @param ruleGraphMap
	 * @return
	 */
	private List<NodePair> getWeightedNeighbourhood(NodePair nodePairWZ,
			DirectedGraph<Node, CapsuleEdge> graphW,
			DirectedGraph<Node, CapsuleEdge> graphZ,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<NodePair> nodePairsFulfilCondition1And2;
		nodePairsFulfilCondition1And2 = getNeighbourhoodUnderCondition1And2(
				nodePairWZ, graphW, graphZ);
		if (nodePairsFulfilCondition1And2.size() == 0) {
			return new LinkedList<NodePair>();
		}
		return sortNeighbourhoodNodePairsPerWeight(
				nodePairsFulfilCondition1And2, graphW, graphZ, ruleGraphMap);

	}

	/**
	 * the positions in the lists are according to each other, means the graph
	 * at the k-th position of the graphWList/graphZList belongs to the
	 * first/second Node of the NodePairs at the k-th position of the
	 * NodePair-List
	 * 
	 * @param nodePairsWZToSort
	 * @param graphW
	 * @param graphZ
	 * @param ruleGraphMap
	 * @return
	 */
	private List<NodePair> sortNeighbourhoodNodePairsPerWeight(
			Set<NodePair> nodePairsWZToSort,
			DirectedGraph<Node, CapsuleEdge> graphW,
			DirectedGraph<Node, CapsuleEdge> graphZ,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {

		Map<Double, Set<NodePair>> weightedNodePairs = new HashMap<Double, Set<NodePair>>();
		for (NodePair nodePairWZ : nodePairsWZToSort) {
			double weight = getSimilarityFunctionValue(nodePairWZ, graphW,
					graphZ);
			if (weightedNodePairs.containsKey(weight)) {
				weightedNodePairs.get(weight).add(nodePairWZ);
			} else {
				Set<NodePair> set = new HashSet<NodePair>();
				set.add(nodePairWZ);
				weightedNodePairs.put(weight, set);
			}
		}

		List<Double> weights = new LinkedList<Double>();
		weights.addAll(weightedNodePairs.keySet());
		Collections.sort(weights);

		List<NodePair> res = new LinkedList<NodePair>();

		for (int i = weights.size() - 1; i >= 0; i--) {
			res.addAll(weightedNodePairs.get(weights.get(i)));
		}

		return res;

	}

	/**
	 * the positions in the lists are according to each other, means the graph
	 * at the k-th position of the graphWList/graphZList belongs to the
	 * first/second Node of the NodePairs at the k-th position of the
	 * NodePair-List
	 * 
	 * @param nodePairsWZToSort
	 * @param graphWList
	 * @param graphZList
	 * @return
	 */
	private List<NodePair> sortNodePairsPerWeight(
			List<NodePair> nodePairsWZToSort,
			List<DirectedGraph<Node, CapsuleEdge>> graphWList,
			List<DirectedGraph<Node, CapsuleEdge>> graphZList) {

		Map<Double, Set<NodePair>> weightedNodePairs = new HashMap<Double, Set<NodePair>>();
		for (int i = 0; i < nodePairsWZToSort.size(); i++) {
			double weight = getSimilarityFunctionValue(
					nodePairsWZToSort.get(i), graphWList.get(i),
					graphZList.get(i));
			if (weightedNodePairs.containsKey(weight)) {
				weightedNodePairs.get(weight).add(nodePairsWZToSort.get(i));
			} else {
				Set<NodePair> set = new HashSet<NodePair>();
				set.add(nodePairsWZToSort.get(i));
				weightedNodePairs.put(weight, set);
			}

		}

		List<Double> weights = new LinkedList<Double>();
		weights.addAll(weightedNodePairs.keySet());
		Collections.sort(weights);

		List<NodePair> res = new LinkedList<NodePair>();

		for (int i = weights.size() - 1; i >= 0; i--) {
			res.addAll(weightedNodePairs.get(weights.get(i)));
		}

		return res;
	}

	private double getSimilarityFunctionValue(NodePair nodePairUV,
			DirectedGraph<Node, CapsuleEdge> graphU,
			DirectedGraph<Node, CapsuleEdge> graphV) {
		double res = 0;
		for (int i = 0; i < NUMBER_OF_TERMS; i++) {
			double weighting = (1 / Math.pow(2, i));
			res = res
					+ (weighting * functionSi(nodePairUV.getNode1(),
							nodePairUV.getNode2(), graphU, graphV, i));
		}
		if ((new Double(res)).isNaN()) {
			System.out
					.println("CloneDetective -  getSimilarityFunctionValue: NaN");
			return 0;
		}
		return res;
	}

	private double functionSi(Node nodeU, Node nodeV,
			DirectedGraph<Node, CapsuleEdge> graphU,
			DirectedGraph<Node, CapsuleEdge> graphV, int i) {

		if (!LabelCreator.haveEqualCloneDetectiveLabels(nodeU, nodeV, graphU,
				graphV)) {
			return 0;
		} else {
			if (i <= 0) {
				return 1;
			} else {
				double numerator = 0;
				Set<NodePair> nuNv = getNeighbourhoodUnderCondition1And2(
						new NodePair(nodeU, nodeV), graphU, graphV);
				Set<Node> adjacentNodesU = new HashSet<Node>();
				Set<Node> adjacentNodesV = new HashSet<Node>();
				for (NodePair nodePair : nuNv) {
					adjacentNodesU.add(nodePair.getNode1());
					adjacentNodesV.add(nodePair.getNode2());
				}

				for (Node adjacentNodeU : adjacentNodesU) {
					for (Node adjacentNodeV : adjacentNodesV) {
						double temp = functionSi(adjacentNodeU, adjacentNodeV,
								graphU, graphV, (i - 1));
						if (temp > numerator) {
							numerator = temp;
						}
					}
				}
				int denominator = Math.max(adjacentNodesU.size(),
						adjacentNodesV.size());
				if (numerator == 0) {
					return 0;
				} else {
					return (numerator / denominator);
				}
			}
		}
	}

}
