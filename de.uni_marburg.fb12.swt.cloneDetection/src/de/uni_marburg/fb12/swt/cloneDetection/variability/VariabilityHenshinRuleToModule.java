package de.uni_marburg.fb12.swt.cloneDetection.variability;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;
import org.eclipse.ocl.examples.pivot.PivotPackage;
import org.jgrapht.DirectedGraph;

import GraphConstraint.GraphConstraintPackage;

public class VariabilityHenshinRuleToModule {

	public static Module getNewModule(Module oldModule,
			CloneGroupMapping cloneGroupMapping, String name) {
		Module module = createModul(oldModule);
		DirectedGraph<Node, CapsuleEdge> graph 
				= CloneGoupMappingToVariabilityActionNamedDirectedGraph
				.convert(cloneGroupMapping, cloneGroupMapping.getRules());
		boolean setVariabilities = true;
		Rule rule = ActionNamedDirectedGraphToHenshinRule.getRuleGraph(graph,
				name, setVariabilities);
		Set<String> vPoints = new HashSet<String>();
		for (Rule r : cloneGroupMapping.getRules()) {
			vPoints.add(r.getName());
		}

		setForbidden(rule, vPoints);

		module.getUnits().add(rule);
		return module;
	}

	private static Module createModul(Module oldModule) {

		PivotPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();

		Module module = HenshinFactory.eINSTANCE.createModule();
		for (EPackage p : oldModule.getImports()) {
			module.getImports().add(p);
		}
		return module;
	}

	private static void setForbidden(Rule rule, Set<String> vPoints) {
		String value = "xor (";
		Iterator<String> stringIterator = vPoints.iterator();
		while (stringIterator.hasNext()) {
			value = value + stringIterator.next();
			if (stringIterator.hasNext()) {
				value = value + ", ";
			}
		}
		value = value + ")";
		// rule.setFeatureModell(value);
		rule.setForbiddenVariants(value);
	}

}
