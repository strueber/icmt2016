package de.uni_marburg.fb12.swt.cloneDetection.variability;

import java.util.HashMap;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Mapping;
import org.eclipse.emf.henshin.model.NestedCondition;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

public class ActionNamedDirectedGraphToHenshinRule {

	private static final String PRESERVE = (new Action(Action.Type.PRESERVE))
			.toString();
	private static final String DELETE = (new Action(Action.Type.DELETE))
			.toString();
	private static final String CREATE = (new Action(Action.Type.CREATE))
			.toString();
	private static final String FORBID = (new Action(Action.Type.FORBID))
			.toString();
	private static final String REQUIRE = (new Action(Action.Type.REQUIRE))
			.toString();

	private static List<Attribute> getNewAttributeList(Node formerNode,
			boolean setVariabilities) {
		List<Attribute> attributes1b = new LinkedList<Attribute>();
		for (Attribute a : formerNode.getAttributes()) {
			Attribute a1b = HenshinFactory.eINSTANCE.createAttribute();
			a1b.setType(a.getType());
			a1b.setValue(a.getValue());
			if (setVariabilities) {
				a1b.setPresenceCondition(a.getPresenceCondition());
			}
			attributes1b.add(a1b);
		}
		return attributes1b;
	}

	/**
	 * 
	 * @param graph
	 *            the action of the Nodes has to be contained in the name of the
	 *            Node (since the Actions of the ActionNodes of the Nodes
	 *            couldn�t be set)
	 * @param name
	 * @param setVariabilities
	 * @return
	 */
	public static Rule getRuleGraph(DirectedGraph<Node, CapsuleEdge> graph,
			String name, boolean setVariabilities) {
		Rule r = HenshinFactory.eINSTANCE.createRule(name);

		Graph lhs = HenshinFactory.eINSTANCE.createGraph("Lhs");
		Graph rhs = HenshinFactory.eINSTANCE.createGraph("Rhs");

		r.setLhs(lhs);
		r.setRhs(rhs);
		boolean hasForbidNodes = false;
		boolean hasRequireNodes = false;

		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(FORBID)) {
				hasForbidNodes = true;
			}
			if (n.getName().equals(REQUIRE)) {
				hasRequireNodes = true;
			}
		}

		// for forbid
		Graph nac = null;
		NestedCondition ncN = null;
		if (hasForbidNodes) {
			ncN = lhs.createNAC("");
			nac = ncN.getConclusion();
		}
		// for require
		Graph pac = null;
		NestedCondition ncP = null;
		if (hasRequireNodes) {
			ncP = lhs.createPAC("");
			pac = ncP.getConclusion();
		}
		Map<Node, Node> formerToNewNodeLhs = new HashMap<Node, Node>();
		Map<Node, Node> formerToNewNodeRhs = new HashMap<Node, Node>();

		List<Node> preserveNodesForForbid = new LinkedList<Node>();
		List<Node> preserveNodesForRequire = new LinkedList<Node>();

		// preserve, create und delete Nodes
		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(PRESERVE)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(lhs,
						n.getType(), null);
				Node node2 = HenshinFactory.eINSTANCE.createNode(rhs,
						n.getType(), null);

				if (setVariabilities) {
					node1.setPresenceCondition(n.getPresenceCondition());
					node2.setPresenceCondition(n.getPresenceCondition());
				}

				List<Attribute> attributes1 = getNewAttributeList(n,
						setVariabilities);
				List<Attribute> attributes2 = getNewAttributeList(n,
						setVariabilities);

				node1.getAttributes().addAll(attributes1);
				node2.getAttributes().addAll(attributes2);

				lhs.getNodes().add(node1);
				rhs.getNodes().add(node2);

				Mapping mapping = HenshinFactory.eINSTANCE.createMapping(node1,
						node2); // ode1, node2);
				r.getMappings().add(mapping);

				formerToNewNodeLhs.put(n, node1);
				formerToNewNodeRhs.put(n, node2);

				preserveNodesForForbid.add(node1);
				preserveNodesForRequire.add(node1);
			} else if (n.getName().equals(CREATE)) {

				Node node2 = HenshinFactory.eINSTANCE.createNode(rhs,
						n.getType(), null);
				if (setVariabilities) {
					node2.setPresenceCondition(n.getPresenceCondition());
				}

				List<Attribute> attributes2 = getNewAttributeList(n,
						setVariabilities);

				node2.getAttributes().addAll(attributes2);

				rhs.getNodes().add(node2);

				formerToNewNodeRhs.put(n, node2);

			} else if (n.getName().equals(DELETE)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(lhs,
						n.getType(), null);
				if (setVariabilities) {
					node1.setPresenceCondition(n.getPresenceCondition());
				}

				List<Attribute> attributes1 = getNewAttributeList(n,
						setVariabilities);

				node1.getAttributes().addAll(attributes1);

				lhs.getNodes().add(node1);

				formerToNewNodeLhs.put(n, node1);
			}
		}

		// forbid nodes and forbid Edges
		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(FORBID)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(nac,
						n.getType(), null);
				if (setVariabilities) {
					node1.setPresenceCondition(n.getPresenceCondition());
				}

				List<Attribute> attributes1 = getNewAttributeList(n,
						setVariabilities);

				node1.getAttributes().addAll(attributes1);

				nac.getNodes().add(node1);

				// forbid Edges

				for (CapsuleEdge ce : (graph.incomingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node sourceNode = graph.getEdgeSource(ce);

					Node node1b = HenshinFactory.eINSTANCE.createNode(nac,
							sourceNode.getType(), null);
					if (setVariabilities) {
						node1b.setPresenceCondition(sourceNode
								.getPresenceCondition());
					}
					List<Attribute> attributes1b = getNewAttributeList(
							sourceNode, setVariabilities);

					node1b.getAttributes().addAll(attributes1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(sourceNode)), node1b);
					ncN.getMappings().add(mapping);
					preserveNodesForForbid.remove(formerToNewNodeLhs
							.get(sourceNode));

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							node1b, node1, e.getType());

					if (setVariabilities) {
						createEdge1.setPresenceCondition(e
								.getPresenceCondition());
					}
					nac.getEdges().add(createEdge1);

				}

				for (CapsuleEdge ce : (graph.outgoingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node targetNode = graph.getEdgeTarget(ce);

					Node node1b = HenshinFactory.eINSTANCE.createNode(nac,
							targetNode.getType(), null);
					if (setVariabilities) {
						node1b.setPresenceCondition(targetNode
								.getPresenceCondition());
					}

					List<Attribute> attributes1b = getNewAttributeList(
							targetNode, setVariabilities);

					node1b.getAttributes().addAll(attributes1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(targetNode)), node1b);
					ncN.getMappings().add(mapping);
					preserveNodesForForbid.remove(formerToNewNodeLhs
							.get(targetNode));

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							node1, node1b, e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e
								.getPresenceCondition());
					}
					nac.getEdges().add(createEdge1);

				}
			}

		}

		if (hasForbidNodes) {
			for (Node node : preserveNodesForForbid) {
				Node node2 = HenshinFactory.eINSTANCE.createNode(nac,
						node.getType(), null);
				if (setVariabilities) {
					node2.setPresenceCondition(node.getPresenceCondition());
				}
				List<Attribute> attributes = getNewAttributeList(node,
						setVariabilities);
				node2.getAttributes().addAll(attributes);
				Mapping mapping = HenshinFactory.eINSTANCE.createMapping(node,
						node2);
				ncN.getMappings().add(mapping);
			}
		}

		// require Nodes and require Edges
		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(REQUIRE)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(pac,
						n.getType(), null);
				if (setVariabilities) {
					node1.setPresenceCondition(n.getPresenceCondition());
				}

				List<Attribute> attributes1 = getNewAttributeList(n,
						setVariabilities);

				node1.getAttributes().addAll(attributes1);

				pac.getNodes().add(node1);

				// require Edges
				for (CapsuleEdge ce : (graph.incomingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node sourceNode = graph.getEdgeSource(ce);

					Node node1b = HenshinFactory.eINSTANCE.createNode(pac,
							sourceNode.getType(), null);
					if (setVariabilities) {
						node1b.setPresenceCondition(sourceNode
								.getPresenceCondition());
					}

					List<Attribute> attributes1b = getNewAttributeList(
							sourceNode, setVariabilities);

					node1b.getAttributes().addAll(attributes1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(sourceNode)), node1b);
					ncP.getMappings().add(mapping);

					preserveNodesForRequire.remove(formerToNewNodeLhs
							.get(sourceNode));

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							node1b, node1, e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e
								.getPresenceCondition());
					}
					pac.getEdges().add(createEdge1);

				}

				for (CapsuleEdge ce : (graph.outgoingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node targetNode = graph.getEdgeTarget(ce);

					Node node1b = HenshinFactory.eINSTANCE.createNode(pac,
							targetNode.getType(), null);
					if (setVariabilities) {
						node1b.setPresenceCondition(targetNode
								.getPresenceCondition());
					}

					List<Attribute> attributes1b = getNewAttributeList(
							targetNode, setVariabilities);

					node1b.getAttributes().addAll(attributes1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(targetNode)), node1b);
					ncP.getMappings().add(mapping);

					preserveNodesForRequire.remove(formerToNewNodeLhs
							.get(targetNode));

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							node1, node1b, e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e
								.getPresenceCondition());
					}
					pac.getEdges().add(createEdge1);

				}
			}
		}

		if (hasRequireNodes) {
			for (Node node : preserveNodesForRequire) {
				Node node2 = HenshinFactory.eINSTANCE.createNode(pac,
						node.getType(), null);
				if (setVariabilities) {
					node2.setPresenceCondition(node.getPresenceCondition());
				}
				List<Attribute> attributes = getNewAttributeList(node,
						setVariabilities);
				node2.getAttributes().addAll(attributes);
				Mapping mapping = HenshinFactory.eINSTANCE.createMapping(node,
						node2);
				ncP.getMappings().add(mapping);
			}
		}

		// remaining edges

		for (Object tempEdge : graph.edgeSet()) {
			CapsuleEdge e = (CapsuleEdge) tempEdge;
			Node source = graph.getEdgeSource(e);
			Node target = graph.getEdgeTarget(e);

			if (source.getName().equals(PRESERVE)) {
				if (target.getName().equals(PRESERVE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					if (setVariabilities) {
						createEdge2.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getLhs().getEdges().add(createEdge1);
					r.getRhs().getEdges().add(createEdge2);
				}
				if (target.getName().equals(CREATE)) {
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge2.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getRhs().getEdges().add(createEdge2);
				}
				if (target.getName().equals(DELETE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getLhs().getEdges().add(createEdge1);
				}

			}

			if (source.getName().equals(CREATE)) {
				if ((target.getName().equals(PRESERVE))) {
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge2.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getRhs().getEdges().add(createEdge2);
				}

				if ((target.getName().equals(CREATE))) {
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge2.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getRhs().getEdges().add(createEdge2);
				}

			}

			if (source.getName().equals(DELETE)) {
				if (target.getName().equals(PRESERVE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getLhs().getEdges().add(createEdge1);
				}

				if (target.getName().equals(DELETE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					if (setVariabilities) {
						createEdge1.setPresenceCondition(e.getOriginalEdge()
								.getPresenceCondition());
					}
					r.getLhs().getEdges().add(createEdge1);
				}
			}

		}

		return r;
	}

}
