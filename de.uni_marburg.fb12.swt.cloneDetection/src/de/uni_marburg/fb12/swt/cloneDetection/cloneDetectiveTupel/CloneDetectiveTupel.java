package de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel;

import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.NodeUtility;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 
 * Contains the CloneDetection-Algorithm CloneDetectiveTupel
 *
 */
public class CloneDetectiveTupel extends CloneDetection {

	private final int NUMBER_OF_TERMS;

	public CloneDetectiveTupel(Collection<Rule> rules) {
		super(rules);
		NUMBER_OF_TERMS = getNumberOfTermsForCloneDetective();
	}

	public CloneDetectiveTupel(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap, ruleList, considerAttributeNodes);
		NUMBER_OF_TERMS = getNumberOfTermsForCloneDetective();
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		System.out.println(startDetectCloneGroups("CloneDetectiveTupel"));

		Collection<CloneTupel> res = runCloneDetectiveTupel();

		System.out.println(startConversion());

		resultAsCloneMatrix = CloneMatrixCreator.convertCloneTupelSet(res,
				ruleGraphMap);

		System.out.println(endDetectCloneGroups("CloneDetectiveTupel",
				startZeit));
	}

	private Set<Node> removeAttributeNodes(Set<Node> nodes, Rule rule) {
		Set<Node> res = new HashSet<Node>();
		for (Node node : nodes) {
			if (!NodeUtility.isAttributeNode(node, ruleGraphMap.get(rule))) {
				res.add(node);
			}
		}
		return res;
	}

	private Set<NodeTupel> getStartNodeTupelSet() {
		List<Set<Node>> nodes = new LinkedList<Set<Node>>();
		List<DirectedGraph<Node, CapsuleEdge>> graphs 
				= new LinkedList<DirectedGraph<Node, CapsuleEdge>>();
		for (Rule rule : ruleList) {
			graphs.add(ruleGraphMap.get(rule));
			Set<Node> nodesGraph = ruleGraphMap.get(rule).vertexSet();
			if (!considerAttributeNodes) {
				Set<Node> tempNodesGraph = removeAttributeNodes(nodesGraph,
						rule);
				nodesGraph = tempNodesGraph;
			}
			nodes.add(nodesGraph);
		}
		Set<NodeTupel> allNodeTupel = getNodeTupelSetOnlyEqualLabeledNodes(
				nodes, graphs);

		Set<NodeTupel> nodeTupelStartSet = new HashSet<NodeTupel>();
		for (NodeTupel nodeTupel : allNodeTupel) {
			if (LabelCreator.haveEqualCloneDetectiveLabels(
					nodeTupel.getNodeTupelAsNodeList(), graphs)) {
				nodeTupelStartSet.add(nodeTupel);
			} else {
				System.out
					.println("CloneDetectiveTupel - getStartNodeTupelSet - labels are not equal");
			}
		}

		return nodeTupelStartSet;
	}

	/**
	 * the CloneDetectiveTupel - an adapted Version of the CloneDetective
	 * algorithm from the paper, the line comments correspond to the lines of
	 * the pseudocode of the CloneDetectie algorithm from the paper
	 * 
	 */
	public Collection<CloneTupel> runCloneDetectiveTupel() {
		Set<NodeTupel> nodeTupelStartSet = getStartNodeTupelSet();
		List<DirectedGraph<Node, CapsuleEdge>> graphs 
				= new LinkedList<DirectedGraph<Node, CapsuleEdge>>();
		for (Rule rule : ruleList) {
			graphs.add(ruleGraphMap.get(rule));
		}

		List<NodeTupel> sortedNodeTupelSet = sortNodeTupelsPerWeight(
				nodeTupelStartSet, graphs);

		List<CloneTupel> cloneTupels = new LinkedList<CloneTupel>();

		// line 1
		Set<NodeTupel> doneD = new HashSet<NodeTupel>();
		// line 2
		for (NodeTupel nodeTupelUV : sortedNodeTupelSet) {
			// line 3
			if (!doneD.contains(nodeTupelUV)) {
				// line 4
				Queue<NodeTupel> queueQ = new LinkedBlockingQueue<NodeTupel>();
				queueQ.add(nodeTupelUV);
				Set<NodeTupel> currentC = new HashSet<NodeTupel>();
				currentC.add(nodeTupelUV);
				Set<Node> seenS = new HashSet<Node>();
				for (Node node : nodeTupelUV.getNodeTupelAsNodeList()) {
					seenS.add(node);
				}
				// line 5
				while (!queueQ.isEmpty()) {
					// line 6
					NodeTupel nodeTupelWZ = queueQ.poll();
					// line 7
					List<NodeTupel> adjacentP = getWeightedNeighbourhood(
							nodeTupelWZ, graphs, ruleGraphMap);
					for (NodeTupel nodeTupelXY : adjacentP) {
						// line 9 <-- optional
						// line 10
						if (!nodeTupelXY.nodesAreIdentical()) {
							if (!skipDueToSeen(seenS, nodeTupelXY)) {
								// line 11
								currentC.add(nodeTupelXY);
								for (Node node : nodeTupelXY
										.getNodeTupelAsNodeList()) {
									seenS.add(node);
								}
								// line 12
								queueQ.add(nodeTupelXY);
							}
						}
					}

				}
				Set<NodeTupel> cloneTupelSet = new HashSet<NodeTupel>();
				cloneTupelSet.addAll(currentC);
				CloneTupel cloneTupel = new CloneTupel(cloneTupelSet, graphs,
						considerAttributeNodes);
				// line 13
				if (!cloneTupel.edgeSetIsEmpty()) {
					cloneTupels.add(cloneTupel);
				}
				// line 14
				doneD.addAll(currentC);
			}
		}

		return cloneTupels;
	}

	private boolean skipDueToSeen(Set<Node> seenS, NodeTupel nodeTupel) {

		for (Node node : nodeTupel.getNodeTupelAsNodeList()) {
			if (seenS.contains(node)) {
				return true; // counter ++;
			}
		}

		return false;
	}

	private Set<NodeTupel> getNodeTupelSet(List<Set<Node>> nodes) {
		Set<List<Node>> tempRes = getCartesianProduct(nodes);
		Set<NodeTupel> res = new HashSet<NodeTupel>();
		for (List<Node> nodeTupel : tempRes) {
			res.add(new NodeTupel(nodeTupel));
		}
		return res;

	}

	private Set<List<Node>> getCartesianProduct(List<Set<Node>> nodes) {
		Set<List<Node>> res = new HashSet<List<Node>>();
		for (Node node : nodes.get(0)) {
			List<Node> nodeList = new LinkedList<Node>();
			nodeList.add(node);
			res.add(nodeList);
		}
		for (int i = 1; i < nodes.size(); i++) {
			res = getCartesianProduct2(res, nodes.get(i));
		}
		return res;
	}

	private Set<List<Node>> getCartesianProduct2(Set<List<Node>> nodesFirst,
			Set<Node> nodesEnd) {
		Set<List<Node>> res = new HashSet<List<Node>>();
		for (List<Node> nodes : nodesFirst) {
			for (Node endNode : nodesEnd) {
				// Clone nodes
				List<Node> nodesTemp = new LinkedList<Node>();
				for (Node node : nodes) {
					nodesTemp.add(node);
				}
				// add endNode
				nodesTemp.add(endNode);
				res.add(nodesTemp);
			}

		}
		return res;
	}

	private Set<NodeTupel> getNodeTupelSetOnlyEqualLabeledNodes(
			List<Set<Node>> nodes, List<DirectedGraph<Node, CapsuleEdge>> graphs) {
		Set<List<Node>> tempRes = getCartesianProductOnlyEqualLabeledNodes(
				nodes, graphs);
		Set<NodeTupel> res = new HashSet<NodeTupel>();
		for (List<Node> nodeTupel : tempRes) {
			res.add(new NodeTupel(nodeTupel));
		}
		return res;

	}

	private Set<List<Node>> getCartesianProductOnlyEqualLabeledNodes(
			List<Set<Node>> nodes, List<DirectedGraph<Node, CapsuleEdge>> graphs) {
		Set<List<Node>> res = new HashSet<List<Node>>();
		for (Node node : nodes.get(0)) {
			List<Node> nodeList = new LinkedList<Node>();
			nodeList.add(node);
			res.add(nodeList);
		}
		for (int i = 1; i < nodes.size(); i++) {
			res = getCartesianProduct2OnlyEqualLabeledNodes(res, nodes.get(i),
					graphs.get(0), graphs.get(i));
		}
		return res;
	}

	private Set<List<Node>> getCartesianProduct2OnlyEqualLabeledNodes(
			Set<List<Node>> nodesFirst, Set<Node> nodesEnd,
			DirectedGraph<Node, CapsuleEdge> graph0,
			DirectedGraph<Node, CapsuleEdge> graphNodesEnd) {
		Set<List<Node>> res = new HashSet<List<Node>>();
		for (List<Node> nodes : nodesFirst) {
			for (Node endNode : nodesEnd) {
				if (LabelCreator.haveEqualCloneDetectiveLabels(nodes.get(0),
						endNode, graph0, graphNodesEnd)) {
					List<Node> nodesTemp = new LinkedList<Node>();
					for (Node node : nodes) {
						nodesTemp.add(node);
					}
					nodesTemp.add(endNode);
					res.add(nodesTemp);
				}
			}

		}
		return res;
	}

	/**
	 * 
	 * Condition 1 from the CloneDetective paper
	 * 
	 * @param nodeTupels
	 * @param graphs
	 * @return
	 */
	private Set<NodeTupel> getNodeTupelsFulfilCondition1(
			Set<NodeTupel> nodeTupels,
			List<DirectedGraph<Node, CapsuleEdge>> graphs) {
		Set<NodeTupel> nodePairsFulFilCondition1 = new HashSet<NodeTupel>();
		for (NodeTupel nodeTupel : nodeTupels) {
			// condition (1)
			if (LabelCreator.haveEqualCloneDetectiveLabels(
					nodeTupel.getNodeTupelAsNodeList(), graphs)) {
				nodePairsFulFilCondition1.add(nodeTupel);
			}
		}
		return nodePairsFulFilCondition1;
	}

	private Set<NodeTupel> getNeighbourhoodUnderCondition1And2(
			NodeTupel nodeTupel, List<DirectedGraph<Node, CapsuleEdge>> graphs) {

		List<Set<Node>> adjacentNodes = new LinkedList<Set<Node>>();
		List<Node> nodes = nodeTupel.getNodeTupelAsNodeList();
		for (Node node : nodes) {
			adjacentNodes.add(NodeUtility.getAdjacentNodes(node,
					graphs.get(nodes.indexOf(node)), considerAttributeNodes));
		}
		Set<NodeTupel> adjacentNodeTupels = getNodeTupelSet(adjacentNodes);

		Set<NodeTupel> nodeTupelsFulfilCondition1;
		nodeTupelsFulfilCondition1 = getNodeTupelsFulfilCondition1(
				adjacentNodeTupels, graphs);
		if (nodeTupelsFulfilCondition1.size() == 0) {
			return nodeTupelsFulfilCondition1;
		}

		Set<NodeTupel> nodeTupelsFulfilCondition1And2;
		nodeTupelsFulfilCondition1And2 = getNodeTupelsFulfilCondition2(
				nodeTupelsFulfilCondition1, nodeTupel, graphs);
		return nodeTupelsFulfilCondition1And2;
	}

	/**
	 * 
	 * Condition 2 from the CopneDetective paper
	 * 
	 * the positions in the graphs lists are according to the position of the
	 * Nodes in the NodeTupel, means the graph at the k-th position of the
	 * graphs-List belongs to the Node at the k-th position in the NodeTupels
	 * 
	 * @param nodeTupelsFulfilCondition1
	 * @param nodeTupel
	 * @param graphs
	 * @return
	 */
	private Set<NodeTupel> getNodeTupelsFulfilCondition2(
			Set<NodeTupel> nodeTupelsFulfilCondition1, NodeTupel nodeTupel,
			List<DirectedGraph<Node, CapsuleEdge>> graphs) {
		Set<NodeTupel> nodePairsFulFilCondition2 = new HashSet<NodeTupel>();

		for (NodeTupel adjacentNodeTupel : nodeTupelsFulfilCondition1) {
			List<Node> nodes = adjacentNodeTupel.getNodeTupelAsNodeList();

			List<CapsuleEdge> capsuleEdgesOut = new LinkedList<CapsuleEdge>();
			for (Node node : nodes) {
				int index = nodes.indexOf(node);
				CapsuleEdge capsuleEdge = (graphs.get(index)).getEdge(node,
						nodeTupel.getNode(index));
				if (capsuleEdge != null) {
					capsuleEdgesOut.add(capsuleEdge);
				}
			}

			if (capsuleEdgesOut.size() != 0
					&& capsuleEdgesOut.size() == nodes.size()) {
				if (LabelCreator
						.haveEqualCloneDetectiveCapsuleEdgeLabels(capsuleEdgesOut)) {
					nodePairsFulFilCondition2.add(adjacentNodeTupel);
				}
			}

			List<CapsuleEdge> capsuleEdgesIn = new LinkedList<CapsuleEdge>();
			for (Node node : nodes) {
				int index = nodes.indexOf(node);
				CapsuleEdge capsuleEdge = (graphs.get(index)).getEdge(
						nodeTupel.getNode(index), node);
				if (capsuleEdge != null) {
					capsuleEdgesIn.add(capsuleEdge);
				}
			}

			if (capsuleEdgesIn.size() != 0
					&& capsuleEdgesIn.size() == nodes.size()) {
				if (LabelCreator
						.haveEqualCloneDetectiveCapsuleEdgeLabels(capsuleEdgesIn)) {
					nodePairsFulFilCondition2.add(adjacentNodeTupel);
				}
			}
		}

		return nodePairsFulFilCondition2;
	}

	/**
	 * 
	 * the positions in the graphs lists are according to the position of the
	 * Nodes in the NodeTupel, means the graph at the k-th position of the
	 * graphs-List belongs to the Node at the k-th position in the NodeTupels
	 * 
	 * 
	 * @param nodeQuartetWZMN
	 * @param graphs
	 * @param ruleGraphMap
	 * @return
	 */
	private List<NodeTupel> getWeightedNeighbourhood(NodeTupel nodeQuartetWZMN,
			List<DirectedGraph<Node, CapsuleEdge>> graphs,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<NodeTupel> nodeQuartetsFulfilCondition1And2;
		nodeQuartetsFulfilCondition1And2 = getNeighbourhoodUnderCondition1And2(
				nodeQuartetWZMN, graphs);
		if (nodeQuartetsFulfilCondition1And2.size() == 0) {
			return new LinkedList<NodeTupel>();
		}
		return sortNodeTupelsPerWeight(nodeQuartetsFulfilCondition1And2, graphs);

	}

	/**
	 * the positions in the graphs lists are according to the position of the
	 * Nodes in the NodeTupel, means the graph at the k-th position of the
	 * graphs-List belongs to the Node at the k-th position in the NodeTupels
	 * 
	 * @param nodeTupelsToSort
	 * @param graphs
	 * @return
	 */
	private List<NodeTupel> sortNodeTupelsPerWeight(
			Set<NodeTupel> nodeTupelsToSort,
			List<DirectedGraph<Node, CapsuleEdge>> graphs) {

		Map<Double, Set<NodeTupel>> weightedNodeTupels = new HashMap<Double, Set<NodeTupel>>();
		for (NodeTupel nodeTupel : nodeTupelsToSort) {
			double weight = getSimilarityFunctionValue(nodeTupel, graphs);
			if (weightedNodeTupels.containsKey(weight)) {
				weightedNodeTupels.get(weight).add(nodeTupel);
			} else {
				Set<NodeTupel> set = new HashSet<NodeTupel>();
				set.add(nodeTupel);
				weightedNodeTupels.put(weight, set);
			}

		}

		List<Double> weights = new LinkedList<Double>();
		weights.addAll(weightedNodeTupels.keySet());
		Collections.sort(weights);

		List<NodeTupel> res = new LinkedList<NodeTupel>();

		for (int i = weights.size() - 1; i >= 0; i--) {
			res.addAll(weightedNodeTupels.get(weights.get(i)));
		}

		return res;
	}

	private double getSimilarityFunctionValue(NodeTupel nodeTupel,
			List<DirectedGraph<Node, CapsuleEdge>> graphs) {
		double res = 0;
		for (int i = 0; i < NUMBER_OF_TERMS; i++) {
			double weighting = (1 / Math.pow(2, i));
			res = res + (weighting * functionSi(nodeTupel, graphs, i));
		}
		return res;
	}

	private double functionSi(NodeTupel nodeTupel,
			List<DirectedGraph<Node, CapsuleEdge>> graphs, int i) {

		if ((!LabelCreator.haveEqualCloneDetectiveLabels(
				nodeTupel.getNodeTupelAsNodeList(), graphs))) {
			return 0;
		} else {
			if (i <= 0) {
				return 1;
			} else {
				double numerator = 0;

				Set<NodeTupel> nuNv = getNeighbourhoodUnderCondition1And2(
						nodeTupel, graphs);

				List<Set<Node>> adjacentNodes = new LinkedList<Set<Node>>();
				for (int k = 0; k < nodeTupel.size(); k++) {
					Set<Node> adjacentNodesSet = new HashSet<Node>();
					adjacentNodes.add(adjacentNodesSet);
				}

				for (NodeTupel nT : nuNv) {
					List<Node> nodes = nT.getNodeTupelAsNodeList();
					for (Node node : nodes) {
						(adjacentNodes.get(nodes.indexOf(node))).add(node);
					}
				}

				Set<NodeTupel> newNodeTupelSet = getNodeTupelSet(adjacentNodes);
				for (NodeTupel newNodeTupel : newNodeTupelSet) {
					double temp = functionSi(newNodeTupel, graphs, (i - 1));
					if (temp > numerator) {
						numerator = temp;
					}
				}
				int denominator = adjacentNodes.get(0).size();
				for (Set<Node> adjacentNodesSets : adjacentNodes) {
					denominator = Math.max(denominator,
							adjacentNodesSets.size());
				}

				return (numerator / denominator);
			}
		}
	}

}
