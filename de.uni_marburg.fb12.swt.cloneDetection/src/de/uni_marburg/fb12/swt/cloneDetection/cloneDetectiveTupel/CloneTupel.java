package de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

public class CloneTupel {

	/**
	 * the NodeTupels of this CloneTupel
	 * 
	 */
	private Set<NodeTupel> nodeTupels;

	/**
	 * 
	 * the n-th lists contains the CapsuleEdges between the Nodes at the n-th
	 * position of the NodeTupels in nodeTupels the CapsuleEdge at the k-th
	 * position of each list is a clone of the CapsuleEdge at the k-th position
	 * of the other lists
	 */
	private List<List<CapsuleEdge>> capsuleEdges;

	public Set<Fragment> getAsFragments(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		if (capsuleEdges.get(0).size() == 0) {
			return null;
		}

		Set<Fragment> res = new HashSet<Fragment>();
		List<Fragment> tempRes = getAsFragmentList(ruleGraphMap);
		if (tempRes == null) {
			return null;
		}
		res.addAll(tempRes);
		return res;
	}

	public List<Fragment> getAsFragmentList(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		if (capsuleEdges.get(0).size() == 0) {
			return null;
		}
		List<Fragment> tempRes = new LinkedList<Fragment>();

		for (int i = 0; i < capsuleEdges.size(); i++) {
			Rule rule = (capsuleEdges.get(i)).get(0).getRule();
			Set<CapsuleEdge> capsuleEdgeSet = new HashSet<CapsuleEdge>();
			capsuleEdgeSet.addAll(capsuleEdges.get(i));
			DirectedGraph<Node, CapsuleEdge> graphNeu = ruleGraphMap.get(rule);
			Fragment f = new Fragment(capsuleEdgeSet, rule, graphNeu);
			tempRes.add(f);
		}

		String label1 = tempRes.get(0).getLabel();
		boolean labelsAreEqual = true;
		for (Fragment fragment : tempRes) {
			if (!label1.equals(fragment.getLabel())) {
				labelsAreEqual = false;
			}

		}

		if (labelsAreEqual) {
			return tempRes;
		} else {
			System.out
					.println("CloneTupel: getAsFragmentList: Conversion not successfull");
		}
		return null;
	}

	@Override
	public String toString() {
		String res = "NodePairs: + /n";
		for (NodeTupel nodeTupel : nodeTupels) {
			res = res + " " + nodeTupel.toString();
		}

		res = res + "CapsuleEdges + /n";

		for (CapsuleEdge capsuleEdge : capsuleEdges.get(0)) {
			res = res + " " + capsuleEdge.toString();
		}

		return res;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		CloneTupel cp = (CloneTupel) o;
		return nodeTupels.equals(cp.nodeTupels);
	}

	public Set<NodeTupel> getNodeTupels() {
		return nodeTupels;
	}

	public boolean edgeSetIsEmpty() {
		for (List<CapsuleEdge> capsuleEdgeList : capsuleEdges) {
			if (capsuleEdgeList.size() != 0) {
				return false;
			}
		}
		return true;
	}

	public CloneTupel(Set<NodeTupel> nodeTupels,
			List<DirectedGraph<Node, CapsuleEdge>> graphs,
			boolean considerAttributeNodes) {
		this.nodeTupels = nodeTupels;
		setCapsuleEdges(nodeTupels, graphs, considerAttributeNodes);
	}

	public List<List<CapsuleEdge>> getCapsuleEdges() {
		return capsuleEdges;
	}

	/**
	 * starts with 0!
	 * 
	 * @param index
	 * @return
	 */
	public List<CapsuleEdge> getCapsuleEdges(int index) {
		return capsuleEdges.get(index);
	}

	/**
	 * starts with 0!
	 * 
	 * @return
	 */
	public Set<Node> getNodes(int index) {
		Set<Node> res = new HashSet<Node>();
		for (NodeTupel nodeTupel : nodeTupels) {
			res.add(nodeTupel.getNode(index));
		}
		return res;
	}

	public int size() {
		return nodeTupels.iterator().next().size();
	}

	/**
	 * 
	 * Only exact clones are allowed, so wrong, means not cloned, edges are not
	 * set.
	 * 
	 * 
	 * @param filteredNodeTupels
	 * @param graphs
	 * @param considerAttributeNodes
	 */
	private void setCapsuleEdges(Set<NodeTupel> filteredNodeTupels,
			List<DirectedGraph<Node, CapsuleEdge>> graphs,
			boolean considerAttributeNodes) {
		capsuleEdges = new LinkedList<List<CapsuleEdge>>();
		for (int i = 0; i < size(); i++) {
			capsuleEdges.add(new LinkedList<CapsuleEdge>());
		}
		Map<Node, NodeTupel> nodeNodeTupelMap = new HashMap<Node, NodeTupel>();

		for (NodeTupel nodeTupel : filteredNodeTupels) {
			for (int i = 0; i < nodeTupel.size(); i++) {
				nodeNodeTupelMap.put(nodeTupel.getNode(i), nodeTupel);
			}
		}

		for (NodeTupel nodeTupel : filteredNodeTupels) {
			List<Node> nodeInsGraph = nodeTupel.getNodeTupelAsNodeList();

			Set<CapsuleEdge> inCapsuleEdgesNode1 = graphs.get(0)
					.incomingEdgesOf(nodeInsGraph.get(0));
			if (!considerAttributeNodes) {
				Set<CapsuleEdge> inCapsuleEdgesNode1Temp 
						= removeAttributeCapsuleEdges(inCapsuleEdgesNode1);
				inCapsuleEdgesNode1 = inCapsuleEdgesNode1Temp;
			}
			for (CapsuleEdge capsuleEdge : inCapsuleEdgesNode1) {
				if (!capsuleEdges.get(0).contains(capsuleEdge)) {
					Node node1out = graphs.get(0).getEdgeSource(capsuleEdge);
					if (nodeNodeTupelMap.containsKey(node1out)) {
						List<Node> nodeOutCandidatesGraph = new ArrayList<Node>();

						for (int i = 0; i < nodeTupel.size() - 1; i++) {
							nodeOutCandidatesGraph.add(nodeNodeTupelMap.get(
									node1out).getNode(i + 1));
						}

						List<CapsuleEdge> cloneCapsuleEdgeCandidates = new ArrayList<CapsuleEdge>();

						boolean notNull = true;
						for (int i = 0; i < nodeTupel.size() - 1; i++) {
							CapsuleEdge ce = (graphs.get(i + 1)).getEdge(
									nodeOutCandidatesGraph.get(i),
									nodeInsGraph.get(i + 1));
							if (ce != null) {
								cloneCapsuleEdgeCandidates.add(ce);
							} else {
								notNull = false;
							}

						}

						if (notNull) {
							boolean labelsAreEqual = true;

							for (CapsuleEdge ce : cloneCapsuleEdgeCandidates) {
								if (!(LabelCreator
										.haveEqualCloneDetectiveCapsuleEdgeLabels(
												capsuleEdge, ce))) {
									labelsAreEqual = false;
								}
							}

							if (labelsAreEqual) {
								capsuleEdges.get(0).add(capsuleEdge);
								for (int i = 1; i < nodeTupel.size(); i++) {
									capsuleEdges.get(i).add(
											cloneCapsuleEdgeCandidates
													.get(i - 1));
								}

							}

						}
					}
				}

			}
			// -------------------------------

			List<Node> nodeOutsGraph = nodeTupel.getNodeTupelAsNodeList();

			Set<CapsuleEdge> outCapsuleEdgesNode1 = graphs.get(0)
					.outgoingEdgesOf(nodeOutsGraph.get(0));

			if (!considerAttributeNodes) {
				Set<CapsuleEdge> outCapsuleEdgesNode1Temp 
							= removeAttributeCapsuleEdges(outCapsuleEdgesNode1);
				outCapsuleEdgesNode1 = outCapsuleEdgesNode1Temp;
			}

			for (CapsuleEdge capsuleEdge : outCapsuleEdgesNode1) {
				if (!capsuleEdges.get(0).contains(capsuleEdge)) {
					Node node1in = graphs.get(0).getEdgeTarget(capsuleEdge);
					if (nodeNodeTupelMap.containsKey(node1in)) {
						List<Node> nodeInCandidatesGraph = new ArrayList<Node>();
						for (int i = 0; i < nodeTupel.size() - 1; i++) {
							nodeInCandidatesGraph.add(nodeNodeTupelMap.get(
									node1in).getNode(i + 1));
						}

						List<CapsuleEdge> cloneCapsuleEdgeCandidates = new ArrayList<CapsuleEdge>();

						boolean notNull = true;
						for (int i = 0; i < nodeTupel.size() - 1; i++) {
							CapsuleEdge ce = (graphs.get(i + 1)).getEdge(
									nodeOutsGraph.get(i + 1),
									nodeInCandidatesGraph.get(i));
							if (ce != null) {
								cloneCapsuleEdgeCandidates.add(ce);
							} else {
								notNull = false;
							}

						}

						if (notNull) {
							boolean labelsAreEqual = true;

							for (CapsuleEdge ce : cloneCapsuleEdgeCandidates) {
								if (!(LabelCreator
										.haveEqualCloneDetectiveCapsuleEdgeLabels(
												capsuleEdge, ce))) {
									labelsAreEqual = false;
								}
							}

							if (labelsAreEqual) {
								capsuleEdges.get(0).add(capsuleEdge);
								for (int i = 1; i < nodeTupel.size(); i++) {
									capsuleEdges.get(i).add(
											cloneCapsuleEdgeCandidates
													.get(i - 1));
								}

							}
						}
					}
				}
			}

		}
	}

	private Set<CapsuleEdge> removeAttributeCapsuleEdges(
			Set<CapsuleEdge> capsuleEdges) {
		Set<CapsuleEdge> res = new HashSet<CapsuleEdge>();
		for (CapsuleEdge capsuleEdge : capsuleEdges) {
			if (!capsuleEdge.isAttributeEdge()) {
				res.add(capsuleEdge);
			}
		}
		return res;
	}

}
