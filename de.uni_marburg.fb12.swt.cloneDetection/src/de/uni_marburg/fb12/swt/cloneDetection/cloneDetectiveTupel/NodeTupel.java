package de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel;

import java.util.List;

import org.eclipse.emf.henshin.model.Node;

/**
 * 
 * A Tupel of Nodes
 *
 */
public class NodeTupel {
	private List<Node> nodes;

	public NodeTupel(List<Node> nodes) {
		this.nodes = nodes;
	}

	public boolean nodesAreIdentical() {
		for (Node node : nodes) {
			for (Node n : nodes) {
				if (node != n) {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * starts with 0!
	 * 
	 * @param i
	 * @return
	 */
	public Node getNode(int i) {
		return nodes.get(i);
	}

	public List<Node> getNodeTupelAsNodeList() {
		return nodes;
	}

	public int size() {
		return nodes.size();
	}

	@Override
	public String toString() {
		return nodes.toString();
	}

	@Override
	public int hashCode() {
		return nodes.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		NodeTupel nodeTupel = (NodeTupel) o;
		return nodes.equals(nodeTupel.nodes);
	}
}
