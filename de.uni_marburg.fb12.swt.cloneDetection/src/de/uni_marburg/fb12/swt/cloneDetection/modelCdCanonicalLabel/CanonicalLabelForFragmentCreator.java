package de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Node;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

public class CanonicalLabelForFragmentCreator {

	/**
	 * System.getProperty("line.separator");
	 */
	public static final String SEPARATOR = LabelCreator.getSeparator();

	private static Map<String, List<CapsuleEdge>> getCanonicalLabels(
			DirectedGraph<Node, CapsuleEdge> fragmentGraph) {
		Map<String, List<CapsuleEdge>> labelsToOrderedCapsuleEdges 
				= new HashMap<String, List<CapsuleEdge>>();

		List<List<Node>> nodeSequences = getNodeSequences(fragmentGraph);

		if (nodeSequences.isEmpty()) {
			System.out
					.println("CanonicalLabelForFragmenCreator -"
							+ " getCanonicalLabels: empty nodeSequence");
		}
		for (List<Node> nodeSequence : nodeSequences) {
			List<CapsuleEdge> orderedCapsuleEdges = getOrderedCapsuleEdges(
					fragmentGraph, nodeSequence);
			labelsToOrderedCapsuleEdges.put(
					getCanonicalLabel(fragmentGraph, nodeSequence,
							orderedCapsuleEdges), orderedCapsuleEdges);
		}

		return labelsToOrderedCapsuleEdges;
	}

	/**
	 * in case there are more than one possible label, the smallest one is
	 * chosen (occurs when a perfect refining of the nodeSequenzes is not found)
	 * 
	 * @param f
	 * @param ruleGraphMap
	 * @return
	 */

	public static Map<String, List<CapsuleEdge>> getCanonicalLabel(
			DirectedGraph<Node, CapsuleEdge> fragmentGraph) {
		Map<String, List<CapsuleEdge>> res = new HashMap<String, List<CapsuleEdge>>();
		Map<String, List<CapsuleEdge>> labelsToOrderedCapsuleEdges 
						= getCanonicalLabels(fragmentGraph);

		Set<String> labelsSet = labelsToOrderedCapsuleEdges.keySet();
		List<String> labels = new LinkedList<String>();
		labels.addAll(labelsSet);
		Collections.sort(labels);

		String label = labels.get(0);
		res.put(label, labelsToOrderedCapsuleEdges.get(label));
		return res;
	}

	private static String getCanonicalLabel(
			DirectedGraph<Node, CapsuleEdge> fragmentGraph,
			List<Node> orderedNodes, List<CapsuleEdge> orderedCapsuleEdges) {
		StringBuilder stringBuilder = new StringBuilder();
		for (CapsuleEdge capsuleEdge : orderedCapsuleEdges) {
			stringBuilder
					.append(LabelCreator.getModelCdEdgeLabel(capsuleEdge,
							fragmentGraph, orderedNodes.indexOf(fragmentGraph
									.getEdgeSource(capsuleEdge)), orderedNodes
									.indexOf(fragmentGraph
											.getEdgeTarget(capsuleEdge))));
		}
		return stringBuilder.toString();
	}

	private static List<CapsuleEdge> getOrderedCapsuleEdges(
			DirectedGraph<Node, CapsuleEdge> fragmentGraph,
			List<Node> orderedNodes) {
		List<CapsuleEdge> orderedCapsuleEdges = new LinkedList<CapsuleEdge>();
		for (Node node : orderedNodes) {
			Set<Node> nodesIn = new HashSet<Node>();
			Set<CapsuleEdge> ceIn = fragmentGraph.incomingEdgesOf(node);
			for (CapsuleEdge ce : ceIn) {
				Node source = fragmentGraph.getEdgeSource(ce);
				nodesIn.add(source);
			}

			// put nodesIn in order
			List<Integer> positions = new LinkedList<Integer>();
			for (Node nIn : nodesIn) {
				int position = orderedNodes.indexOf(nIn);
				if (position >= 0) {
					positions.add(orderedNodes.indexOf(nIn));
				}
			}
			Collections.sort(positions);

			for (Integer position : positions) {
				Node n = orderedNodes.get(position);
				orderedCapsuleEdges.add(fragmentGraph.getEdge(n, node));
			}

		}
		return orderedCapsuleEdges;
	}

	/**
	 * 
	 * @param fragmentGraph
	 * @return List of possible NodeSequences a NodeSequenzes is a List of Nodes
	 *         if a perfect Partitioning could be achieved the List of possible
	 *         NodeSequences contains only one NodeSequence
	 */

	private static List<List<Node>> getNodeSequences(
			DirectedGraph<Node, CapsuleEdge> fragmentGraph) {
		// create Partitioning based on NodeLabels
		Map<String, Set<Node>> nodeLabelsToNodes = new HashMap<String, Set<Node>>();
		boolean isPerfect = true;
		for (Node node : fragmentGraph.vertexSet()) {
			String label = LabelCreator
					.getModelCdNodeLabel(node, fragmentGraph);
			if (nodeLabelsToNodes.containsKey(label)) {
				nodeLabelsToNodes.get(label).add(node);
				isPerfect = false;
			} else {
				HashSet<Node> hashSet = new HashSet<Node>();
				hashSet.add(node);
				nodeLabelsToNodes.put(label, hashSet);
			}
		}
		if (isPerfect) {
			List<Node> nodeSequenz = getNodeSequenzesSimpel(nodeLabelsToNodes);
			List<List<Node>> res = new LinkedList<List<Node>>();
			res.add(nodeSequenz);
			return res;
		}

		// refine Partitioning
		Map<String, Set<Node>> extendedNodeLabelsToNodes = getRefineNodePartitioning(
				nodeLabelsToNodes, fragmentGraph);

		if (isPerfectPartitioning(extendedNodeLabelsToNodes, fragmentGraph)) {
			List<Node> nodeSequenz = getNodeSequenzesSimpel(extendedNodeLabelsToNodes);
			List<List<Node>> res = new LinkedList<List<Node>>();
			res.add(nodeSequenz);
			return res;
		}

		return getNodeSequenzes(extendedNodeLabelsToNodes);
	}

	private static Map<String, Set<Node>> getRefineNodePartitioning(
			Map<String, Set<Node>> nodeLabelsToNodes,
			DirectedGraph<Node, CapsuleEdge> fragmentGraph) {
		Map<String, Set<Node>> extendedNodeLabelsToNodes = new HashMap<String, Set<Node>>();

		for (String s : nodeLabelsToNodes.keySet()) {
			if (nodeLabelsToNodes.get(s).size() == 1) {
				extendedNodeLabelsToNodes.put(s, nodeLabelsToNodes.get(s));
			} else {
				extendedNodeLabelsToNodes.putAll(getRefinedNodeSet(s,
						nodeLabelsToNodes.get(s), fragmentGraph));
			}
		}
		return extendedNodeLabelsToNodes;
	}

	/**
	 * refine nodeset based on the incoming and outgoing edges
	 * 
	 * @param label
	 * @param nodes
	 * @return
	 */
	private static Map<String, Set<Node>> getRefinedNodeSet(String label,
			Set<Node> nodes, DirectedGraph<Node, CapsuleEdge> fragmentGraph) {
		Map<String, Set<Node>> tempRes = new HashMap<String, Set<Node>>();
		Map<Node, String> nodesToTempLabel = new HashMap<Node, String>();
		// incomingEdges
		boolean isPerfect = true;
		for (Node node : nodes) {
			String newLabel = label + SEPARATOR
					+ fragmentGraph.incomingEdgesOf(node).size();
			if (tempRes.containsKey(newLabel)) {
				tempRes.get(newLabel).add(node);
				nodesToTempLabel.put(node, newLabel);
				isPerfect = false;
			} else {
				Set<Node> set = new HashSet<Node>();
				set.add(node);
				tempRes.put(newLabel, set);
				nodesToTempLabel.put(node, newLabel);
			}
		}

		if (isPerfect) {
			return tempRes;
		}

		Map<String, Set<Node>> res = new HashMap<String, Set<Node>>();
		// outgoingEdges
		for (Node node : nodes) {
			String newLabel = nodesToTempLabel.get(node) + SEPARATOR
					+ fragmentGraph.outgoingEdgesOf(node).size();
			if (res.containsKey(newLabel)) {
				res.get(newLabel).add(node);
			} else {
				Set<Node> set = new HashSet<Node>();
				set.add(node);
				res.put(newLabel, set);
			}
		}
		return res;
	}

	private static boolean isPerfectPartitioning(
			Map<String, Set<Node>> nodeLabelsToNodes,
			DirectedGraph<Node, CapsuleEdge> fragmentGraph) {
		if (nodeLabelsToNodes.size() == fragmentGraph.vertexSet().size()) {
			return true;
		} else {
			return false;
		}

	}

	private static List<Node> getNodeSequenzesSimpel(
			Map<String, Set<Node>> nodeLabelsToNodes) {
		List<Node> res = new LinkedList<Node>();
		List<String> labels = new LinkedList<String>();
		labels.addAll(nodeLabelsToNodes.keySet());
		Collections.sort(labels);
		for (String label : labels) {
			if (nodeLabelsToNodes.get(label).size() != 1) {
				System.out
						.println("CanonicalLabelForFragmenCreator - getNodeSequenzesSimpel: ????");
			}
			res.add(nodeLabelsToNodes.get(label).iterator().next());
		}
		return res;
	}

	private static List<List<Node>> getNodeSequenzes(
			Map<String, Set<Node>> nodeLabelsToNodes) {
		int positionLastDuplicate = -1;

		List<String> labels = new LinkedList<String>();
		labels.addAll(nodeLabelsToNodes.keySet());
		Collections.sort(labels);
		int position = -1;
		for (String label : labels) {
			position++;
			if (nodeLabelsToNodes.get(label).size() != 1) {
				positionLastDuplicate = position;
			}
		}

		List<Node> endTail = new LinkedList<Node>();
		for (int i = positionLastDuplicate + 1; i < labels.size(); i++) {
			// beyond the last duplicate there is only one Node per set
			Node insertNode = nodeLabelsToNodes.get(labels.get(i)).iterator()
					.next();
			endTail.add(insertNode);
		}

		if (positionLastDuplicate == -1) {
			List<List<Node>> res = new LinkedList<List<Node>>();
			res.add(endTail);
			return res;
		}

		List<List<Node>> tails = getAllPermutations(nodeLabelsToNodes
				.get(labels.get(0)));

		List<String> remainingLabels = labels.subList(1,
				positionLastDuplicate + 1);

		for (String label : remainingLabels) {
			List<List<Node>> newTails = new LinkedList<List<Node>>();
			List<List<Node>> intermediateTails = getAllPermutations(nodeLabelsToNodes
					.get(label));
			for (List<Node> tail : tails) {
				for (List<Node> intermediateTail : intermediateTails) {
					List<Node> newTail = new LinkedList<Node>();
					newTail.addAll(tail);
					newTail.addAll(intermediateTail);
					newTails.add(newTail);
				}

			}
			tails = newTails;
		}

		for (List<Node> tail : tails) {
			tail.addAll(endTail);
		}

		return tails;
	}

	private static List<List<Node>> getAllPermutations(
			Set<Node> nodesOfSameLabel) {
		return Permutation.permute(nodesOfSameLabel);
	}

}
