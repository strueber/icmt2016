package de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestViaRule;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.List;

import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Node;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.variability.ResultObject;

public class CloneMatrixToActionNamedDirectedGraphConverter {

	/**
	 * Henshin isomorphism test returns false, if the names of the graphs are
	 * different
	 */
	private static final String NAME = "name";

	public static List<Graph> getCloneGraphs(CloneMatrix cloneMatrix) {
		List<Graph> graphs = new LinkedList<Graph>();
		for (int row = 0; row < cloneMatrix.getEdgeMatrix().size(); row++) {
			Graph graph = getGraph(cloneMatrix.getEdgeMatrix().get(row),
					cloneMatrix.getAttributeMatrix().get(row), NAME);
			graphs.add(graph);
		}
		return graphs;
	}

	public static List<DirectedGraph<Node, CapsuleEdge>> getDirectedGraphs(
			CloneMatrix cloneMatrix) {
		List<DirectedGraph<Node, CapsuleEdge>> graphs 
				= new LinkedList<DirectedGraph<Node, CapsuleEdge>>();
		for (int row = 0; row < cloneMatrix.getEdgeMatrix().size(); row++) {
			DirectedGraph<Node, CapsuleEdge> graph;
			if (cloneMatrix.getAttributeMatrix().size() != 0) {
				graph = getDirectedGraph(cloneMatrix.getEdgeMatrix().get(row),
						cloneMatrix.getAttributeMatrix().get(row));
			} else {
				graph = getDirectedGraph(cloneMatrix.getEdgeMatrix().get(row),
						new LinkedList<Attribute>());
			}
			graphs.add(graph);
		}
		return graphs;
	}

	private static DirectedGraph<Node, CapsuleEdge> getDirectedGraph(
			List<Edge> edgeMatrixRow, List<Attribute> attributeMatrixRow) {
		if (edgeMatrixRow == null && attributeMatrixRow == null) {
			return null;
		}

		ResultObject ro = convertWithoutAttributes(edgeMatrixRow);
		DirectedGraph<Node, CapsuleEdge> variabilityGraph = ro
				.getVariabilityGraph();

		if (attributeMatrixRow.size() > 0) {
			Map<Node, Node> orginalActionNodeFirstRuleInCommonPartToNewNode = ro
					.getOrginalActionNodeFirstRuleInCommonPartToNewNode();
			variabilityGraph = addAttributes(variabilityGraph,
					attributeMatrixRow,
					orginalActionNodeFirstRuleInCommonPartToNewNode);
		}
		return variabilityGraph;
	}

	private static DirectedGraph<Node, CapsuleEdge> addAttributes(
			DirectedGraph<Node, CapsuleEdge> graph, List<Attribute> attributes,
			Map<Node, Node> orginalActionNodeToNewNodes) {

		Set<Attribute> originalAttributesIncluded = new HashSet<Attribute>();
		for (Attribute attribute : attributes) {
			Node originalActionNode = attribute.getActionAttribute().getNode()
					.getActionNode();
			if (orginalActionNodeToNewNodes.keySet().contains(
					originalActionNode)) {
				Node node = orginalActionNodeToNewNodes.get(originalActionNode);
				for (Node n : graph.vertexSet()) {
					if (node == n) {
						Attribute a = HenshinFactory.eINSTANCE.createAttribute(
								n, attribute.getType(), attribute.getValue());
						n.getAttributes().add(a);
						originalAttributesIncluded.add(attribute);
					}
				}
			} else {
				System.out
						.println("CloneMatrixToDirectedGraphConverter - addAttributes - ??????");
			}
		}

		return graph;
	}

	/**
	 * Since the nodes still have their attributes and the attributes couldn�t
	 * be removed, new nodes must be created.
	 * 
	 * @param edges
	 * @return
	 */
	private static ResultObject convertWithoutAttributes(List<Edge> edges) {
		DirectedGraph<Node, CapsuleEdge> graph = new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);
		Map<Node, Node> orginalActionNodeAlreadyAddToNewNodes = new HashMap<Node, Node>();
		for (Edge edge : edges) {
			Node formerSourceNode = edge.getSource().getActionNode();
			Node newSourceNode;
			if (!orginalActionNodeAlreadyAddToNewNodes
					.containsKey(formerSourceNode)) {
				newSourceNode = HenshinFactory.eINSTANCE.createNode();
				newSourceNode.setType(formerSourceNode.getType());
				Action action = formerSourceNode.getAction();
				newSourceNode.setName(action.toString());
				orginalActionNodeAlreadyAddToNewNodes.put(formerSourceNode,
						newSourceNode);
				graph.addVertex(newSourceNode);
			} else {
				newSourceNode = orginalActionNodeAlreadyAddToNewNodes
						.get(formerSourceNode);
			}

			Node formerTargetNode = edge.getTarget().getActionNode();
			Node newTargetNode;
			if (!orginalActionNodeAlreadyAddToNewNodes
					.containsKey(formerTargetNode)) {
				newTargetNode = HenshinFactory.eINSTANCE.createNode();
				newTargetNode.setType(formerTargetNode.getType());
				Action action = formerTargetNode.getAction();
				newTargetNode.setName(action.toString());
				orginalActionNodeAlreadyAddToNewNodes.put(formerTargetNode,
						newTargetNode);
				graph.addVertex(newTargetNode);
			} else {
				newTargetNode = orginalActionNodeAlreadyAddToNewNodes
						.get(formerTargetNode);
			}

			CapsuleEdge capsuleEdge = new CapsuleEdge(edge);
			graph.addEdge(newSourceNode, newTargetNode, capsuleEdge);
		}

		ResultObject resultObject = new ResultObject(graph, null,
				orginalActionNodeAlreadyAddToNewNodes);
		return resultObject;
	}

	/**
	 * Since the nodes still have their attributes and the attributes couldn�t
	 * be removed, new nodes must be created.
	 * 
	 * @param edges
	 * @param attributes
	 * @param name
	 * @return
	 */
	private static Graph getGraph(List<Edge> edges, List<Attribute> attributes,
			String name) {

		Graph graph = HenshinFactory.eINSTANCE.createGraph(name);
		Map<Node, Node> formerToNewNode = new HashMap<Node, Node>();
		for (Edge formerEdge : edges) {
			Node formerSourceNode = formerEdge.getSource().getActionNode();
			Node formerTargetNode = formerEdge.getTarget().getActionNode();
			Node sourceNode;
			Node targetNode;

			if (!formerToNewNode.containsKey(formerSourceNode)) {
				sourceNode = HenshinFactory.eINSTANCE.createNode(graph,
						formerSourceNode.getType(), formerSourceNode.getName());
				sourceNode.setName(formerSourceNode.getName());
				graph.getNodes().add(sourceNode);
				formerToNewNode.put(formerSourceNode, sourceNode);

			} else {
				sourceNode = formerToNewNode.get(formerSourceNode);
			}

			if (!formerToNewNode.containsKey(formerTargetNode)) {
				targetNode = HenshinFactory.eINSTANCE.createNode(graph,
						formerTargetNode.getType(), formerTargetNode.getName());
				targetNode.setName(formerTargetNode.getName());
				graph.getNodes().add(targetNode);
				formerToNewNode.put(formerTargetNode, targetNode);
			} else {
				targetNode = formerToNewNode.get(formerTargetNode);
			}

			Edge edge = HenshinFactory.eINSTANCE.createEdge(sourceNode,
					targetNode, formerEdge.getType());
			graph.getEdges().add(edge);

		}

		for (Attribute formerAttribute : attributes) {
			Node formerAttributeNode = formerAttribute.getNode()
					.getActionNode();
			Node attributeNode;

			if (!formerToNewNode.containsKey(formerAttributeNode)) {
				attributeNode = HenshinFactory.eINSTANCE.createNode(graph,
						formerAttributeNode.getType(),
						formerAttributeNode.getName());
				attributeNode.setName(formerAttributeNode.getName());
				graph.getNodes().add(attributeNode);
				formerToNewNode.put(formerAttributeNode, attributeNode);
			} else {
				attributeNode = formerToNewNode.get(formerAttributeNode);
			}

			Attribute attribute = HenshinFactory.eINSTANCE.createAttribute(
					attributeNode, formerAttribute.getType(),
					formerAttribute.getValue());
			attributeNode.getAttributes().add(attribute);
		}

		return graph;
	}

}
