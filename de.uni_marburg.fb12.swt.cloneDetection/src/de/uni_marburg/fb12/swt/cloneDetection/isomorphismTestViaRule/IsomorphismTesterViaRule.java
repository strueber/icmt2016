package de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestViaRule;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/*
 import org.eclipse.emf.common.util.URI;
 import org.eclipse.emf.ecore.resource.impl.PlatformResourceURIHandlerImpl;
 import org.eclipse.emf.ecore.resource.impl.URIHandlerImpl;
 import org.eclipse.emf.henshin.interpreter.EGraph;
 import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
 import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
 import org.eclipse.emf.henshin.model.Graph;
 import org.eclipse.emf.henshin.model.Module;
 import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
 import org.eclipse.emf.henshin.model.impl.ModuleImpl;
 */
import org.jgrapht.DirectedGraph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.ExtendedIntegratedGraphConverter;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;
import de.uni_marburg.fb12.swt.cloneDetection.variability.ActionNamedDirectedGraphToHenshinRule;

public class IsomorphismTesterViaRule {

	public static boolean testResult(Set<CloneMatrix> result) {
		boolean res = true;
		for (CloneMatrix cloneMatrix : result) {
			if (!testCloneMatrix(cloneMatrix)) {
				res = false;
			}
		}

		return res;
	}

	private static boolean areIsomorph(Rule rule1, Rule rule2) {
		ExtendedIntegratedGraphConverter graphConverter = new ExtendedIntegratedGraphConverter();
		Set<Rule> ruleSet = new HashSet<Rule>();
		ruleSet.add(rule1);
		ruleSet.add(rule2);
		Map<Rule, DirectedGraph<Node, CapsuleEdge>> map = graphConverter
				.getRuleToGraphWithAttributesMap(ruleSet);

		DirectedGraph<Node, CapsuleEdge> graph1 = map.get(rule1);
		DirectedGraph<Node, CapsuleEdge> graph2 = map.get(rule2);

		Fragment fragment1 = new Fragment(graph1.edgeSet(), rule1, graph1);
		Fragment fragment2 = new Fragment(graph2.edgeSet(), rule2, graph2);

		String canonicalLabel1 = fragment1.getLabel();
		String canonicalLabel2 = fragment2.getLabel();
		return (canonicalLabel1.equals(canonicalLabel2));
	}

	/*
	 * private static boolean areIsomorphByHenshinIsomorphismTest(Rule rule1,
	 * Rule rule2) { boolean allAreIsomorph = true;
	 * 
	 * Graph lhs1 = rule1.getLhs(); EGraph eLhs1 = new EGraphImpl(lhs1);
	 * 
	 * //EGraph eLhs1 = new EGraphImpl(); //eLhs1.addTree(lhs1);
	 * 
	 * Graph rhs1 = rule1.getRhs(); EGraph eRhs1 = new EGraphImpl(rhs1);
	 * 
	 * // EGraph eRhs1 = new EGraphImpl(); // eLhs1.addTree(rhs1);
	 * 
	 * 
	 * Graph lhs2 = rule2.getLhs(); EGraph eLhs2 = new EGraphImpl(lhs2);
	 * 
	 * // EGraph eLhs2 = new EGraphImpl(); // eLhs2.addTree(lhs2);
	 * 
	 * 
	 * Graph rhs2 = rule2.getRhs(); EGraph eRhs2 = new EGraphImpl(rhs2);
	 * 
	 * // EGraph eRhs2 = new EGraphImpl(); // eLhs2.addTree(rhs2);
	 * 
	 * 
	 * 
	 * //lhs-Graph looks identical in the "baumbasierte" Editor, so there are
	 * either some hidden information //or the henshin-isomorphism-test is not
	 * working correctly
	 * 
	 * //if (eLhs1 != eLhs2) { // if (!InterpreterUtil.areIsomorphic(eLhs1,
	 * eLhs2)) { // allAreIsomorph = false; // } //}
	 * 
	 * 
	 * if (eRhs1 != eRhs2) { if (!InterpreterUtil.areIsomorphic(eRhs1, eRhs2)) {
	 * allAreIsomorph = false; } }
	 * 
	 * return allAreIsomorph; }
	 */

	public static boolean testCloneMatrix(CloneMatrix cloneMatrix, Rule rule1) {
		boolean allAreIsomorph = true;
		long start = System.currentTimeMillis();
		Date dateStart = new Date();
		System.out.println(getStartString("isomorphism-test", dateStart));

		List<DirectedGraph<Node, CapsuleEdge>> directedGraphs 
				= CloneMatrixToActionNamedDirectedGraphConverter
				.getDirectedGraphs(cloneMatrix);
		// int i= 0;
		for (DirectedGraph<Node, CapsuleEdge> directedGraph : directedGraphs) {
			boolean setVariabilities = false;
			Rule rule2 = ActionNamedDirectedGraphToHenshinRule.getRuleGraph(
					directedGraph, "name", setVariabilities);

			/*
			 * i++; String fileName = "ruleTest" + i + ".henshin"; String path =
			 * "files/cloneDetectionTests/4er/"; writeRuleToFile(rule2, path,
			 * fileName);
			 */

			if (!areIsomorph(rule1, rule2)) {
				allAreIsomorph = false;
			}

		}

		System.out.println(getEndString(allAreIsomorph, start));
		return allAreIsomorph;
	}

	/**
	 * write the rule / the henshin graph of the rule to a file, so it could be
	 * surveyed
	 * 
	 * @param rule
	 * @param path
	 *            the relative Path without filename
	 * @param fileName
	 */
	/*
	 * private static void writeRuleToFile(Rule rule, String path, String
	 * fileName){
	 * 
	 * URIHandlerImpl uhi = new PlatformResourceURIHandlerImpl(); URI eUri = URI
	 * .createURI((path + fileName));
	 * 
	 * //if (!uhi.exists(eUri, null)) { Module module = new ModuleImpl();
	 * //Module module = HenshinFactory.eINSTANCE.createModule();
	 * module.getUnits().add(rule);
	 * 
	 * HenshinResourceSet rs = new HenshinResourceSet("");
	 * rs.saveEObject(module, eUri); }
	 * 
	 * }
	 */

	private static boolean testCloneMatrix(CloneMatrix cloneMatrix) {
		boolean allAreIsomorph = true;
		long start = System.currentTimeMillis();
		Date dateStart = new Date();
		System.out.println(getStartString("isomorphism-test", dateStart));

		List<DirectedGraph<Node, CapsuleEdge>> directedGraphs 
				= CloneMatrixToActionNamedDirectedGraphConverter
				.getDirectedGraphs(cloneMatrix);
		boolean setVariabilities = false;
		Rule rule1 = ActionNamedDirectedGraphToHenshinRule.getRuleGraph(
				directedGraphs.iterator().next(), "name", setVariabilities);

		for (DirectedGraph<Node, CapsuleEdge> directedGraph : directedGraphs) {
			Rule rule2 = ActionNamedDirectedGraphToHenshinRule.getRuleGraph(
					directedGraph, "name", setVariabilities);

			if (!areIsomorph(rule1, rule2)) {
				allAreIsomorph = false;
			}

		}

		System.out.println(getEndString(allAreIsomorph, start));
		return allAreIsomorph;
	}

	private static String getStartString(String s, Date dateStart) {
		return ("start " + s + ": " + dateStart.toString() + "\n");
	}

	private static String getEndString(boolean allAreIsomorph, long start) {
		Date dateDone = new Date();
		long end = System.currentTimeMillis();
		String testRes = "";

		if (allAreIsomorph) {
			testRes = testRes + "all Clones are isomorph" + "\n";
		} else {
			testRes = testRes + "fail \n";
		}
		long sec = (end - start) / 1000;
		testRes = testRes + "IsomorphismTest - done, in " + sec + " sec "
				+ dateDone.toString() + "\n";
		return testRes;
	}

}
