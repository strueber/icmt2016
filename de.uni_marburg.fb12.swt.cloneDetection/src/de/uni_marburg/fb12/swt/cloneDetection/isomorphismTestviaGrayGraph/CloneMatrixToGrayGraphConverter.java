package de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Node;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;

public class CloneMatrixToGrayGraphConverter {

	/**
	 * Henshin isomorphism test returns false, if the names of the graphs are
	 * different
	 */
	private static final String NAME = "name";

	public static Set<Graph> getCloneGraphs(CloneMatrix cloneMatrix) {
		Set<Graph> graphs = new HashSet<Graph>();
		for (int i = 0; i < cloneMatrix.getRuleList().size(); i++) {
			Graph graph;
			if (cloneMatrix.getAttributeMatrix().size() != 0) {
				graph = getCloneGraph(cloneMatrix.getEdgeMatrix().get(i),
						cloneMatrix.getAttributeMatrix().get(i));
			} else {
				graph = getCloneGraph(cloneMatrix.getEdgeMatrix().get(i),
						new LinkedList<Attribute>());
			}
			graphs.add(graph);
		}
		return graphs;
	}

	private static Graph getCloneGraph(List<Edge> edges,
			List<Attribute> attributes) {

		Set<Edge> edgeSet = new HashSet<Edge>();
		edgeSet.addAll(edges);
		Set<Attribute> attributeSet = new HashSet<Attribute>();
		attributeSet.addAll(attributes);
		// Graph graph = getGraph(edges, attributes, rule.getName());
		Graph graph = getGraph(edgeSet, attributeSet, NAME);
		return graph;
	}

	public static Graph getGraph(Set<Edge> edges, Set<Attribute> attributes,
			String name) {
		Graph graph = HenshinFactory.eINSTANCE.createGraph(name);
		Map<Node, Node> formerToNewNode = new HashMap<Node, Node>();
		for (Edge formerEdge : edges) {
			Node formerSourceNode = formerEdge.getSource().getActionNode();
			Node formerTargetNode = formerEdge.getTarget().getActionNode();
			Node sourceNode;
			Node targetNode;

			if (!formerToNewNode.containsKey(formerSourceNode)) {
				sourceNode = HenshinFactory.eINSTANCE.createNode(graph,
						formerSourceNode.getType(), formerSourceNode.getName());
				sourceNode.setName(formerSourceNode.getName());
				graph.getNodes().add(sourceNode);
				formerToNewNode.put(formerSourceNode, sourceNode);

			} else {
				sourceNode = formerToNewNode.get(formerSourceNode);
			}

			if (!formerToNewNode.containsKey(formerTargetNode)) {
				targetNode = HenshinFactory.eINSTANCE.createNode(graph,
						formerTargetNode.getType(), formerTargetNode.getName());
				targetNode.setName(formerTargetNode.getName());
				graph.getNodes().add(targetNode);
				formerToNewNode.put(formerTargetNode, targetNode);
			} else {
				targetNode = formerToNewNode.get(formerTargetNode);
			}

			Edge edge = HenshinFactory.eINSTANCE.createEdge(sourceNode,
					targetNode, formerEdge.getType());
			graph.getEdges().add(edge);

		}

		for (Attribute formerAttribute : attributes) {
			Node formerAttributeNode = formerAttribute.getNode()
					.getActionNode();
			Node attributeNode;

			if (!formerToNewNode.containsKey(formerAttributeNode)) {
				attributeNode = HenshinFactory.eINSTANCE.createNode(graph,
						formerAttributeNode.getType(),
						formerAttributeNode.getName());
				attributeNode.setName(formerAttributeNode.getName());
				graph.getNodes().add(attributeNode);
				formerToNewNode.put(formerAttributeNode, attributeNode);
			} else {
				attributeNode = formerToNewNode.get(formerAttributeNode);
			}

			Attribute attribute = HenshinFactory.eINSTANCE.createAttribute(
					attributeNode, formerAttribute.getType(),
					formerAttribute.getValue());
			attributeNode.getAttributes().add(attribute);
		}

		return graph;
	}

}
