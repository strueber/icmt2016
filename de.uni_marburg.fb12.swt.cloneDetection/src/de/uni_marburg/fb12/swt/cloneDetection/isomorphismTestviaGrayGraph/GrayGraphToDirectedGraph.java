package de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph;

import java.util.HashSet;
import java.util.Iterator;

import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;

import org.eclipse.emf.henshin.model.Node;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

public class GrayGraphToDirectedGraph {

	public static DirectedGraph<Node, CapsuleEdge> getIntegratedGraph(
			Graph grayGraph) {

		DirectedGraph<Node, CapsuleEdge> graphRes = new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);

		Iterator<Node> nodeIterator = grayGraph.getNodes().iterator();
		while (nodeIterator.hasNext()) {
			Node tempNode = (nodeIterator.next());
			graphRes.addVertex(tempNode);

		}

		for (Edge edge : grayGraph.getEdges()) {
			CapsuleEdge capsuleEdge = new CapsuleEdge(edge);
			graphRes.addEdge(edge.getSource(), edge.getTarget(), capsuleEdge);
		}

		Set<Node> nodes = new HashSet<Node>();
		for (Node n : graphRes.vertexSet()) {
			if (n.getAttributes().size() != 0) {
				nodes.add(n);
			}
		}
		for (Node n : nodes) {
			for (Attribute a : n.getAttributes()) {
				Node node = HenshinFactory.eINSTANCE.createNode();
				node.setName("A: " + a.getValue() + " Node: "
						+ a.getNode().toString());

				graphRes.addVertex(node);

				Edge originalEdge = HenshinFactory.eINSTANCE.createEdge();

				CapsuleEdge capsuleEdge = new CapsuleEdge(originalEdge, a);
				graphRes.addEdge(n, node, capsuleEdge);

			}

		}
		return graphRes;

	}

}
