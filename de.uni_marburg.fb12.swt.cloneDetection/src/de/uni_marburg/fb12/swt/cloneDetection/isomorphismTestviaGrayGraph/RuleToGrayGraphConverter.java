package de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.ExtendedIntegratedGraphConverter;

public class RuleToGrayGraphConverter {

	/**
	 * Henshin isomorphism test returns false, if the names of the graphs are
	 * different
	 */
	private static final String NAME = "name";

	public static Graph getIntegeratedHenshinGraph(Rule rule) {
		Set<Rule> set = new HashSet<Rule>();
		set.add(rule);
		ExtendedIntegratedGraphConverter eigc = new ExtendedIntegratedGraphConverter();
		DirectedGraph<Node, CapsuleEdge> computationGraph = (eigc
				.getRuleToGraphWithAttributesMap(set)).get(rule);
		Set<CapsuleEdge> capsuleEdges = computationGraph.edgeSet();

		Set<Edge> edges = new HashSet<Edge>();
		Set<Attribute> attributes = new HashSet<Attribute>();
		for (CapsuleEdge capsuleEdge : capsuleEdges) {
			if (capsuleEdge.isAttributeEdge()) {
				attributes.add(capsuleEdge.getAttribute());
			} else {
				edges.add(capsuleEdge.getOriginalEdge());
			}

		}
		Graph graph = CloneMatrixToGrayGraphConverter.getGraph(edges,
				attributes, NAME);
		return graph;
	}

}
