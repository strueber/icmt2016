package de.uni_marburg.fb12.swt.cloneDetection.combineDetectionWeb;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.CloneDetective;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.ClonePair;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CombineDetection;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.ConvertToFragments;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CreateNewLatticeUtility;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTesterViaGg;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetection3D;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleInc;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

/**
 * 
 * Contains the CombineAlgorithm consisting of CloneDetection and
 * ModelCd-eScanInc
 *
 */
public class CombineDetectionCDWebwithEScanInc extends CombineDetection {
	private boolean considerAttributeNodesInStep1;

	public CombineDetectionCDWebwithEScanInc(Collection<Rule> rules) {
		super(rules);
		considerAttributeNodesInStep1 = true;
	}

	public CombineDetectionCDWebwithEScanInc(Collection<Rule> rules,
			boolean considerAttributeNodesInStep1) {
		super(rules);
		this.considerAttributeNodesInStep1 = considerAttributeNodesInStep1;
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		System.out
				.println(startDetectCloneGroups("CombineDetectionCDwithEScanInc"));
//		CloneDetective cloneDetective = new CloneDetective(ruleGraphMap,
//				ruleList, considerAttributeNodesInStep1);
//		Collection<ClonePair> resCloneDetective = cloneDetective
//				.runCloneDetective();

		ConqatBasedCloneGroupDetector cq = new ConqatBasedCloneGroupDetector(
				(List<Rule>) rules, true);
		cq.detectCloneGroups();
		
		System.out.println(startConversionTempResult("CloneDetection"));
		
		Set<Set<Fragment>> cloneGroups = ConvertToFragments.convertCloneGroupsToFragments(cq.getResultOrderedByNumberOfCommonElements(), ruleGraphMap);
		
		resultAsCloneMatrix.clear();
		System.out.println(startConversion());
		Set<Set<Rule>> combinations = getCombinations(cloneGroups);
		
		for (Set<Rule> combination : combinations) {

			Set<Fragment> simpleTopLayer = getMaxSizedFragmentSetThatContainsOneFragmentOfEachRule(
					cloneGroups, combination);
			
			if (simpleTopLayer != null) {
				List<Set<Fragment>> lattice = CreateNewLatticeUtility.createLattice(simpleTopLayer);

				System.out.println(moveOnToSecondAlgortihm("ModelCd-eScanInc"));
				boolean ignoreIsGeneratingParent = true;
				EScanDetectionArticleInc eScanDetection = new EScanDetectionArticleInc(
						ruleGraphMap, ruleList, considerAttributeNodes,
						ignoreIsGeneratingParent);
				int fragmentSize = lattice.get(lattice.size() - 1).iterator()
						.next().size();
				eScanDetection.detectCloneGroups(fragmentSize, lattice);

				
				Set<CloneMatrix> eScanIncResult = eScanDetection.getResultAsCloneMatrix();
				for (CloneMatrix m : eScanIncResult) {
					if (IsomorphismTesterViaGg.testCloneMatrix(m)) {
						resultAsCloneMatrix.add(m);
					}
				}

			}
			// else {
			// Set<Fragment> topLayer = createTopLayer(resConvert);
			// lattice = CreateNewLatticeUtility.createLattice(topLayer);
			// }
		}

		System.out.println(endDetectCloneGroups(
				"CombineDetectionCDwithEScanInc", startZeit));
	}

	private Set<Set<Rule>> getCombinations(Set<Set<Fragment>> cloneGroups) {
		Set<Set<Rule>> result = new HashSet<Set<Rule>>();
		for (Set<Fragment> fragments : cloneGroups) {
			Set<Rule> rules = new HashSet<Rule>();
			for (Fragment f: fragments) {
				rules.add(f.getRule());
			}
			
			if (!result.contains(rules))
				result.add(rules);
		}
		return result;
	}

	private Set<Fragment> createTopLayer(Map<Integer, Set<Fragment>> resConvert) {

		Set<Fragment> maxFragments = CreateNewLatticeUtility
				.getMaxFragments(resConvert);
		List<Set<CapsuleEdge>> graphsFragmentsSetList = CreateNewLatticeUtility
				.getFragmentsEdgeSetListWithAttributes(maxFragments, ruleList);

		EScanDetection3D eScan3d = new EScanDetection3D(ruleGraphMap, ruleList,
				considerAttributeNodesInStep1);
		Set<Set<Fragment>> cloneGroups = eScan3d
				.getCloneGroups(graphsFragmentsSetList);
		if (cloneGroups == null) {
			return null;
		}
		// find maxSized CloneGroup
		Set<Fragment> res = cloneGroups.iterator().next();
		for (Set<Fragment> cloneGroup : cloneGroups) {
			int size = cloneGroup.iterator().next().size();
			if (size > res.iterator().next().size()) {
				res = cloneGroup;
			}
		}
		return res;
	}

	private Set<Fragment> getMaxSizedFragmentSetThatContainsOneFragmentOfEachRule(
			Set<Set<Fragment>> cloneGroups, Set<Rule> set) {
		Set<Set<Fragment>> candidates = new HashSet<Set<Fragment>>();
		for (Set<Fragment> cloneGroup : cloneGroups) {
			Set<Rule> rules = new HashSet<Rule>();
			for (Fragment f : cloneGroup) {
				rules.add(f.getRule());
			}
			if (rules.equals(set))
				candidates.add(cloneGroup);
			
// old impl.
//			if (cloneGroup.size() == set.size()) {
//				// one Fragment per Rule?
//				boolean doubleRule = false;
//				Set<Rule> rules = new HashSet<Rule>();
//				for (Fragment fragment : cloneGroup) {
//					if (!rules.contains(fragment.getRule())) {
//						rules.add(fragment.getRule());
//					} else {
//						doubleRule = true;
//					}
//				}
//				if (!doubleRule) {
//					candidates.add(cloneGroup);
//				}
//			}
		}
		if (candidates.size() == 0) {
			return null;
		}
		// find maximal one
		int max = 0;
		Set<Fragment> res = candidates.iterator().next();
		for (Set<Fragment> candidate : candidates) {
			if (max < candidate.iterator().next().size()) {
				max = candidate.iterator().next().size();
				res = candidate;
			}
		}
		return res;
	}

}
