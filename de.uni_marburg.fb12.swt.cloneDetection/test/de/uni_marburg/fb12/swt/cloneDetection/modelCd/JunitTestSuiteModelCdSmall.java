package de.uni_marburg.fb12.swt.cloneDetection.modelCd;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	TestSmall.class, 
	TestDelete.class, 
	TestRequire.class,
	TestInternalPart1.class
	})
public class JunitTestSuiteModelCdSmall {
}
