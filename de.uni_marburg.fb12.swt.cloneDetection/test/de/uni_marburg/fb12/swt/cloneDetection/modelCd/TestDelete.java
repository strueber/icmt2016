package de.uni_marburg.fb12.swt.cloneDetection.modelCd;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
//import org.junit.Before;
//import org.junit.After;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetection;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetection3D;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetection3DOpt;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleInc;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleIncOpt1;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleIncOpt2;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleOriginal;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.CloneDetectionTestUtility;
//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestViaRule.IsomorphismTesterViaRule;
//import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTester;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTesterViaGg;

public class TestDelete {
	private static final boolean USE_MODULE_GET_RULES = true;

	private static final String TEST_PATH 
			= "files/test/cloneDetectionTest/delete/OCL2NGC.henshin";
	private static final String TEST_EXPECTED_PATH 
			= "files/test/cloneDetectionTest/deleteClone/OCL2NGC.henshin";

	private static Set<Rule> testRuleSet;
	private static Rule comparisonRule;

	@BeforeClass
	public static void setUp() {
		List<Rule> rules = RuleProvider.getRules(TEST_PATH,
				USE_MODULE_GET_RULES);
		testRuleSet = new HashSet<Rule>();
		testRuleSet.addAll(rules);
		List<Rule> rules2 = RuleProvider.getRules(TEST_EXPECTED_PATH,
				USE_MODULE_GET_RULES);
		comparisonRule = rules2.iterator().next();
	}

	@Test
	public void testEScanDetectionArticleOriginal() {
		EScanDetection eScanDetection = new EScanDetectionArticleOriginal(
				testRuleSet);
		eScanDetection.detectCloneGroups();
		Set<CloneMatrix> result = eScanDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testEScanDetectionArticleInc() {
		EScanDetection eScanDetection = new EScanDetectionArticleInc(
				testRuleSet);
		eScanDetection.detectCloneGroups();
		Set<CloneMatrix> result = eScanDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testEScanDetectionArticleIncOpt1() {
		EScanDetection eScanDetection = new EScanDetectionArticleIncOpt1(
				testRuleSet);
		eScanDetection.detectCloneGroups();
		Set<CloneMatrix> result = eScanDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testEScanDetectionArticleIncOpt2() {
		EScanDetection eScanDetection = new EScanDetectionArticleIncOpt2(
				testRuleSet);
		eScanDetection.detectCloneGroups();
		Set<CloneMatrix> result = eScanDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testEScanDetection3d() {
		EScanDetection eScanDetection = new EScanDetection3D(testRuleSet);
		eScanDetection.detectCloneGroups();
		Set<CloneMatrix> result = eScanDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);

		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testEScanDetection3DOpt() {
		EScanDetection eScanDetection = new EScanDetection3DOpt(testRuleSet);
		eScanDetection.detectCloneGroups();
		Set<CloneMatrix> result = eScanDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

}
