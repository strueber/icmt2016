package de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JApplet;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgraph.JGraph;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.JGraphModelAdapter;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.ExtendedIntegratedGraphConverter;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;

// based on:
// http://jgrapht.org/visualizations.html
// A demo applet that shows how to use JGraph to visualize JGraphT graphs.

/**
 * 
 * This Applet shows the integrated graph representation of the rule, that are
 * selected in the dropDownList
 * 
 * The action of the Nodes are not be shown, but still exist (thats due to the
 * fact, thatJGraph use as the name of the Nodes the toString() method an in
 * Henshin the actions are not part of the node�s toString() method)
 * 
 * Although all Nodes are drawn in the same place, so at first view it looks
 * like there would only be one Node, but the Nodes could just be drag to some
 * other coordinates
 * 
 * 
 */

@SuppressWarnings("serial")
public class IntegratedGraphViewer extends JApplet implements ActionListener {

	private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
	private static final Dimension DEFAULT_SIZE = new Dimension(530, 320);
	private JGraphModelAdapter m_jgAdapter;
	private Choice ruleChoice;
	private List<Rule> ruleList;
	private Rule rule;

	/**
	 * @see java.applet.Applet#init().
	 */
	public void init() {
		final boolean USE_MODULE_GET_RULES = true;

		String path = "OCL2NGC.henshin";
		Collection<Rule> rules = RuleProvider.getRules(path,
				USE_MODULE_GET_RULES);
		ruleList = new LinkedList<Rule>();
		ruleList.addAll(rules);

		ruleChoice = new Choice();
		for (Rule rule : ruleList) {
			ruleChoice.add(rule.getName());
		}
		add(ruleChoice);
	}

	public boolean action(Event event, Object object) {
		if (event.target.equals(ruleChoice)) {
			int ruleInt = ruleChoice.getSelectedIndex();
			// System.out.println (ruleInt);
			rule = ruleList.get(ruleInt);
			drawGraph();
			return true;
		} else {
			return super.action(event, object);
		}
	}

	public void actionPerformed(ActionEvent event) {
	}

	public void drawGraph() {
		Collection<Rule> testRuleSet = new HashSet<Rule>();
		testRuleSet.add(rule);

		ExtendedIntegratedGraphConverter graphConverter = new ExtendedIntegratedGraphConverter();
		Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap = graphConverter
				.getRuleToGraphWithAttributesMap(testRuleSet);

		// JGraphT graph
		DirectedGraph<Node, CapsuleEdge> graph;
		graph = ruleGraphMap.values().iterator().next();

		// JGraph visualization via an adapter
		m_jgAdapter = new JGraphModelAdapter(graph);
		JGraph jgraph = new JGraph(m_jgAdapter);
		adjustDisplaySettings(jgraph);
		getContentPane().add(jgraph);
		resize(DEFAULT_SIZE);

		// change the title to the name of the rule
		// to remember, which one was selected
		setTitle(ruleGraphMap.keySet().iterator().next().getName());
	}

	private void adjustDisplaySettings(JGraph jgraph) {
		jgraph.setPreferredSize(DEFAULT_SIZE);
		Color c = DEFAULT_BG_COLOR;
		String colorStr = null;

		try {
			colorStr = getParameter("bgcolor");
		} catch (Exception e) {
		}

		if (colorStr != null) {
			c = Color.decode(colorStr);
		}

		jgraph.setBackground(c);
	}

	/**
	 * https://www.tutorials.de/threads/frame-title-nachtraeglich-aendern.159687
	 * /
	 * 
	 */
	protected void setTitle(String title) {
		getFrame().setTitle(title);
	}

	protected Frame getFrame() {
		for (Container p = getParent(); p != null; p = p.getParent()) {
			if (p instanceof Frame) {
				return (Frame) p;
			}
		}
		return null;
	}

}