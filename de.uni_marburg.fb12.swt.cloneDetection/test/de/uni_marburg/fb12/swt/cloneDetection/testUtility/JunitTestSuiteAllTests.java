package de.uni_marburg.fb12.swt.cloneDetection.testUtility;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.JunitTestSuiteCloneDetective.class,
	de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.JunitTestSuiteCloneDetectiveTupel.class,
	de.uni_marburg.fb12.swt.cloneDetection.combineDetection.JunitTestSuiteCombineDetection.class,
	de.uni_marburg.fb12.swt.cloneDetection.modelCd.JunitTestSuiteModelCdSmall.class,
	de.uni_marburg.fb12.swt.cloneDetection.modelCd.TestInternalPart2.class,
	de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel.JunitTestSuiteCanonicalLabel.class
	})
public class JunitTestSuiteAllTests {
}
