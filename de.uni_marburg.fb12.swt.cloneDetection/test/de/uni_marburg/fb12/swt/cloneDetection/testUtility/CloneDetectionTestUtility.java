package de.uni_marburg.fb12.swt.cloneDetection.testUtility;
  

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;
 

public class CloneDetectionTestUtility {
		
	/* 
	public static CloneGroupMapping getMaxCloneGroupMapping
								(Set<CloneGroupMapping> cloneGroupMappings) {
		
		Map<Integer, Set<CloneGroupMapping>> map = new HashMap<Integer, Set<CloneGroupMapping>>();
		for(CloneGroupMapping cgm: cloneGroupMappings) {
			int size = cgm.getRules().size();
			if(map.containsKey(size)) {
				map.get(size).add(cgm);
			} else { 
				Set<CloneGroupMapping> set = new HashSet<CloneGroupMapping>();
				set.add(cgm);
				map.put(size, set);
			}	
		}
		
		List<Integer> list = new LinkedList<Integer>();
		list.addAll(map.keySet());
		Collections.sort(list);
		int maxRuleSize = list.get(list.size() - 1);
		
		Set<CloneGroupMapping> cloneGroupMappingsMaxRule = map.get(maxRuleSize);
		
		CloneGroupMapping maxCgm = cloneGroupMappingsMaxRule.iterator().next();
		int max = maxCgm.getNumberOfCommonEdges() + maxCgm.getNumberOfCommonAttributes();
		for(CloneGroupMapping cgm: cloneGroupMappingsMaxRule) {
			int size = maxCgm.getNumberOfCommonEdges() + maxCgm.getNumberOfCommonAttributes();
			if (max < size) {
				maxCgm = cgm;
				max = size;
			}
			
		}
		
		return maxCgm;
	}
	
	*/
	public static CloneMatrix getMaxCloneMatrix(Set<CloneMatrix> cloneMatrices) {

		// first Number of Rules
		Map<Integer, Set<CloneMatrix>> map = new HashMap<Integer, Set<CloneMatrix>>();
		for (CloneMatrix cm : cloneMatrices) {
			int size = cm.getRuleSet().size();
			if (map.containsKey(size)) {
				map.get(size).add(cm);
			} else {
				Set<CloneMatrix> set = new HashSet<CloneMatrix>();
				set.add(cm);
				map.put(size, set);
			}
		}

		List<Integer> list = new LinkedList<Integer>();
		list.addAll(map.keySet());
		Collections.sort(list);

		int maxRuleSize = list.get(list.size() - 1);

		Set<CloneMatrix> cloneMatricesMaxRule = map.get(maxRuleSize);

		// second NumberOfCommonEdges + NumberOfCommonAttributes
		Map<Integer, Set<CloneMatrix>> map2 = new HashMap<Integer, Set<CloneMatrix>>();
		for (CloneMatrix cm : cloneMatricesMaxRule) {
			int size = cm.getNumberOfCommonEdges()
					+ cm.getNumberOfCommonAttributes();
			if (map2.containsKey(size)) {
				map2.get(size).add(cm);
			} else {
				Set<CloneMatrix> set = new HashSet<CloneMatrix>();
				set.add(cm);
				map2.put(size, set);
			}
		}

		List<Integer> list2 = new LinkedList<Integer>();
		list2.addAll(map2.keySet());
		Collections.sort(list2);

		int maxEdgeAndAttributeSize = list2.get(list2.size() - 1);

		Set<CloneMatrix> cloneMatricesMax = map2.get(maxEdgeAndAttributeSize);

		return cloneMatricesMax.iterator().next();
	}

}
