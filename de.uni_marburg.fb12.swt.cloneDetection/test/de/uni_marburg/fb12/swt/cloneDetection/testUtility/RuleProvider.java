package de.uni_marburg.fb12.swt.cloneDetection.testUtility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.ocl.examples.pivot.PivotPackage;

import GraphConstraint.GraphConstraintPackage;

/**
 * 
 * Since the rules couldn�t be provided by the ui, this class in used instead
 *
 */
public class RuleProvider {

	/**
	 * Returns a subset of the rules according to the boolean[]
	 * 
	 * @param rules
	 * @param booleanNewRules
	 * @return
	 */
	public static Collection<Rule> reduceRules(List<Rule> rules,
			boolean[] booleanNewRules) {
		Rule[] rulesArray = (Rule[]) rules.toArray(new Rule[rules.size()]);
		rules = new ArrayList<Rule>();
		for (int i = 0; i < booleanNewRules.length; i++) {
			if (booleanNewRules[i]) {
				Rule rule1 = rulesArray[i];
				System.out.println("i: " + i + " Rule: " + rule1.getName());
				rules.add(rule1);
			}
		}

		return rules;
	}

	/**
	 * Provides the RuleList of the module in the specified path.
	 * 
	 * @param relativePath
	 *            used by the HenshinResourceSet, relative to workspace
	 * @param useModuleGetRules
	 *            getRules() is deprecated, but will be used if
	 *            useModuleGetRules is true
	 * @return
	 */
	public static List<Rule> getRules(String relativePath,
			boolean useModuleGetRules) {
		return getRuleList(relativePath, useModuleGetRules);
	}

	private static List<Rule> getRuleList(String relativePath,
			boolean useModuleGetRules) {
		PivotPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("ecore", new XMIResourceFactoryImpl());
		m.put("oclas", new XMIResourceFactoryImpl());
		m.put("oclstdlib", new XMIResourceFactoryImpl());

		HenshinResourceSet resourceSet = new HenshinResourceSet();
		// Load the module and find the rule:
		Module module = resourceSet.getModule(relativePath, false);
		return getRuleList(module, useModuleGetRules);
	}

	private static List<Rule> getRuleList(Module module,
			boolean useModuleGetRules) {
		if (useModuleGetRules) {
			return module.getRules();

		} else {

			List<Rule> ergebnis = new LinkedList<Rule>();
			Iterator<Unit> ruleIterator = module.getUnits().iterator();
			while (ruleIterator.hasNext()) {
				Unit u = ruleIterator.next();
				Rule r;
				if (u instanceof Rule) {
					r = (Rule) u;
					ergebnis.add(r);
					Iterator<EObject> objectIterator = r.eAllContents();
					while (objectIterator.hasNext()) {
						Object o = objectIterator.next();
					}
				}
			}
			return ergebnis;
		}
	}

}
