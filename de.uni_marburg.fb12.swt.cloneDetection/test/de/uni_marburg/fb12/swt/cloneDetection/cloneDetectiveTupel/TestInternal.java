package de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

//import org.junit.Before;
//import org.junit.After;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;

//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.CloneDetectiveTupel;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestViaRule.IsomorphismTesterViaRule;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTesterViaGg;
//import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTester;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.CloneDetectionTestUtility;

public class TestInternal {
	private static final boolean USE_MODULE_GET_RULES = true;

	private static final String TEST_PATH
			= "files/test/cloneDetectionTest/internal/OCL2NGC.henshin";
	private static final String TEST_EXPECTED_PATH 
			= "files/test/cloneDetectionTest//internalClone/OCL2NGC.henshin";

	private static Set<Rule> testRuleSet;
	private static Rule comparisonRule;

	@BeforeClass
	public static void setUp() {
		List<Rule> rules = RuleProvider.getRules(TEST_PATH,
				USE_MODULE_GET_RULES);
		testRuleSet = new HashSet<Rule>();
		testRuleSet.addAll(rules);
		List<Rule> rules2 = RuleProvider.getRules(TEST_EXPECTED_PATH,
				USE_MODULE_GET_RULES);
		comparisonRule = rules2.iterator().next();
	}

	@Test
	public void testCloneDetectiveTupel() {
		CloneDetectiveTupel cloneDetectiveTupel = new CloneDetectiveTupel(
				testRuleSet);
		cloneDetectiveTupel.detectCloneGroups();
		Set<CloneMatrix> result = cloneDetectiveTupel.getResultAsCloneMatrix();
		for (CloneMatrix cloneMatrix : result) {
			assertTrue(cloneMatrix.getNumberOfCommonEdges() != 0
					|| cloneMatrix.getNumberOfCommonAttributes() != 0);
		}

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);

		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

}
