package de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel;

import static org.junit.Assert.*;

//import org.junit.After;
//import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.ExtendedIntegratedGraphConverter;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;

public class CanonicalLabelForFragmenCreatorAllRulesTest {

	private static String[] allRulesLabels;

	@BeforeClass
	public static void setUp() {
		final boolean USE_MODULE_GET_RULES = true;
		String realtivePath = "files/test/canonicalLabelTest/henshinclassic/OCL2NGC.henshin";

		Rule[] rulesArray;
		Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap;
		Collection<Rule> rules = RuleProvider.getRules(realtivePath,
				USE_MODULE_GET_RULES);
		rulesArray = (Rule[]) rules.toArray(new Rule[rules.size()]);

		Set<Rule> testRuleSet = new HashSet<Rule>();
		for (int i = 0; i < rulesArray.length; i++) {
			testRuleSet.add(rulesArray[i]);
			//System.out.println(rulesArray[i].toString());
		}
		ExtendedIntegratedGraphConverter graphConverter = new ExtendedIntegratedGraphConverter();
		ruleGraphMap = graphConverter
				.getRuleToGraphWithAttributesMap(testRuleSet);

		allRulesLabels = new String[rulesArray.length];
		for (int i = 0; i < rulesArray.length; i++) {
			Fragment f = new Fragment(
					ruleGraphMap.get(rulesArray[i]).edgeSet(), rulesArray[i],
					ruleGraphMap.get(rulesArray[i]));
			allRulesLabels[i] = f.getLabel();
		}

	}

	@Test
	public void test() {
		Set<String> labels = new HashSet<String>();
		for (String label : allRulesLabels) {
			assertTrue(labels.add(label));
		}
	}

}
