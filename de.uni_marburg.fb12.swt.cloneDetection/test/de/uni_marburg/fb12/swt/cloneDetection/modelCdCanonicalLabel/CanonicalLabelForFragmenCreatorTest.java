package de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel;

import static org.junit.Assert.*;

//import org.junit.After;
//import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.ExtendedIntegratedGraphConverter;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;

public class CanonicalLabelForFragmenCreatorTest {

	private static String set1NameAndAttributePreserve;
	private static String set1NoNameButAttributePreserve;
	private static String set1NameAndNoAttributePreserve;
	private static String set1NoNameAndNoAttributePreserve;

	private static String set1NameAndAttributeCreate;
	private static String set1NoNameButAttributeCreate;
	private static String set1NameAndNoAttributeCreate;
	private static String set1NoNameAndNoAttributeCreate;

	private static String set2DeleteEdge;
	private static String set2PreserveEdge;
	private static String set3InLine;
	private static String set3Fork;

	private static String set1NameAndAttributeDelete;
	private static String set1NoNameButAttributeDelete;
	private static String set1NameAndNoAttributeDelete;
	private static String set1NoNameAndNoAttributeDelete;

	private static String set1NameAndAttributeForbid;
	private static String set1NoNameButAttributeForbid;
	private static String set1NameAndNoAttributeForbid;
	private static String set1NoNameAndNoAttributeForbid;

	private static String set1NameAndAttributeRequire;
	private static String set1NoNameButAttributeRequire;
	private static String set1NameAndNoAttributeRequire;
	private static String set1NoNameAndNoAttributeRequire;

	@BeforeClass
	public static void setUp() {
		final boolean USE_MODULE_GET_RULES = true;
		String realtivePath = "files/test/canonicalLabelTest/OCL2NGC.henshin";

		Rule[] rulesArray;
		Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap;
		Collection<Rule> rules = RuleProvider.getRules(realtivePath,
				USE_MODULE_GET_RULES);
		rulesArray = (Rule[]) rules.toArray(new Rule[rules.size()]);
		/*
		for (int i = 0; i < rulesArray.length - 1; i++) {
			System.out.println(i + ": " + rulesArray[i].getName());
		}
		*/
		Set<Rule> testRuleSet = new HashSet<Rule>();
		for (int i = 0; i <= rulesArray.length - 1; i++) {
			testRuleSet.add(rulesArray[i]);
			//System.out.println(rulesArray[i].toString());
		}
		ExtendedIntegratedGraphConverter graphConverter = new ExtendedIntegratedGraphConverter();
		ruleGraphMap = graphConverter
				.getRuleToGraphWithAttributesMap(testRuleSet);

		String[] labels = new String[rulesArray.length];
		for (int i = 0; i <= rulesArray.length - 1; i++) {
			Fragment f = new Fragment(
					ruleGraphMap.get(rulesArray[i]).edgeSet(), rulesArray[i],
					ruleGraphMap.get(rulesArray[i]));
			labels[i] = f.getLabel();
		}

		set1NameAndAttributePreserve = labels[0];
		set1NoNameButAttributePreserve = labels[1];
		set1NameAndNoAttributePreserve = labels[2];
		set1NoNameAndNoAttributePreserve = labels[3];

		set1NameAndAttributeCreate = labels[4];
		set1NoNameButAttributeCreate = labels[5];
		set1NameAndNoAttributeCreate = labels[6];
		set1NoNameAndNoAttributeCreate = labels[7];

		set2DeleteEdge = labels[8];
		set2PreserveEdge = labels[9];

		set3InLine = labels[10];
		set3Fork = labels[11];

		set1NameAndAttributeDelete = labels[12];
		set1NoNameButAttributeDelete = labels[13];
		set1NameAndNoAttributeDelete = labels[14];
		set1NoNameAndNoAttributeDelete = labels[15];

		set1NameAndAttributeForbid = labels[16];
		set1NoNameButAttributeForbid = labels[17];
		set1NameAndNoAttributeForbid = labels[18];
		set1NoNameAndNoAttributeForbid = labels[19];

		set1NameAndAttributeRequire = labels[20];
		set1NoNameButAttributeRequire = labels[21];
		set1NameAndNoAttributeRequire = labels[22];
		set1NoNameAndNoAttributeRequire = labels[23];

	}

	@Test
	public void testSet1Preserve() {
		assertEquals(set1NameAndAttributePreserve, set1NameAndAttributePreserve);
		assertEquals(set1NameAndAttributePreserve,
				set1NoNameButAttributePreserve);
		assertEquals(set1NameAndNoAttributePreserve,
				set1NoNameAndNoAttributePreserve);
		assertNotEquals(set1NameAndAttributePreserve,
				set1NameAndNoAttributePreserve);
		assertNotEquals(set1NameAndNoAttributePreserve,
				set1NameAndAttributePreserve);
	}

	@Test
	public void testSet1Create() {
		assertEquals(set1NameAndAttributeCreate, set1NameAndAttributeCreate);
		assertEquals(set1NameAndAttributeCreate, set1NoNameButAttributeCreate);
		assertEquals(set1NameAndNoAttributeCreate,
				set1NoNameAndNoAttributeCreate);
		assertNotEquals(set1NameAndAttributeCreate,
				set1NameAndNoAttributeCreate);
		assertNotEquals(set1NameAndNoAttributeCreate,
				set1NameAndAttributeCreate);
	}

	@Test
	public void testSet1Delete() {
		assertEquals(set1NameAndAttributeDelete, set1NameAndAttributeDelete);
		assertEquals(set1NameAndAttributeDelete, set1NoNameButAttributeDelete);
		assertEquals(set1NameAndNoAttributeDelete,
				set1NoNameAndNoAttributeDelete);
		assertNotEquals(set1NameAndAttributeDelete,
				set1NameAndNoAttributeDelete);
		assertNotEquals(set1NameAndNoAttributeDelete,
				set1NameAndAttributeDelete);
	}

	@Test
	public void testSet1Forbid() {
		assertEquals(set1NameAndAttributeForbid, set1NameAndAttributeForbid);
		assertEquals(set1NameAndAttributeForbid, set1NoNameButAttributeForbid);
		assertEquals(set1NameAndNoAttributeForbid,
				set1NoNameAndNoAttributeForbid);
		assertNotEquals(set1NameAndAttributeForbid,
				set1NameAndNoAttributeForbid);
		assertNotEquals(set1NameAndNoAttributeForbid,
				set1NameAndAttributeForbid);
	}

	@Test
	public void testSet1Require() {
		assertEquals(set1NameAndAttributeRequire, set1NameAndAttributeRequire);
		assertEquals(set1NameAndAttributeRequire, set1NoNameButAttributeRequire);
		assertEquals(set1NameAndNoAttributeRequire,
				set1NoNameAndNoAttributeRequire);
		assertNotEquals(set1NameAndAttributeRequire,
				set1NameAndNoAttributeRequire);
		assertNotEquals(set1NameAndNoAttributeRequire,
				set1NameAndAttributeRequire);
	}

	@Test
	public void testSet1() {
		assertNotEquals(set1NameAndAttributePreserve,
				set1NameAndAttributeCreate);
		assertNotEquals(set1NoNameButAttributePreserve,
				set1NoNameButAttributeCreate);
		assertNotEquals(set1NameAndNoAttributePreserve,
				set1NameAndNoAttributeCreate);
		assertNotEquals(set1NoNameAndNoAttributePreserve,
				set1NoNameAndNoAttributeCreate);
	}

	@Test
	public void testSet2() {
		assertNotEquals(set2DeleteEdge, set2PreserveEdge);
	}

	@Test
	public void testSet3() {
		assertNotEquals(set3InLine, set3Fork);
	}

}
