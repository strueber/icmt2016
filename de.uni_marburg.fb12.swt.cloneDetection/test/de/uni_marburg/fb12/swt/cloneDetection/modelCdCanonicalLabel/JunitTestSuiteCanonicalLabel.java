package de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	CanonicalLabelForFragmenCreatorAllRulesTest.class,
	CanonicalLabelForFragmenCreatorTest.class,
	})
public class JunitTestSuiteCanonicalLabel {
}
