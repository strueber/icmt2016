package de.uni_marburg.fb12.swt.cloneDetection.combineDetection;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
//import org.junit.Before;
//import org.junit.After;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.testUtility.CloneDetectionTestUtility;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CloneDetectiveWithEScanGroupAndFilterAdded;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CombineDetection;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CombineDetectionCDTwithEScan3D;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetection.CombineDetectionCDwithEScanInc;
import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneDetection;

//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestViaRule.IsomorphismTesterViaRule;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTesterViaGg;

public class TestRequire {
	private static final boolean USE_MODULE_GET_RULES = true;

	private static final String TEST_PATH 
			= "files/test/cloneDetectionTest/require/OCL2NGC.henshin";
	private static final String TEST_EXPECTED_PATH 
			= "files/test/cloneDetectionTest/requireClone/OCL2NGC.henshin";

	private static Set<Rule> testRuleSet;
	private static Rule comparisonRule;

	@BeforeClass
	public static void setUp() {
		List<Rule> rules = RuleProvider.getRules(TEST_PATH,
				USE_MODULE_GET_RULES);
		testRuleSet = new HashSet<Rule>();
		testRuleSet.addAll(rules);
		List<Rule> rules2 = RuleProvider.getRules(TEST_EXPECTED_PATH,
				USE_MODULE_GET_RULES);
		comparisonRule = rules2.iterator().next();
	}

	@Test
	public void testCloneDetectiveWithEScanGroupAndFilterAdded() {
		CombineDetection combineDetection = new CloneDetectiveWithEScanGroupAndFilterAdded(
				testRuleSet);
		combineDetection.detectCloneGroups();
		Set<CloneMatrix> result = combineDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testCombineDetectionCDwithEScanInc() {
		CloneDetection combineDetection = new CombineDetectionCDwithEScanInc(
				testRuleSet);
		combineDetection.detectCloneGroups();
		Set<CloneMatrix> result = combineDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

	@Test
	public void testCombineDetectionCDTwithEScan3D() {
		CloneDetection combineDetection = new CombineDetectionCDTwithEScan3D(
				testRuleSet);
		combineDetection.detectCloneGroups();
		Set<CloneMatrix> result = combineDetection.getResultAsCloneMatrix();

		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));

		CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm,
				comparisonRule));
		assertTrue(IsomorphismTesterViaGg
				.testCloneMatrix(maxCm, comparisonRule));

	}

}
