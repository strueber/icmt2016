package de.uni_marburg.fb12.swt.cloneDetection.cloneDetective;
  
import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
//import org.junit.Before;
//import org.junit.After;








import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;


//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;  


import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestViaRule.IsomorphismTesterViaRule;
//import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTester;
import de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph.IsomorphismTesterViaGg;

public class TestRequire {
	private static final boolean USE_MODULE_GET_RULES = true;

	private static final String TEST_PATH 
			= "files/test/cloneDetectionTest/require/OCL2NGC.henshin";
	//private static final String TEST_EXPECTED_PATH 
	//		= "files/test/cloneDetectionTest/requireClone/OCL2NGC.henshin";
	
	private static Set<Rule> testRuleSet; 
	//private static Rule comparisonRule;
	
	@BeforeClass
	public static void setUp() {
		List<Rule> rules = RuleProvider.getRules(TEST_PATH, USE_MODULE_GET_RULES);
		testRuleSet = new HashSet<Rule>();
		testRuleSet.addAll(rules);
		//List<Rule> rules2 = RuleProvider.getRules(TEST_EXPECTED_PATH, USE_MODULE_GET_RULES);
		//comparisonRule = rules2.iterator().next();
	}
	
		
	

	

	@Test
	public void testCloneDetective() {
		CloneDetective cloneDetective = new CloneDetective(testRuleSet);
		cloneDetective.detectCloneGroups();
		Set<CloneMatrix> result =  cloneDetective.getResultAsCloneMatrix();
		
		assertTrue(IsomorphismTesterViaRule.testResult(result));
		assertTrue(IsomorphismTesterViaGg.testResult(result));
		
		//maxCm has more than two Rules, but CloneDetective itself only finds CloenPairs, 
		//means only 2 Rules
		//CloneMatrix maxCm = CloneDetectionTestUtility.getMaxCloneMatrix(result);
		//assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm, comparisonRule));
		//assertTrue(IsomorphismTesterViaRule.testCloneMatrix(maxCm, comparisonRule));
	
	}	
}
