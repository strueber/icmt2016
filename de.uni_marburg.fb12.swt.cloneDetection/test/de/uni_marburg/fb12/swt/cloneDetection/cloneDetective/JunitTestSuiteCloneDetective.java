package de.uni_marburg.fb12.swt.cloneDetection.cloneDetective;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	TestSmall.class, 
	TestDelete.class, 
	TestRequire.class,
	TestInternal.class
	})
public class JunitTestSuiteCloneDetective {
}
