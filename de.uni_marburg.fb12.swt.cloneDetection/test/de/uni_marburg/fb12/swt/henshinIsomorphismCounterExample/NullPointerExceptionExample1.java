package de.uni_marburg.fb12.swt.henshinIsomorphismCounterExample;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

//import org.junit.Before;
//import org.junit.After;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.testUtility.RuleProvider;

public class NullPointerExceptionExample1 {
	private static final boolean USE_MODULE_GET_RULES = true;

	private static final String TEST_PATH 
			= "test/de/uni_marburg/fb12/swt/henshinIsomorphismCounterExample/0isomorphism1/OCL2NGC.henshin";

	private static Set<Rule> testRuleSet;

	@BeforeClass
	public static void setUp() {
		List<Rule> rules = RuleProvider.getRules(TEST_PATH,
				USE_MODULE_GET_RULES);
		testRuleSet = new HashSet<Rule>();
		testRuleSet.addAll(rules);
	}

	@Test
	public void test() {
		Rule rule1 = testRuleSet.iterator().next();

		Graph lhs1 = rule1.getLhs();
		EGraph eLhs1 = new EGraphImpl();
		eLhs1.addTree(lhs1);

		Graph rhs1 = rule1.getRhs();
		EGraph eRhs1 = new EGraphImpl();
		eRhs1.addTree(rhs1);

		assertTrue(InterpreterUtil.areIsomorphic(eLhs1, eLhs1));
		assertTrue(InterpreterUtil.areIsomorphic(eRhs1, eRhs1));

		for (Rule rule : testRuleSet) {
			Graph lhs = rule.getLhs();
			EGraph eLhs = new EGraphImpl();
			eLhs.addTree(lhs);

			Graph rhs = rule.getRhs();
			EGraph eRhs = new EGraphImpl();
			eRhs.addTree(rhs);

			assertTrue(InterpreterUtil.areIsomorphic(eLhs, eLhs1));
			assertTrue(InterpreterUtil.areIsomorphic(eRhs, eRhs1));
		}

	}

}
