package de.uni_marburg.fb12.swt.henshinIsomorphismCounterExample;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	WorkingExample.class,
	NullPointerExceptionExample1.class,
	NullPointerExceptionExample2.class,
	InfiniteLoop.class
	})
public class JunitTestSuite {
}
