/**
 */
package GraphConstraint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>True</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see GraphConstraint.GraphConstraintPackage#getTrue()
 * @model
 * @generated
 */
public interface True extends NestedGraphCondition {
} // True
