package org.eclipse.emf.henshin.variability.mergein.refactoring.logic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mergeSuggestion.MergeRule;
import mergeSuggestion.MergeRuleElement;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.henshin.model.And;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Formula;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.GraphElement;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Mapping;
import org.eclipse.emf.henshin.model.MappingList;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.NestedCondition;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Not;
import org.eclipse.emf.henshin.model.Parameter;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.variability.mergein.refactoring.util.StringUtil;

public class Merger {

	private static final String COMMA = ",";
	private static final String BRACKET_RIGHT = ")";
	private static final String BRACKET_LEFT = "(";
	private static final String XOR = "xor";
	private static final String OR = " or ";
	private MergeRule mergeRule;
	private Module module;
	private RuleSpecifics commons, masterSpecifics;
	private List<RuleSpecifics> ruleSpecifics;
	private RuleParameters commonParameters, masterSpecificParameters;
	private List<RuleParameters> ruleSpecificParameters;
	private HashMap<GraphElement, GraphElement> mapOldNewGraphElements;
	private Rule masterRule;
	private List<Rule> furtherRules;
	
	private RuleSpecifics masterSubCommons;
	private List<RuleSpecifics> ruleSubCommons;

	private HashMap<Parameter, String> mapParameterToOldName;
	private HashMap<Parameter, String> mapParameterToNewName;
	private HashMap<String, Set<Parameter>> mapOldNameToParametersMap;
	
	private boolean entireAcIsCommon;

	public Merger(MergeRule mergeRule) throws MergeInException {
		this.mergeRule = mergeRule;
		this.masterRule = mergeRule.getMasterRule();
		this.furtherRules = mergeRule.getRules();
		this.furtherRules.remove(masterRule);
		this.setModule();
		this.initEntireAcIsCommon();
		this.initCommonsAndMap();
		this.initMasterSpecifics();
		this.initRuleSpecifics();
		this.initMasterSubCommons();
		this.initRuleSubCommons();
		this.initCommonAndMasterSpecificParameters();
		this.initRuleSpecificParameters();
//		TestPrinter.printAll(commons, masterSpecifics, ruleSpecifics,
//		mapOldNewGraphElements); // Test!!!
		// TestPrinter.printAll(commonParameters, masterSpecificParameters,
		// ruleSpecificParameters); // Test!!!
//		TestPrinter.printAll(masterSubCommons, ruleSubCommons);
		// TestPrinter.printMap(mapOldNewGraphElements);
	}

	private void initRuleSubCommons() {
		ruleSubCommons = new ArrayList<RuleSpecifics>();
		int numberOfRules = furtherRules.size() + 1;
		for (Rule rule : furtherRules) {
			RuleSpecifics commons = new RuleSpecifics(rule);
			for (MergeRuleElement mergeRuleElement : mergeRule.getElements()) {
				if (mergeRuleElement.getReferenceElements().size() > 1 &&
						mergeRuleElement.getReferenceElements().size() < numberOfRules &&
						! isApplicationConditionElement(mergeRuleElement)) {
					for (GraphElement elem : mergeRuleElement.getReferenceElements()) {
						if (elem.getGraph().getRule() != null
								&& elem.getGraph().getRule() == rule) {
							commons.addGraphElement(elem);
							GraphElement key = getKeyForGraphElement(mergeRuleElement); 
							if (key != null) {
								mapOldNewGraphElements.put(elem, mapOldNewGraphElements.get(key));
							} else {
								mapOldNewGraphElements.put(elem, elem);
							}
						}
					}
				}
			}
			ruleSubCommons.add(commons);
		}
	}

	private GraphElement getKeyForGraphElement(MergeRuleElement mergeRuleElement) {
		for (GraphElement elem : mergeRuleElement.getReferenceElements()) {
			if (mapOldNewGraphElements.containsKey(elem)) return elem;
		}
		return null;
	}

	private void initMasterSubCommons() {
		masterSubCommons = new RuleSpecifics(masterRule);
		int numberOfRules = furtherRules.size() + 1;
		for (MergeRuleElement mergeRuleElement : mergeRule.getElements()) {
			if (mergeRuleElement.getReferenceElements().size() > 1 &&
					mergeRuleElement.getReferenceElements().size() < numberOfRules &&
					! isApplicationConditionElement(mergeRuleElement)) {
				for (GraphElement elem : mergeRuleElement.getReferenceElements()) {
					if (elem.getGraph().getRule() != null
							&& elem.getGraph().getRule() == masterRule) {
						masterSubCommons.addGraphElement(elem);
						mapOldNewGraphElements.put(elem, elem);
					}
				}
			}
		}
	}

	private void initEntireAcIsCommon() {
		this.entireAcIsCommon = false;
		if (masterRule.getLhs().getFormula() != null) {	
			boolean isCommon = true;
			int numberOfRules = this.furtherRules.size() + 1;
			for (MergeRuleElement mre : mergeRule.getElements()) {
				if (isApplicationConditionElement(mre)) {
					if (mre.getReferenceElements().size() != numberOfRules) {
						isCommon = false;
						break;
					}
				}
			}
			this.entireAcIsCommon = isCommon;
		}
//		System.out.println("IS COMMON: " + this.entireAcIsCommon);
	}

	private void initRuleSpecificParameters() {
		ruleSpecificParameters = new ArrayList<RuleParameters>();
		for (Rule rule : furtherRules) {
			RuleParameters specificParameters = new RuleParameters(rule);
			for (Parameter param : rule.getParameters()) {
				if (isRuleSpecificParameter(masterRule, param)) {
					specificParameters.addParameter(param);
				}
			}
			ruleSpecificParameters.add(specificParameters);
		}
	}

	private boolean isRuleSpecificParameter(Rule masterRule, Parameter param) {
		for (Parameter commonParameter : commonParameters.getParameters()) {
			if (commonParameter.getName() != null && param.getName() != null) {
				if (commonParameter.getName().equals(param.getName())) {
					if (commonParameter.getType() == null
							&& param.getType() == null) {
						return false;
					}
					if (commonParameter.getType() != null
							&& param.getType() != null) {
						if (commonParameter.getType() == param.getType()) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private void initCommonAndMasterSpecificParameters() {
		this.commonParameters = new RuleParameters(masterRule);
		this.masterSpecificParameters = new RuleParameters(masterRule);
		for (Parameter param : masterRule.getParameters()) {
			if (isUsedInCommonPart(param)) {
				commonParameters.addParameter(param);
			} else {
				masterSpecificParameters.addParameter(param);
			}
		}
	}

	private boolean isUsedInCommonPart(Parameter param) {
		for (GraphElement elem : commons.getSpecificElements()) {
			if (elem instanceof Node) {
				Node node = (Node) elem;
				if (node.getName() != null
						&& node.getName().equals(param.getName())) {
					return true;
				}
			}
			if (elem instanceof Attribute) {
				Attribute attribute = (Attribute) elem;
				if (attribute.getValue() != null
						&& attribute.getValue().contains(param.getName())) {
					return true;
				}
			}
		}
		return false;
	}

	public void merge(String newName) throws MergeInException {
		if (!furtherRules.isEmpty()) {
			mergeParameters();
			setPresenceConditions();
//			setPresenceCondition(masterSpecifics);
//			setPresenceConditionSubCommons(masterSubCommons);
			for (RuleSpecifics subCommons : ruleSubCommons) {
				mergeRuleSubCommons(subCommons);
			}
			for (RuleSpecifics specifics : ruleSpecifics) {
				merge(specifics);
			}
			setFeatureModel();
			removeNonMasterRules();
			setNewName(newName);
		}
	}

	private void setPresenceConditions() {
		int numberOfRules = furtherRules.size() + 1;
		for (MergeRuleElement mre : mergeRule.getElements()) {
			if (mre.getReferenceElements().size() != numberOfRules) {
				for (GraphElement elem : mre.getReferenceElements()) {
					if (! (isApplicationConditionElement(mre) && this.entireAcIsCommon)) {
						String condition = getCondition(mre);
						elem.setPresenceCondition(condition);
					}
				}
			}
		}
	}

	private String getCondition(MergeRuleElement mre) {
		List<Rule> affectedRules = new ArrayList<Rule>();
		for (GraphElement elem : mre.getReferenceElements()) {
			affectedRules.add(elem.getGraph().getRule());
		}
		String condition = "";
		if (! affectedRules.isEmpty()) {
			condition = getCondition(affectedRules.get(0));
		}
		for (int i = 1; i < affectedRules.size(); i++) {
			condition += OR + getCondition(affectedRules.get(i));
		}
		return condition;
	}

	private void mergeRuleSubCommons(RuleSpecifics subCommons) {
//		setPresenceConditionSubCommons(subCommons);
		List<Node> nodesLHS = new ArrayList<Node>();
		List<Node> nodesRHS = new ArrayList<Node>();
		List<Edge> edgesLHS = new ArrayList<Edge>();
		List<Edge> edgesRHS = new ArrayList<Edge>();
		List<Attribute> attributes = new ArrayList<Attribute>();
		List<GraphElement> nestedElements = new ArrayList<GraphElement>();
		fillLists(nodesLHS, nodesRHS, edgesLHS, edgesRHS, attributes,
				nestedElements, subCommons);
		MappingList mappingList = subCommons.getRule().getMappings();
		this.addNodes2Graph(masterRule.getLhs(), nodesLHS);
		this.addNodes2Graph(masterRule.getRhs(), nodesRHS);
		this.addMappings2Rule(masterRule, mappingList);
		this.addAttributes2Nodes(attributes);
		this.addEdges2Graph(masterRule.getLhs(), edgesLHS);
		this.addEdges2Graph(masterRule.getRhs(), edgesRHS);
	}

	private void setPresenceConditionSubCommons(RuleSpecifics subCommons) {
		String condition = getCondition(subCommons);
		for (GraphElement elem : subCommons.getSpecificElements()) {
			if ((! elem.getGraph().isNestedCondition()) ||
					(! this.entireAcIsCommon))
			System.out.println("elem: " + elem + "; condition: " + condition);
			elem.setPresenceCondition(condition + " ");
		}
	}

	private String getCondition(RuleSpecifics subCommons) {
		List<Rule> affectedRules = new ArrayList<Rule>();
		for (GraphElement key : mapOldNewGraphElements.keySet()) {
//			System.out.println("key: " + key + "; contains: " + );
			if (subCommons.getSpecificElements().contains(mapOldNewGraphElements.get(key))) {
				if (! affectedRules.contains(key.getGraph().getRule())) {
					affectedRules.add(key.getGraph().getRule());
				}
			}
		}
		String condition = "";
		if (! affectedRules.isEmpty()) {
			condition = getCondition(affectedRules.get(0));
		}
		for (int i = 1; i < affectedRules.size(); i++) {
			condition += OR + getCondition(affectedRules.get(i));
		}
		return condition;
	}

	private void setPresenceConditionOnAC(Rule rule) {
//		String condition = getCondition(rule);
//		if (rule.getLhs().getFormula() != null) {
//			if (rule.getLhs().getFormula() instanceof Not) {
//				Not not = (Not) rule.getLhs().getFormula();
//				if (not.getChild() instanceof NestedCondition) {
//					NestedCondition nc = (NestedCondition) not.getChild();
//					nc.setPresenceCondition(condition + " ");
//				}
//			}
//			if (rule.getLhs().getFormula() instanceof NestedCondition) {
//				NestedCondition nc = (NestedCondition) rule.getLhs().getFormula();
//				nc.setPresenceCondition(condition + " ");
//			}
//		}
	}

	private void mergeParameters() {
		populateParameterMaps();
		renameSpecificParameters();

		updateReferencesToSpecificParameter(masterSpecificParameters);
		for (RuleParameters specificParams : ruleSpecificParameters) {
			updateReferencesToSpecificParameter(specificParams);
		}
		completeParameterList();
	}

	private void populateParameterMaps() {
		mapParameterToNewName = new HashMap<Parameter, String>();
		mapParameterToOldName = new HashMap<Parameter, String>();
		mapOldNameToParametersMap = new HashMap<String, Set<Parameter>>();
		for (Parameter param : masterSpecificParameters.getParameters()) {
			addToParameterMaps(param);
		}
		for (RuleParameters specificParams : ruleSpecificParameters) {
			for (Parameter param : specificParams.getParameters()) {
				addToParameterMaps(param);
			}
		}
	}

	private void addToParameterMaps(Parameter param) {
		mapParameterToOldName.put(param, param.getName());
		Set<Parameter> params = mapOldNameToParametersMap.get(param.getName());
		if (params == null) {
			params = new HashSet<Parameter>();
			mapOldNameToParametersMap.put(param.getName(), params);
		}
		params.add(param);
	}

	private void completeParameterList() {
		for (Parameter param : masterSpecificParameters.getParameters()) {
			masterRule.getParameters().add(param);
		}
		for (RuleParameters specificParams : ruleSpecificParameters) {
			for (Parameter param : specificParams.getParameters()) {
				masterRule.getParameters().add(param);
			}
		}
	}

	private void renameSpecificParameters() {
		for (String oldName : mapOldNameToParametersMap.keySet()) {
			String newName = oldName;
			for (Parameter param : mapOldNameToParametersMap.get(oldName)) {
				Rule rule = (Rule) param.getUnit();
				if (rule.getName() != null) {
					String ruleName = rule.getName().toLowerCase();
					newName = joinCamelCase(ruleName, newName);
				}
			}
			for (Parameter param : mapOldNameToParametersMap.get(oldName)) {
				param.setName(newName);
				mapParameterToNewName.put(param, newName);
			}
		}
	}

	private String joinCamelCase(String a, String b) {
		StringBuilder sb = new StringBuilder();
		sb.append(a);
		if (sb != null & sb.length() > 1) {
			sb.append(Character.toUpperCase(b.charAt(0)));
			sb.append(b.substring(1));
		} else {
			sb.append(b);
		}
		return sb.toString();
	}

	private void updateReferencesToSpecificParameter(
			RuleParameters ruleParameters) {
		Rule rule = ruleParameters.getRule();
		RuleSpecifics specifics = getRuleSpecifics(rule);
		for (Parameter param : ruleParameters.getParameters()) {
			String oldName = mapParameterToOldName.get(param);
			String newName = mapParameterToNewName.get(param);
			if (specifics != null) {
				for (GraphElement elem : specifics.getSpecificElements()) {
					if (elem instanceof Node) {
						Node node = (Node) elem;
						if (node.getName() != null
								&& node.getName().equals(oldName)) {
							node.setName(newName);
						}
						for (Attribute attribute : node.getAttributes())
							updateAttributeReferencesToSpecificParamter(
									attribute, oldName, newName);
					}
					if (elem instanceof Attribute
							&& !specifics.getSpecificElements().contains(
									((Attribute) elem).getNode())) {
						Attribute attribute = (Attribute) elem;
						updateAttributeReferencesToSpecificParamter(attribute,
								oldName, newName);
					}
				}
			}
		}

	}

	private void updateAttributeReferencesToSpecificParamter(
			Attribute attribute, String oldName, String newName) {
		if ("opname".equals(attribute.getValue()))
			System.out.println(attribute.getNode().getGraph());
		if (attribute.getValue() != null
				&& attribute.getValue().contains(oldName)
				&& !attribute.getValue().contains(newName)) {
			String oldValue = attribute.getValue();
			String newValue = StringUtil.cleanReplace(oldValue, oldName,
					newName);
			// String newValue = oldValue.replace(oldName, newName);
			attribute.setValue(newValue);
		}
	}

	private RuleSpecifics getRuleSpecifics(Rule rule) {
		if (masterRule == rule) {
			return masterSpecifics;
		}
		for (RuleSpecifics specifics : ruleSpecifics) {
			if (specifics.getRule() == rule) {
				return specifics;
			}
		}
		return null;
	}

	private void setNewName(String newName) throws MergeInException {
		// if (module.getUnit(newName) != null) {
		// throw new MergeInException(MergeInException.NAME_CONFLICT);
		// } else {
		masterRule.setName(newName);
		// }
	}

	private void setFeatureModel() {
		String featureModel = XOR;
		featureModel += BRACKET_LEFT;
		featureModel += getCondition(masterRule);
		for (Rule rule : furtherRules) {
			featureModel += COMMA;
			featureModel += getCondition(rule);
		}
		featureModel += BRACKET_RIGHT;
//		masterRule.setFeatureModel(featureModel);
	}

	private void merge(RuleSpecifics specifics) {
//		setPresenceCondition(specifics);
		List<Node> nodesLHS = new ArrayList<Node>();
		List<Node> nodesRHS = new ArrayList<Node>();
		List<Edge> edgesLHS = new ArrayList<Edge>();
		List<Edge> edgesRHS = new ArrayList<Edge>();
		List<Attribute> attributes = new ArrayList<Attribute>();
		List<GraphElement> nestedElements = new ArrayList<GraphElement>();
		fillLists(nodesLHS, nodesRHS, edgesLHS, edgesRHS, attributes,
				nestedElements, specifics);
		MappingList mappingList = specifics.getRule().getMappings();
		this.addNodes2Graph(masterRule.getLhs(), nodesLHS);
		this.addNodes2Graph(masterRule.getRhs(), nodesRHS);
		this.addMappings2Rule(masterRule, mappingList);
		this.addAttributes2Nodes(attributes);
		this.addEdges2Graph(masterRule.getLhs(), edgesLHS);
		this.addEdges2Graph(masterRule.getRhs(), edgesRHS);
		if (! this.entireAcIsCommon) {
			if (masterRule.getLhs().getFormula() != null) {
				setACName(masterRule.getLhs().getFormula(), masterRule);
			}
			this.addNestedCondition2Graph(masterRule.getLhs(), 
					nestedElements, specifics.getRule());
		}
	}

	private void addNestedCondition2Graph(Graph graph,
			List<GraphElement> nestedElements, Rule rule) {
		if (nestedElements.isEmpty())
			return;
		Formula formula = rule.getLhs().getFormula();
		if (graph.getFormula() == null) {
			graph.setFormula(formula);
		} else {
			And and = HenshinFactory.eINSTANCE.createAnd();
			and.setLeft(graph.getFormula());
			and.setRight(formula);
			graph.setFormula(and);
		}
		adjustMappings(formula);
		setACName(formula, rule);
	}

	private void setACName(Formula formula, Rule rule) {
		if (formula instanceof NestedCondition) {
			String newACName = "AC";
			if (rule.getName() != null) {
				newACName += rule.getName();
			}
			NestedCondition nestedCondition = (NestedCondition) formula;
			nestedCondition.getConclusion().setName(newACName);
		}
		if (formula instanceof Not) {
			Not not = (Not) formula;
			setACName(not.getChild(), rule);
		}
	}

	private void adjustMappings(Formula formula) {
		if (formula instanceof NestedCondition) {
			NestedCondition nestedCondition = (NestedCondition) formula;
			Iterator<Mapping> iter = nestedCondition.getMappings().iterator();
			while (iter.hasNext()) {
				Mapping mapping = iter.next();
				Node oldOrigin = mapping.getOrigin();
				mapping.setOrigin((Node) mapOldNewGraphElements.get(oldOrigin));
			}
		}
		if (formula instanceof Not) {
			Not not = (Not) formula;
			adjustMappings(not.getChild());
		}
	}

	private void addAttributes2Nodes(List<Attribute> attributes) {
		for (Attribute attribute : attributes) {
			Node newOwningNode = (Node) mapOldNewGraphElements.get(attribute.getNode());
			if (newOwningNode.getAttribute(attribute.getType()) == null) {
				newOwningNode.getAttributes().add(attribute);
			}
		}
	}

	private void addEdges2Graph(Graph graph, List<Edge> edges) {
		for (Edge edge : edges) {
			Node sourceNew = (Node) mapOldNewGraphElements.get(edge.getSource());
			Node targetNew = (Node) mapOldNewGraphElements.get(edge.getTarget());
			Edge  e = sourceNew.getOutgoing(edge.getType(), targetNew); 
			if (e == null) {
				edge.setSource(sourceNew);
				edge.setTarget(targetNew);
				graph.getEdges().add(edge);
			} else {
				if (e == edge && ! graph.getEdges().contains(edge)) {
					graph.getEdges().add(edge);
				}
			}
		}
	}

	private void addMappings2Rule(Rule rule, MappingList mappingList) {
		Iterator<Mapping> iter = mappingList.iterator();
		while (iter.hasNext()) {
			Mapping mapping = iter.next();
			Node origin = ((Node) mapOldNewGraphElements.get(mapping.getOrigin()));
			Node image = ((Node) mapOldNewGraphElements.get(mapping.getImage()));
			if (rule.getMappings().get(origin, image) == null) {
				rule.getMappings().add(HenshinFactory.eINSTANCE.createMapping(origin, image));
			}
		}
	}

	private void addNodes2Graph(Graph graph, List<Node> nodes) {
		for (Node node : nodes) {
			if (mapOldNewGraphElements.get(node) == node) {
				graph.getNodes().add(node);
			}
		}
	}

	private void fillLists(List<Node> nodesLHS, List<Node> nodesRHS,
			List<Edge> edgesLHS, List<Edge> edgesRHS,
			List<Attribute> attributes, List<GraphElement> nestedElements,
			RuleSpecifics specifics) {
		for (GraphElement elem : specifics.getSpecificElements()) {
			if (elem instanceof Node) {
				if (elem.getGraph().isLhs()) {
					nodesLHS.add((Node) elem);
				}
				if (elem.getGraph().isRhs()) {
					nodesRHS.add((Node) elem);
				}
			}
			if (elem instanceof Edge) {
				if (elem.getGraph().isLhs()) {
					edgesLHS.add((Edge) elem);
				}
				if (elem.getGraph().isRhs()) {
					edgesRHS.add((Edge) elem);
				}
			}
			if (elem instanceof Attribute) {
				if (!elem.getGraph().isNestedCondition()) {
					attributes.add((Attribute) elem);
				}
			}
			if (elem.getGraph().isNestedCondition()) {
				nestedElements.add(elem);
			}
		}
	}

	private void removeNonMasterRules() throws MergeInException {
		for (Rule rule : furtherRules) {
			if (rule.getModule() == null) {
				throw new MergeInException(MergeInException.NO_MODULE_2);
			}
		}
		module.getUnits().removeAll(furtherRules);
	}

	private void setPresenceCondition(RuleSpecifics specifics) {
		String condition = getCondition(specifics.getRule());
		for (GraphElement elem : specifics.getSpecificElements()) {
			if ((! elem.getGraph().isNestedCondition()) ||
					(! this.entireAcIsCommon))
			elem.setPresenceCondition(condition + " ");
		}
		if (! this.entireAcIsCommon) {
			setPresenceConditionOnAC(specifics.getRule());
		}
	}

	private String getCondition(Rule rule) {
		String ruleName = rule.getName();
		ruleName = ruleName.replaceAll("\\W", "");
		ruleName = ruleName.replaceAll("_", "");
		return "def(" + ruleName.toLowerCase() + BRACKET_RIGHT;
	}

	public void saveModule(String path) throws MergeInException {
		File file = new File(path);
		HenshinResourceSet resourceSet = new HenshinResourceSet(
				file.getParent());
		Resource resource = resourceSet.createResource(path);
		resource.getContents().add(module);
		try {
			resource.save(null);
		} catch (IOException e) {
			throw new MergeInException(MergeInException.SAVING_IMPOSSIBLE);
		}
	}

	public Module getModule() {
		return this.module;
	}

	private void initRuleSpecifics() {
		ruleSpecifics = new ArrayList<RuleSpecifics>();
		for (Rule rule : furtherRules) {
			RuleSpecifics specifics = new RuleSpecifics(rule);
			for (MergeRuleElement mergeRuleElement : mergeRule.getElements()) {
				if (mergeRuleElement.getReferenceElements().size() == 1) {
					GraphElement graphElement = mergeRuleElement
							.getReferenceElements().get(0);
					if (graphElement.getGraph().getRule() != null
							&& graphElement.getGraph().getRule() == rule) {
						specifics.addGraphElement(graphElement);
						mapOldNewGraphElements.put(graphElement, graphElement);
					} 
					// the following addresses the design decision to have
					// either a complete AC in the clone or nothing from it
					// => could be removed due to valid input model tests
				} else if (isApplicationConditionElement(mergeRuleElement) &&
						!fullApplicationConditionIsCommon(mergeRuleElement)) {
					GraphElement graphElement = getApplicationConditionElement(mergeRuleElement, rule);
					if (graphElement != null) {
						specifics.addGraphElement(graphElement);
						mapOldNewGraphElements.put(graphElement, graphElement);
					}
				}
			}
			ruleSpecifics.add(specifics);
		}
	}

	private GraphElement getApplicationConditionElement(
			MergeRuleElement mergeRuleElement, Rule rule) {
		for (GraphElement element : mergeRuleElement.getReferenceElements()) {
			if (element.getGraph().getRule() == rule)
				return element;
		}
		return null;
	}

	private void initMasterSpecifics() {
		masterSpecifics = new RuleSpecifics(masterRule);
		for (MergeRuleElement mergeRuleElement : mergeRule.getElements()) {
			if (mergeRuleElement.getReferenceElements().size() == 1) {
				GraphElement graphElement = mergeRuleElement
						.getReferenceElements().get(0);
				if (graphElement.getGraph().getRule() != null
						&& graphElement.getGraph().getRule() == masterRule) {
					masterSpecifics.addGraphElement(graphElement);
					mapOldNewGraphElements.put(graphElement, graphElement);
				} 
				// the following addresses the design decision to have
				// either a complete AC in the clone or nothing from it
				// => could be removed due to valid input model tests
			} else if (isApplicationConditionElement(mergeRuleElement) &&
					!fullApplicationConditionIsCommon(mergeRuleElement)) {
				GraphElement graphElement = getApplicationConditionElement(mergeRuleElement, masterRule);
				if (graphElement != null) {
					masterSpecifics.addGraphElement(graphElement);
					mapOldNewGraphElements.put(graphElement, graphElement);
				}
			}
		}
	}

	private void initCommonsAndMap() {
		commons = new RuleSpecifics(masterRule);
		mapOldNewGraphElements = new HashMap<GraphElement, GraphElement>();
		int numberOfRules = furtherRules.size() + 1;
		for (MergeRuleElement mergeRuleElement : mergeRule.getElements()) {
			if (mergeRuleElement.getReferenceElements().size() == numberOfRules) {
				// the following addresses the design decision to have
				// either a complete AC in the clone or nothing from it
				// => could be removed due to valid input model tests
				if (isApplicationConditionElement(mergeRuleElement)) {
					if (!fullApplicationConditionIsCommon(mergeRuleElement))
						continue;
				}
				GraphElement masterRuleElement = null;
				for (GraphElement graphElement : mergeRuleElement
						.getReferenceElements()) {
					if (graphElement.getGraph().getRule() != null
							&& graphElement.getGraph().getRule() == masterRule) {
						masterRuleElement = graphElement;
						break;
					}
				}
				for (GraphElement graphElement : mergeRuleElement
						.getReferenceElements()) {
					mapOldNewGraphElements.put(graphElement, masterRuleElement);
				}
				commons.addGraphElement(masterRuleElement);
			}
		}
	}

	private boolean fullApplicationConditionIsCommon(
			MergeRuleElement mergeRuleElement) {
		int numberOfRules = furtherRules.size() + 1;
		GraphElement any = mergeRuleElement.getReferenceElements().iterator()
				.next();
		for (GraphElement element : getContainedElements(any.getGraph())) {
			int correspondingElements = 0;
			for (MergeRuleElement el : mergeRule.getElements()) {
				if (el.getReferenceElements().contains(element)) {
					correspondingElements = el.getReferenceElements().size();
					continue;
				}
			}
			if (correspondingElements < numberOfRules)
				return false;
		}
		return true;
	}

	private List<GraphElement> getContainedElements(Graph ac) {
		List<GraphElement> result = new ArrayList<GraphElement>();
		for (Node node : ac.getNodes()) {
			result.add(node);
			result.addAll(node.getAttributes());
		}
		result.addAll(ac.getEdges());
		return result;
	}

	private boolean isApplicationConditionElement(
			MergeRuleElement mergeRuleElement) {
		GraphElement any = mergeRuleElement.getReferenceElements().iterator()
				.next();
		if (any.getGraph().isNestedCondition()) {
			return true;
		} else {
			return false;
		}
	}

	private void setModule() throws MergeInException {
		if (mergeRule != null) {
			if (masterRule != null) {
				if (masterRule.getModule() != null) {
					module = masterRule.getModule();
				} else {
					throw new MergeInException(MergeInException.NO_MODULE);
				}
			} else {
				throw new MergeInException(MergeInException.NO_MASTERRULE);
			}
		} else {
			throw new MergeInException(MergeInException.NO_MERGERULE);
		}
	}
}
