package org.eclipse.emf.henshin.variability.mergein.refactoring.popup.actions;

import java.io.File;

import mergeSuggestion.MergeRule;
import mergeSuggestion.MergeSuggestion;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.variability.mergein.refactoring.logic.MergeInException;
import org.eclipse.emf.henshin.variability.mergein.refactoring.logic.Merger;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class MergeAction implements IObjectActionDelegate {

	private Shell shell;
	private MergeSuggestion mergeSuggestion;
	private MergeRule mergeRule;

	private static final String[] FILTER_NAMES = { "Henshin Files (*.henshin)" };
	private static final String[] FILTER_EXTS = { "*.henshin" };
	private static final String HEADER = "MergeIn refactoring";

	/**
	 * Constructor for Action1.
	 */
	public MergeAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	public void setMergeSuggestion(MergeSuggestion mergeSuggestion) {
		this.mergeSuggestion = mergeSuggestion;
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		try {
			Merger merger = null;
			if (mergeSuggestion != null) {
				for (MergeRule mergeRule : mergeSuggestion.getMergeClusters()) {
					merger = new Merger(mergeRule);
					merger.merge(mergeRule.getMasterRule().getName() + "Var");
				}
			} else if (mergeRule != null) {
				merger = new Merger(mergeRule);
				merger.merge(mergeRule.getMasterRule().getName() + "Var");
				saveMergedHenshinFile(merger);
			}
		} catch (MergeInException e) {
			e.printStackTrace();
			MessageDialog.openError(shell, HEADER, e.getMessage());
		}
	}

	private void saveMergedHenshinFile(Merger merger) {
		FileDialog fileDialog = new FileDialog(shell, SWT.SAVE);
		fileDialog.setFilterNames(FILTER_NAMES);
		fileDialog.setFilterExtensions(FILTER_EXTS);
		fileDialog.setText(HEADER);
		try {
			fileDialog
					.setFileName(getHenshinFile(merger.getModule()).getName());
			fileDialog.setFilterPath(getHenshinFile(merger.getModule())
					.getParent());
		} catch (MergeInException e) {
			e.printStackTrace();
			MessageDialog.openError(shell, HEADER, e.getMessage());
		} finally {
			String filename = fileDialog.open();
			if (filename != null) {
				try {
					merger.saveModule(filename);
				} catch (MergeInException e) {
					e.printStackTrace();
					MessageDialog.openError(shell, HEADER, e.getMessage());
				}
			}
		}
	}

	private File getHenshinFile(Module module) throws MergeInException {
		URI uri = module.eResource().getURI();
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IPath path = new Path(uri.toPlatformString(true));
		IFile file = workspaceRoot.getFile(path);
		if (file != null && file.getLocation() != null) {
			File f = file.getLocation().toFile();
			return f;
		}
		throw new MergeInException(MergeInException.NO_MODULE_3);
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		mergeRule = null;
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.getFirstElement() instanceof MergeRule) {
				mergeRule = (MergeRule) structuredSelection.getFirstElement();
			}
		}
	}

}
